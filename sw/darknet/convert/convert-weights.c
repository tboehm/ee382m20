#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#include "../src/fixed_configuration.h"

struct layer {
    int nweights;
    int n;
    int size;
    int c;
    int batch_norm;
    float *biases;
    float *scales;
    float *rolling_mean;
    float *rolling_variance;
    float *weights;
    float *transposed_weights;
};

// Relevant pieces of the yolov3-tiny.cfg
struct layer Layers[] = {
    { 432,     16,   3, 3,    1, 0, 0, 0, 0, 0 },
    { 4608,    32,   3, 16,   1, 0, 0, 0, 0, 0 },
    { 18432,   64,   3, 32,   1, 0, 0, 0, 0, 0 },
    { 73728,   128,  3, 64,   1, 0, 0, 0, 0, 0 },
    { 294912,  256,  3, 128,  1, 0, 0, 0, 0, 0 },
    { 1179648, 512,  3, 256,  1, 0, 0, 0, 0, 0 },
    { 4718592, 1024, 3, 512,  1, 0, 0, 0, 0, 0 },
    { 262144,  256,  1, 1024, 1, 0, 0, 0, 0, 0 },
    { 1179648, 512,  3, 256,  1, 0, 0, 0, 0, 0 },
    { 130560,  255,  1, 512,  0, 0, 0, 0, 0, 0 },
    { 32768,   128,  1, 256,  1, 0, 0, 0, 0, 0 },
    { 884736,  256,  3, 384,  1, 0, 0, 0, 0, 0 },
    { 65280,   255,  1, 256,  0, 0, 0, 0, 0, 0 },
};

void
floating_buffer_to_floating_file(float *buf, int count, FILE *outfile)
{
    int items_written = fwrite(buf, sizeof(float), count, outfile);
    if (items_written != count) {
        fprintf(stderr, "bytes written %d != expected %d\n", items_written, count);
    }
}

void
floating_buffer_to_fixed_file(float *buf, int count, FILE *outfile)
{
    int items_written;
    short *fx_buf = malloc(sizeof(short) * count);
    for (int i = 0; i < count; i++) {
        float floating = buf[i];
        short fx = FP_TO_FX(floating);
        fx_buf[i] = fx;
    }
    items_written = fwrite(fx_buf, sizeof(short), count, outfile);
    if (items_written != count) {
        fprintf(stderr, "bytes written %d != expected %d\n", items_written, count);
    }
}

void
fuse_conv_batchnorm(struct layer *l)
{
    for (int f = 0; f < l->n; ++f) {
        l->biases[f] = l->biases[f] - (double)l->scales[f] * l->rolling_mean[f] / (sqrt((double)l->rolling_variance[f] + .00001));

        double precomputed = l->scales[f] / (sqrt((double)l->rolling_variance[f] + .00001));

        const size_t filter_size = l->size*l->size*l->c;
        for (int i = 0; i < filter_size; ++i) {
            int w_index = f*filter_size + i;

            l->weights[w_index] *= precomputed;
        }
    }
}

void
transpose_weights(int *src, int *dst, int rows, int cols)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            dst[j * rows + i] = src[i * cols + j];
        }
    }
}

void
convert_layer_weights(struct layer *l, FILE *infile, FILE *outfile)
{
    int items_read;

    l->biases = malloc(sizeof(float) * l->n);
    items_read = fread(l->biases, sizeof(float), l->n, infile);
    if (items_read > 0 && items_read != l->n) {
        printf("Biases: Unexpected end of weights file!\n");
    }

    if (l->batch_norm) {
        l->scales = malloc(sizeof(float) * l->n);
        items_read = fread(l->scales, sizeof(float), l->n, infile);
        if (items_read > 0 && items_read != l->n) {
            printf("Scales: Unexpected end of weights file!\n");
        }

        l->rolling_mean = malloc(sizeof(float) * l->n);
        items_read = fread(l->rolling_mean, sizeof(float), l->n, infile);
        if (items_read > 0 && items_read != l->n) {
            printf("Rolling mean: Unexpected end of weights file!\n");
        }

        l->rolling_variance = malloc(sizeof(float) * l->n);
        items_read = fread(l->rolling_variance, sizeof(float), l->n, infile);
        if (items_read > 0 && items_read != l->n) {
            printf("Rolling variance: Unexpected end of weights file!\n");
        }
    }

    l->weights = malloc(sizeof(float) * l->nweights);
    l->transposed_weights = malloc(sizeof(float) * l->nweights);
    items_read = fread(l->weights, sizeof(float), l->nweights, infile);
    if (items_read > 0 && items_read != l->nweights) {
        printf("Weights: Unexpected end of weights file!\n");
    }
    if (l->batch_norm) {
        fuse_conv_batchnorm(l);
    }

    // fuse_conv_batchnorm() modifies the biases, so we need to run this last.
    floating_buffer_to_fixed_file(l->biases, l->n, outfile);

    // Our GEMMs expects the A matrix (i.e. weights) to be transposed.
    int weight_rows = l->n;
    int weight_cols = l->nweights / weight_rows;

    floating_buffer_to_fixed_file(l->weights, l->nweights, outfile);
    // If we don't want B to be transposed:
    // transpose_weights(l->weights, l->transposed_weights, weight_rows, weight_cols);
    // floating_buffer_to_fixed_file(l->transposed_weights, l->nweights, outfile);
}

void
convert_net_weights(FILE *infile, FILE *outfile)
{
    struct layer *l;

    for (size_t i = 0; i < sizeof(Layers) / sizeof(Layers[0]); i++) {
        l = &Layers[i];
        printf("Layer %lu: %d weights, %d biases\n", i, l->nweights, l->n);
        convert_layer_weights(l, infile, outfile);
    }
}

int main(int argc, char *argv[])
{
    const char *fname;
    FILE *infile;
    FILE *outfile;

    if (argc != 2) {
        fprintf(stderr, "usage: %s <weights>\n", argv[0]);
        return 1;
    }

    fname = argv[1];

    infile = fopen(fname, "rb");
    outfile = fopen("converted.weights", "wb");

    int major;
    int minor;
    int revision;
    fread(&major, sizeof(int), 1, infile);
    fwrite(&major, sizeof(int), 1, outfile);
    fread(&minor, sizeof(int), 1, infile);
    fwrite(&minor, sizeof(int), 1, outfile);
    fread(&revision, sizeof(int), 1, infile);
    fwrite(&revision, sizeof(int), 1, outfile);
    if ((major * 10 + minor) >= 2) {
        uint64_t iseen = 0;
        fread(&iseen, sizeof(uint64_t), 1, infile);
        fwrite(&iseen, sizeof(uint64_t), 1, outfile);
    }
    else {
        uint32_t iseen = 0;
        fread(&iseen, sizeof(uint32_t), 1, infile);
        fwrite(&iseen, sizeof(uint32_t), 1, outfile);
    }

    convert_net_weights(infile, outfile);

    fclose(infile);
    fclose(outfile);

    return 0;
}
