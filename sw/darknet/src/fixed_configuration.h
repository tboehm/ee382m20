#ifndef __FIXED_CONFIGURATION_H__
#define __FIXED_CONFIGURATION_H__

#include <limits.h>
#include <stdint.h>

typedef int fixed;

// These are the lowest values that get a non-zero MAP (83.33%).
#define FX_INT_BITS 7
#define FX_SCALE_BITS 9
#define FX_WIDTH ((FX_INT_BITS) + (FX_SCALE_BITS))

#define FX_MASK ((1 << (FX_WIDTH)) - 1)
#define FX_SCALE (1 << FX_SCALE_BITS)

#define FX_MAX ((1 << ((FX_WIDTH) - 1)) - 1)
#define FX_MIN (-1 - (FX_MAX))

#define FX_ONE (1 << FX_SCALE_BITS)

#define FX_CLAMP(fx) ((fx) > (FX_MAX) ? (FX_MAX) : (fx) < (FX_MIN) ? (FX_MIN) : (fx))

static inline fixed
fp_to_fx(float fp)
{
    fp *= FX_SCALE;
    // return (fixed)FX_CLAMP(fp);

    if (fp > (FX_MAX)) {
        return FX_MAX;
    } else if (fp < FX_MIN) {
        return FX_MIN;
    } else {
        return (fixed)fp;
    }
}

#define FP_TO_FX(fp) (fp_to_fx(fp))
#define FX_TO_FP(fx) ((float)(fx) / FX_SCALE)
#define FX_MUL(a, b) (((a) * (b)) >> FX_SCALE_BITS)

#endif // __FIXED_CONFIGURATION_H__
