#include "activations.h"

#include <stdio.h>
#include <string.h>

char *
get_activation_string(ACTIVATION a)
{
    switch (a) {
    case RELU:
        return "relu";
    case LINEAR:
        return "linear";
    case LEAKY:
        return "leaky";
    default:
        return "unknown";
    }
}

ACTIVATION
get_activation(char *s)
{
    if (strcmp(s, "linear") == 0) {
        return LINEAR;
    } else if (strcmp(s, "leaky") == 0) {
        return LEAKY;
    } else {
        fprintf(stderr, "Couldn't find activation function %s, going with ReLU\n", s);
        return RELU;
    }
}
