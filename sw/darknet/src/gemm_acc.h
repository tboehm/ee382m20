#ifndef __GEMM_ACC_H__
#define __GEMM_ACC_H__

#include <stdbool.h>

void *gemm_hw_thread(void *arg);

void gemm_hw(int M, int N, int K, void *A, void *B, void *C, bool partitioned, int layer_count);

#endif // __GEMM_ACC_H__
