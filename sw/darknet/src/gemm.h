#ifndef __GEMM_H__
#define __GEMM_H__

// Pad the K dimension on layer 1 from 27 to 32. Makes SIMD and accelerator packing easier.
#define PAD_K_CONV_1 5
#define LAYER_1_K (27 + PAD_K_CONV_1)

// Define this to help with unrolling for layer 2.
#define LAYER_2_K 144

// Take the minimum of two values. Useful for setting upper bounds in blocked GEMMs.
#define MIN(x, y) ((x) > (y) ? (y) : (x))

void gemm(int M, int N, int K, short *A, short *B, short *C);

#endif // __GEMM_H__
