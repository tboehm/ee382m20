#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"
#include "utils.h"

void
free_matrix(matrix m)
{
    int i;

    for (i = 0; i < m.rows; ++i) {
        free(m.vals[i]);
    }
    free(m.vals);
}

matrix
resize_matrix(matrix m, int size)
{
    int i;

    if (m.rows == size) return m;
    if (m.rows < size) {
        m.vals = (float **)xrealloc(m.vals, size * sizeof(float *));
        for (i = m.rows; i < size; ++i) {
            m.vals[i] = (float *)xcalloc(m.cols, sizeof(float));
        }
    } else if (m.rows > size) {
        for (i = size; i < m.rows; ++i) {
            free(m.vals[i]);
        }
        m.vals = (float **)xrealloc(m.vals, size * sizeof(float *));
    }
    m.rows = size;
    return m;
}

matrix
make_matrix(int rows, int cols)
{
    int i;
    matrix m;

    m.rows = rows;
    m.cols = cols;
    m.vals = (float **)xcalloc(m.rows, sizeof(float *));
    for (i = 0; i < m.rows; ++i) {
        m.vals[i] = (float *)xcalloc(m.cols, sizeof(float));
    }
    return m;
}
