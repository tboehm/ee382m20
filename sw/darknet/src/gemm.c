#include "gemm.h"
#include "utils.h"
#include "im2col.h"
#include "fixed_configuration.h"
#include "time.h"
#include "printd.h"
#include "gemm_acc.h"
#include "logging.h"
#include "secret_sauce.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#if defined(_OPENMP)
#include <omp.h>
#endif

// Whether we're doing GEMMs in hardware, software, or both.
bool HardwareGemms;
bool SoftwareGemms;

// The first GEMM is the only one where the K dimension is not divisible by 8. Since it's so small
// (and known -- 27), we can just unroll the whole thing. This lets us compute all of C[i, j] in one
// go.
//
// We also use the j-i-k loop ordering here. The A matrix is tiny at 16x27 (actually even smaller
// since we're using OpenMP), so it makes sense to have the i loop nested within the j loop.
void
gemm_layer_1(int M, int N, int K,
             short * restrict A,
             short * restrict B,
             short * restrict C,
             const struct gemm_partition *part,
             const struct block_sizes *bs)
{
    for (int j = part->n_lower; j < part->n_upper; j++) {
        for (int i = part->m_lower; i < part->m_upper; i++) {
            int c_part = 0;
            for (int k = 0; k < LAYER_1_K; k++) {
                int product = A[i * K + k] * B[j * K + k];
                c_part += product;
            }
            C[j * M + i] = FX_CLAMP(c_part >> FX_SCALE_BITS);
        }
    }
}

// The second GEMM also benefits from j-i-k because of the weird input shapes.
void
gemm_layer_2(int M, int N, int K,
             short * restrict A,
             short * restrict B,
             short * restrict C,
             const struct gemm_partition *part,
             const struct block_sizes *bs)
{
    for (int i_l1 = part->m_lower; i_l1 < part->m_upper; i_l1 += bs->m_bs) {
        int i_l1_upper = i_l1 + bs->m_bs;
        for (int j = part->n_lower; j < part->n_upper; j++) {
            for (int i = i_l1; i < i_l1_upper; i++) {
                int c_part = 0;
                for (int k = 0; k < LAYER_2_K; k++) {
                    int product = A[i * K + k] * B[j * K + k];
                    c_part += product;
                }
                C[j * M + i] = FX_CLAMP(c_part >> FX_SCALE_BITS);
            }
        }
    }
}

// Most layers do just fine with this general GEMM function. It has an i-j-k loop order and can have
// blocking on the i or j loops.
void
gemm_general(int M, int N, int K,
             short * restrict A,
             short * restrict B,
             short * restrict C,
             const struct gemm_partition *part,
             const struct block_sizes *bs)
{
    for (int i_l1 = part->m_lower; i_l1 < part->m_upper; i_l1 += bs->m_bs) {
        int i_l1_upper = MIN(i_l1 + bs->m_bs, part->m_upper);
        for (int j_l1 = part->n_lower; j_l1 < part->n_upper; j_l1 += bs->n_bs) {
            int j_l1_upper = MIN(j_l1 + bs->n_bs, part->n_upper);
            for (int i = i_l1; i < i_l1_upper; i++) {
                for (int j = j_l1; j < j_l1_upper; j++) {
                    int c_part = 0;
                    for (int k = 0; k < K; k += UNROLL_FACTOR) {
                        for (int k_unroll = 0; k_unroll < UNROLL_FACTOR; k_unroll++) {
                            int product = A[i * K + k + k_unroll] * B[j * K + k + k_unroll];
                            c_part += product;
                        }
                    }
                    C[j * M + i] += FX_CLAMP(c_part >> FX_SCALE_BITS);
                }
            }
        }
    }
}

// Layers 1 and 2 get their own special functions. Make an array to figure out which one to use
// programmatically.
typedef void (*gemm_func)(int, int, int,
                          short * restrict,
                          short *restrict,
                          short *restrict,
                          const struct gemm_partition *,
                          const struct block_sizes *);

static gemm_func GemmFuncs[13] = {
    gemm_layer_1,
    gemm_layer_2,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
    gemm_general,
};

// How much of the non-FPGA share of the work should core 0 work on?
const float Core0ComputeFrac = 0.24;
// const float Core0ComputeFrac = 0;

static void
get_core_partition(int core, int gemm_count, int M, int N, bool fpga, struct gemm_partition *part)
{
    int core0_compute = Core0ComputeFrac ? 1 : 0;
    int compute_core_count = core0_compute ? 4 : 3;

    if (!fpga) {
        if (gemm_count < 2) {
            // Split the N dimension since A fits in L1.
            part->m_lower = 0;
            part->m_upper = M;
            part->n_lower = (core * N) / 4;
            part->n_upper = ((core + 1) * N) / 4;
        } else {
            // Split the M dimension.
            part->m_lower = (core * M) / 4;
            part->m_upper = ((core + 1) * M) / 4;
            part->n_lower = 0;
            part->n_upper = N;
        }
    } else {
        // The size of the partition for the FPGA is not necessarily 1/4 of the total work.
        part->m_lower = 0;
        part->m_upper = M;

        // How much the FPGA alone works on.
        float fpga_frac = FpgaFractions[gemm_count];

        // How much the compute cores work on.
        float cpu_frac = 1 - fpga_frac;

        // How much core0 in total is responsible for (compute + FPGA).
        float core0_frac = fpga_frac + Core0ComputeFrac * cpu_frac;
        int core0_end = N * core0_frac;

        if (core == 0) {
            part->n_lower = 0;
            part->n_upper = core0_end;
        } else {
            // Split the remaining share equally among the three cores.
            int remaining_n = N - core0_end;
            int lower_offset = (core - 1) * remaining_n / 3;
            int upper_offset = core * remaining_n / 3;

            part->n_lower = core0_end + lower_offset;
            part->n_upper = core0_end + upper_offset;
        }
    }
}

void
gemm_hw_and_sw(int M, int N, int K,
               short * restrict A,
               short * restrict B,
               short * restrict C,
               int gemm_count,
               const struct gemm_partition *part,
               const struct block_sizes *bs)
{
    pthread_t hw_tid, sw_tid;
    void *hw_ret, *sw_ret;

    struct gemm_partition sw_part;
    struct gemm_partition hw_part;

    memcpy(&sw_part, part, sizeof(*part));
    memcpy(&hw_part, part, sizeof(*part));

    hw_part.n_upper = N * FpgaFractions[gemm_count];
    hw_part.n_upper &= (~1);
    sw_part.n_lower = hw_part.n_upper;

    struct gemm_args hw_args = { M, N, K, A, B, C, gemm_count, &hw_part, bs };

    if (pthread_create(&hw_tid, NULL, gemm_hw_thread, &hw_args) != 0) {
        log_perror("create hw thread");
        exit(EXIT_FAILURE);
    }

    GemmFuncs[gemm_count](M, N, K, A, B, C, &sw_part, bs);

    if (pthread_join(hw_tid, &hw_ret) != 0) {
        log_perror("join hw thread");
        exit(EXIT_FAILURE);
    }
}

void
gemm(int M, int N, int K, short *A, short *B, short *C)
{
    static int gemm_count = 0;
    static double gemm_time = 0.f;

    double ms_start = get_time_ms();

    if (!SoftwareGemms) {
        gemm_hw(M, N, K, A, B, C, false, gemm_count); // false -> not partitioned
    } else {
        // Hard-coded to use 4 processes. Each should take nearly identical amounts of time since
        // they're all doing equally (or nearly equally) sized GEMMs.
        #pragma omp parallel for
        for (int core = 0; core < 4; core++) {
            // Each matrix gets its own hand-tuned block sizes.
            const struct block_sizes *bs = &BlockSizes[gemm_count];

            // We'll figure out what subsection of the matrices each core will use.
            struct gemm_partition part;

            if (HardwareGemms && FpgaFractions[gemm_count]) {
                // Combination of hardware and softawer. FPGA gets some fraction, the other three
                // cores split the remaining part evenly.
                get_core_partition(core, gemm_count, M, N, true, &part); // true -> fpga
                if (core == 0) {
                    if (Core0ComputeFrac == 0.f) {
                        gemm_hw(M, part.n_upper, K, A, B, C, true, gemm_count); // true -> partitioned
                    } else {
                        gemm_hw_and_sw(M, N, K, A, B, C, gemm_count, &part, bs);
                    }
                } else {
                    GemmFuncs[gemm_count](M, N, K, A, B, C, &part, bs);
                }
            } else {
                // Just run this one entirely on software. Each core gets 25%.
                get_core_partition(core, gemm_count, M, N, false, &part); // false -> fpga
                GemmFuncs[gemm_count](M, N, K, A, B, C, &part, bs);
            }
        }
    }

    gemm_count++;

    double ms_end = get_time_ms();
    double elapsed = ms_end - ms_start;
    double Gops = M * N * K / elapsed / 1e6;
    printd(" [\033[36;1m%.0f ms\033[0m, %.2f Gops] ", elapsed, Gops);

    gemm_time += elapsed;
    if (gemm_count == 13) {
        printd(" [\033[33;1mTotal GEMM time: %.0f ms\033[0m] ", gemm_time);
    }
}
