#ifndef BLAS_H
#define BLAS_H
#include <stdlib.h>
#include "darknet.h"

#ifdef __cplusplus
extern "C" {
#endif

void flatten(float *x, int size, int layers, int batch, int forward);

void axpy_cpu(int N, float ALPHA, float *X, int INCX, float *Y, int INCY);
void copy_cpu(int N, float *X, int INCX, float *Y, int INCY);
void fill_cpu(int N, float ALPHA, float * X, int INCX);

void softmax(float *input, int n, float temp, float *output, int stride);

float find_sim(size_t i, size_t j, contrastive_params *contrast_p, int contrast_p_size);

void get_embedding(float *src, int src_w, int src_h, int src_c, int embedding_size, int cur_w, int cur_h, int cur_n, int cur_b, float *dst);
float cosine_similarity(float *A, float *B, unsigned int feature_size);

#ifdef __cplusplus
}
#endif
#endif
