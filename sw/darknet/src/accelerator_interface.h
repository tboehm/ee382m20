#ifndef __ACCELERATOR_INTERFACE_H__
#define __ACCELERATOR_INTERFACE_H__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <signal.h>   // sighandler_t
#include <stdint.h>   // uint32_t and friends
#include <sys/mman.h> // mmap flags

// Gemm interface functions
void accelerator_setup_buffer1(unsigned int M, unsigned int N, unsigned int K, unsigned int newM, unsigned int B1, unsigned int C1);
void accelerator_clear_buffer1(void);
void accelerator_start_buffer1(void);
void accelerator_update_c1_sp(unsigned int c1_sp);

// Communication.
void accelerator_dump_registers(void);
void accelerator_wait_for_interrupt(void);
void accelerator_yield_for_interrupt(void);
void accelerator_send_array(uint32_t dst, void *src, size_t bytes);
void accelerator_get_result(int *dst, uint32_t src, size_t bytes);

// Setup/teardown.
void accelerator_init();
void accelerator_finish();

// Key:
//  - SC = Self Clear
//  - COR = Clear on Read
//  - TOW = Toggle on Write
//  - COH = Clear on Handshake

// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
#define FPGA_ADDR_AP_CTRL 0x00
#define FPGA_OFFS_AP_CTRL (FPGA_ADDR_AP_CTRL >> 2)

// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
#define FPGA_ADDR_GIE 0x04
#define FPGA_OFFS_GIE (FPGA_ADDR_GIE >> 2)

// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
#define FPGA_ADDR_IER 0x08
#define FPGA_OFFS_IER (FPGA_ADDR_IER >> 2)

// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
#define FPGA_ADDR_ISR 0x0c
#define FPGA_OFFS_ISR (FPGA_ADDR_ISR >> 2)

// 0x10 : Data signal of START1
//        bit 0  - START1[0] (Read/Write)
//        others - reserved
#define FPGA_ADDR_START1_DATA 0x10
#define FPGA_OFFS_START1_DATA (FPGA_ADDR_START1_DATA >> 2)
#define FPGA_BITS_START1_DATA 1

// 0x18 : Data signal of M1
//        bit 31~0 - M1[31:0] (Read/Write)
#define FPGA_ADDR_M1_DATA 0x18
#define FPGA_OFFS_M1_DATA (FPGA_ADDR_M1_DATA >> 2)
#define FPGA_BITS_M1_DATA 32

// 0x20 : Data signal of N1
//        bit 31~0 - N1[31:0] (Read/Write)
#define FPGA_ADDR_N1_DATA 0x20
#define FPGA_OFFS_N1_DATA (FPGA_ADDR_N1_DATA >> 2)
#define FPGA_BITS_N1_DATA 32

// 0x28 : Data signal of K1
//        bit 31~0 - K1[31:0] (Read/Write)
#define FPGA_ADDR_K1_DATA 0x28
#define FPGA_OFFS_K1_DATA (FPGA_ADDR_K1_DATA >> 2)
#define FPGA_BITS_K1_DATA 32

// 0x30 : Data signal of new_M1
//        bit 31~0 - new_M1[31:0] (Read/Write)
#define FPGA_ADDR_NEW_M1_DATA 0x30
#define FPGA_OFFS_NEW_M1_DATA (FPGA_ADDR_NEW_M1_DATA >> 2)
#define FPGA_BITS_NEW_M1_DATA 32

// 0x38 : Data signal of B1_SP_START
//        bit 31~0 - B1_SP_START[31:0] (Read/Write)
#define FPGA_ADDR_B1_SP_START_DATA 0x38
#define FPGA_OFFS_B1_SP_START_DATA (FPGA_ADDR_B1_SP_START_DATA >> 2)
#define FPGA_BITS_B1_SP_START_DATA 32

// 0x40 : Data signal of C1_SP_START
//        bit 31~0 - C1_SP_START[31:0] (Read/Write)
#define FPGA_ADDR_C1_SP_START_DATA 0x40
#define FPGA_OFFS_C1_SP_START_DATA (FPGA_ADDR_C1_SP_START_DATA >> 2)
#define FPGA_BITS_C1_SP_START_DATA 32

struct register_description {
    uint32_t   offset;
    const char name[32];
};

struct register_field {
    int        bit;
    int        size;
    const char description[32];
};

static struct register_description FpgaRegisters[] __attribute__((unused)) = {
    { FPGA_OFFS_AP_CTRL,          "Control signals"           },
    { FPGA_OFFS_GIE,              "Global interrupt enable"   },
    { FPGA_OFFS_IER,              "Interrupt enable register" },
    { FPGA_OFFS_ISR,              "Interrupt status register" },
    { FPGA_OFFS_START1_DATA,      "Start signal"              },
    { FPGA_OFFS_M1_DATA,          "M dimension"               },
    { FPGA_OFFS_K1_DATA,          "K dimension"               },
    { FPGA_OFFS_N1_DATA,          "N dimension"               },
    { FPGA_OFFS_NEW_M1_DATA,      "New M dimension"           },
    { FPGA_OFFS_B1_SP_START_DATA, "B scratchpad start"        },
    { FPGA_OFFS_C1_SP_START_DATA, "C scratchpad start"        },
};

enum FpgaCtrlFieldBits {
    FpgaCtrlFieldStart       = 0,
    FpgaCtrlFieldDone        = 1,
    FpgaCtrlFieldIdle        = 2,
    FpgaCtrlFieldReady       = 3,
    FpgaCtrlFieldAutoRestart = 7,
};

static struct register_field FpgaCtrlFields[] __attribute__((unused)) = {
    { FpgaCtrlFieldStart,       1, "Start"        },
    { FpgaCtrlFieldDone,        1, "Done"         },
    { FpgaCtrlFieldIdle,        1, "Idle"         },
    { FpgaCtrlFieldReady,       1, "Ready"        },
    { FpgaCtrlFieldAutoRestart, 1, "Auto-Restart" },
    { 0,                        0, ""             },
};

// Accelerator location and size.
#define FPGA_BASE 0xa0028000
#define FPGA_MASK 0x0000ffff
#define FPGA_SIZE 0x00008000

// BRAM location (as seen by CDMA) and size.
#define BRAM_CDMA 0xB0080000
#define BRAM_SIZE (512 * 1024)

// CDMA location and size.
#define CDMA 0xB0000000
#define CDMA_SIZE 0x1000

// OCM location and size.
#define OCM 0xFFFC0000
#define OCM_SIZE 0x40000

// Permissions and flags for mmap'ing devices.
#define MMAP_PERMISSIONS (PROT_READ | PROT_WRITE)
#define MMAP_FLAGS MAP_SHARED

// DMA commands.
#define CDMACR            0x00
#define CDMASR            0x04
#define CURDESC_PNTR      0x08
#define CURDESC_PNTR_MSB  0x0C
#define TAILDESC_PNTR     0x10
#define TAILDESC_PNTR_MSB 0x14
#define SA                0x18
#define SA_MSB            0x1C
#define DA                0x20
#define DA_MSB            0x24
#define BTT               0x28

static struct register_field CdmaCrFields[] __attribute__((unused)) = {
    { 1,  1, "Tail Ptr En"      },
    { 2,  1, "Reset"            },
    { 3,  1, "SG Mode"          },
    { 4,  1, "Key Hole Read"    },
    { 5,  1, "Key Hole Write"   },
    { 6,  1, "Cyclic BD Enable" },
    { 12, 1, "Ioc Irq En"       },
    { 13, 1, "Dly Irq En"       },
    { 14, 1, "Err Irq En"       },
    { 0,  0, ""                 },
};

static struct register_field CdmaSrFields[] __attribute__((unused ))= {
    { 1,  1, "Idle"        },
    { 3,  1, "SG Inc ID"   },
    { 4,  1, "DMA Int Err" },
    { 5,  1, "DMA Slv Err" },
    { 6,  1, "DMA Dec Err" },
    { 8,  1, "SG Int Err"  },
    { 9,  1, "SG Slv Err"  },
    { 10, 1, "SG Dec Err"  },
    { 12, 1, "IOC Irq"     },
    { 13, 1, "Dly Irq"     },
    { 14, 1, "Err Irq"     },
    { 0,  0, ""            },
};

// Path to FPGA kernel module/device.
static const char FpgaDevPath[] = "/dev/fpga";

// Path to CDMA kernel module/device.
static const char CdmaDevPath[] = "/dev/cdma";

// Signal for FPGA interrupts.
#define FPGA_SIGNAL SIGUSR1

// Signal for CDMA interrupts.
#define CDMA_SIGNAL SIGUSR2

// Information needed to register a device signal handler.
struct kernel_handler {
    const char  *dev_path; // Path to device
    int          signo;    // Signal number
    sighandler_t handler;  // Signal handler
};

#endif // __ACCELERATOR_INTERFACE_H__
