#ifndef __SECRET_SAUCE_H__
#define __SECRET_SAUCE_H__

// Help GCC vectorize by always unrolling UNROLL_FACTOR iterations of the K loop.
#define UNROLL_FACTOR 16

// Block sizes for each dimension of a GEMM.
struct block_sizes {
    int m_bs;
    int n_bs;
};

// M and N bounds for OpenMP/FPGA splits.
struct gemm_partition {
    int m_lower;
    int m_upper;
    int n_lower;
    int n_upper;
};

// Arguments that a GEMM function (pthread) will get.
struct gemm_args {
    int M;
    int N;
    int K;
    short *A;
    short *B;
    short *C;
    int gemm_count;
    const struct gemm_partition *part;
    const struct block_sizes *bs;
};

// How a layer will be computed in the FPGA.
struct layer_config {
    int M;
    int K;
    int N;
    int rows_M;
    int rows_N;
    int b_iter;
    int a_iter;
    int scenario;
};

// Each GEMM gets its own hand-tuned block sizes.
static const struct block_sizes BlockSizes[] = {
     [0] = { .m_bs = 16,  .n_bs = 50 },
     [1] = { .m_bs = 8,   .n_bs = 50 },
     [2] = { .m_bs = 64,  .n_bs = 50 },
     [3] = { .m_bs = 64,  .n_bs = 50 },
     [4] = { .m_bs = 64,  .n_bs = 50 },
     [5] = { .m_bs = 64,  .n_bs = 10 },
     [6] = { .m_bs = 64,  .n_bs = 10 },
     [7] = { .m_bs = 64,  .n_bs = 10 },
     [8] = { .m_bs = 64,  .n_bs = 10 },
     [9] = { .m_bs = 64,  .n_bs = 10 },
    [10] = { .m_bs = 64,  .n_bs = 10 },
    [11] = { .m_bs = 128, .n_bs = 50 },
    [12] = { .m_bs = 256, .n_bs = 50 },
};

// The FPGA doesn't always do 25% of the work.
static const float FpgaFractions[] = {
     [0] = 0.00,
     [1] = 0.00,
     [2] = 0.06,
     [3] = 0.04,
     [4] = 0.16,
     [5] = 0.16,
     [6] = 0.18,
     [7] = 0.25,
     [8] = 0.18,
     [9] = 0.08,
    [10] = 0.08,
    [11] = 0.00,
    [12] = 0.25,
};

// // The FPGA doesn't always do 25% of the work.
// static const float FpgaFractions[] = {
//      [0] = 0.04,
//      [1] = 0.08,
//      [2] = 0.10,
//      [3] = 0.04,
//      [4] = 0.16,
//      [5] = 0.16,
//      [6] = 0.04,
//      [7] = 0.08,
//      [8] = 0.08,
//      [9] = 0.08,
//     [10] = 0.08,
//     [11] = 0.12,
//     [12] = 0.25,
// };

// Accelerator-only FPGA configuration
static struct layer_config LayerConfigs[] = {
     [0] = { .M = 16,   .K = 32,   .N = 102400, .rows_M = 16,  .rows_N = 5120, .b_iter = 20, .a_iter = 1,  .scenario = 1 },
     [1] = { .M = 32,   .K = 144,  .N = 25600,  .rows_M = 32,  .rows_N = 1280, .b_iter = 20, .a_iter = 1,  .scenario = 1 },
     [2] = { .M = 64,   .K = 288,  .N = 6400,   .rows_M = 64,  .rows_N = 640,  .b_iter = 10, .a_iter = 1,  .scenario = 1 },
     [3] = { .M = 128,  .K = 576,  .N = 1600,   .rows_M = 128, .rows_N = 200,  .b_iter = 8,  .a_iter = 1,  .scenario = 1 },
     [4] = { .M = 256,  .K = 1152, .N = 400,    .rows_M = 64,  .rows_N = 100,  .b_iter = 4,  .a_iter = 4,  .scenario = 2 },
     [5] = { .M = 512,  .K = 2304, .N = 100,    .rows_M = 64,  .rows_N = 25,   .b_iter = 4,  .a_iter = 8,  .scenario = 2 },
     [6] = { .M = 1024, .K = 4608, .N = 100,    .rows_M = 32,  .rows_N = 20,   .b_iter = 5,  .a_iter = 32, .scenario = 2 },
     [7] = { .M = 256,  .K = 1024, .N = 100,    .rows_M = 128, .rows_N = 100,  .b_iter = 1,  .a_iter = 2,  .scenario = 2 },
     [8] = { .M = 512,  .K = 2304, .N = 100,    .rows_M = 64,  .rows_N = 25,   .b_iter = 4,  .a_iter = 8,  .scenario = 2 },
     [9] = { .M = 256,  .K = 512,  .N = 100,    .rows_M = 256, .rows_N = 100,  .b_iter = 1,  .a_iter = 1,  .scenario = 1 },
    [10] = { .M = 128,  .K = 256,  .N = 100,    .rows_M = 128, .rows_N = 100,  .b_iter = 1,  .a_iter = 1,  .scenario = 1 },
    [11] = { .M = 256,  .K = 3456, .N = 400,    .rows_M = 32,  .rows_N = 25,   .b_iter = 16, .a_iter = 8,  .scenario = 2 },
    [12] = { .M = 256,  .K = 256,  .N = 400,    .rows_M = 256, .rows_N = 200,  .b_iter = 2,  .a_iter = 1,  .scenario = 1 },
};

// Partitioned CPU/FPGA configuration
static struct layer_config PartConfigs[] = {
     [0] = { .M = 16,   .K = 32,   .N = 4096, .rows_M = 16,  .rows_N = 4096, .b_iter = 1, .a_iter = 1,  .scenario = 1 },
     [1] = { .M = 32,   .K = 144,  .N = 1024, .rows_M = 32,  .rows_N = 1024, .b_iter = 1, .a_iter = 1,  .scenario = 1 },
     [2] = { .M = 64,   .K = 288,  .N = 400,  .rows_M = 64,  .rows_N = 400,  .b_iter = 1, .a_iter = 1,  .scenario = 1 },
     [3] = { .M = 128,  .K = 576,  .N = 160,  .rows_M = 128, .rows_N = 160,  .b_iter = 1, .a_iter = 1,  .scenario = 1 },
     [4] = { .M = 256,  .K = 1152, .N = 80,   .rows_M = 64,  .rows_N = 80,   .b_iter = 1, .a_iter = 4,  .scenario = 2 },
     [5] = { .M = 512,  .K = 2304, .N = 20,   .rows_M = 64,  .rows_N = 20,   .b_iter = 1, .a_iter = 8,  .scenario = 2 },
     [6] = { .M = 1024, .K = 4608, .N = 20,   .rows_M = 16,  .rows_N = 20,   .b_iter = 1, .a_iter = 64, .scenario = 2 },
     [7] = { .M = 256,  .K = 1024, .N = 25,   .rows_M = 128, .rows_N = 50,   .b_iter = 1, .a_iter = 2,  .scenario = 2 },
     [8] = { .M = 512,  .K = 2304, .N = 20,   .rows_M = 64,  .rows_N = 20,   .b_iter = 1, .a_iter = 8,  .scenario = 2 },
     [9] = { .M = 256,  .K = 512,  .N = 25,   .rows_M = 256, .rows_N = 50,   .b_iter = 1, .a_iter = 1,  .scenario = 1 },
    [10] = { .M = 128,  .K = 256,  .N = 25,   .rows_M = 128, .rows_N = 100,  .b_iter = 1, .a_iter = 1,  .scenario = 1 },
    [11] = { .M = 256,  .K = 3456, .N = 25,   .rows_M = 32,  .rows_N = 25,   .b_iter = 1, .a_iter = 8,  .scenario = 2 },
    [12] = { .M = 256,  .K = 256,  .N = 100,  .rows_M = 256, .rows_N = 200,  .b_iter = 1, .a_iter = 1,  .scenario = 1 },
};

static const int NumRowsInChunkA[] = { 16,   32,   64,  128, 64, 64, 32, 128, 64, 256, 128, 32, 256 };
static const int NumRowsInChunkB[] = { 5120, 1280, 640, 200, 100, 25, 20, 100,  25, 100,  100, 25, 200 };


#endif // __SECRET_SAUCE_H__
