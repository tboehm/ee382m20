#ifndef REGION_LAYER_H
#define REGION_LAYER_H

#include "layer.h"
#include "network.h"

typedef layer region_layer;

#ifdef __cplusplus
extern "C" {
#endif
void get_region_boxes(layer l, int w, int h, float thresh, float **probs, box *boxes, int only_objectness, int *map);

#ifdef __cplusplus
}
#endif
#endif
