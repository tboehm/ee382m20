#include "darknet.h"

#include <time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "accelerator_interface.h"
#include "parser.h"
#include "utils.h"
#include "blas.h"

extern void run_detector(int argc, char **argv);
extern bool HardwareGemms;
extern bool SoftwareGemms;

int main(int argc, char **argv)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    printf(" _DEBUG is used \n");
#endif

#ifdef DEBUG
    printf(" DEBUG=1 \n");
#endif

	int i;
	for (i = 0; i < argc; ++i) {
		if (!argv[i]) continue;
		strip_args(argv[i]);
	}

    if(argc < 2){
        fprintf(stderr, "usage: %s <function>\n", argv[0]);
        return 0;
    }

    for (int i = 0; i < argc; i++) {
        char *opt = argv[i];
        if (strcmp(opt, "hw-only") == 0) {
            HardwareGemms = true;
            SoftwareGemms = false;
            break;
        } else if (strcmp(opt, "hw-sw") == 0) {
            HardwareGemms = true;
            SoftwareGemms = true;
            break;
        } else {
            HardwareGemms = false;
            SoftwareGemms = true;
        }
    }

#ifdef HW_ACC
    if (HardwareGemms) {
        accelerator_init();
    }
#endif

    if (0 == strcmp(argv[1], "detector")){
        run_detector(argc, argv);
    } else if (0 == strcmp(argv[1], "detect")){
        float thresh = find_float_arg(argc, argv, "-thresh", .24);
		int ext_output = find_arg(argc, argv, "-ext_output");
        char *filename = (argc > 4) ? argv[4]: 0;
        test_detector("cfg/coco.data", argv[2], argv[3], filename, thresh, 0.5, 0, ext_output, 0, NULL, 0, 0);
    } else {
        fprintf(stderr, "Not an option: %s\n", argv[1]);
    }

    return 0;
}
