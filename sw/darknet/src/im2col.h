#ifndef __IM2COL_H__
#define __IM2COL_H__

#include <stddef.h>
#include <stdint.h>
#include "darknet.h"

void im2col_of_transpose(const short *data_im, const int channels,
                         const int height, const int width,
                         short *data_col);

void im2col_layer_1(const short *data_im, const int channels,
                    const int height, const int width,
                    short *data_col);

#endif // __IM2COL_H__
