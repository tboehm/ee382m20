#include "gemm_acc.h"

#include <stdbool.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#include "accelerator_interface.h"
#include "logging.h"
#include "printd.h"
#include "secret_sauce.h"

#define SHORTS_PER_WORD 2
#define BYTES_PER_WORD (sizeof(short) * SHORTS_PER_WORD)

#ifdef TERSE
#define get_time_usec() 0
#else
static long
get_time_usec(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}
#endif

void *
gemm_hw_thread(void *arg)
{
    struct gemm_args *ga = (struct gemm_args *)arg;

    int M = ga->M;
    int N = ga->part->n_upper;
    int K = ga->K;
    void *A = (void *)ga->A;
    void *B = (void *)ga->B;
    void *C = (void *)ga->C;
    int gemm_count = ga->gemm_count;

    const struct layer_config *cfg = &LayerConfigs[gemm_count];

    int scenario = cfg->scenario;
    int b_iter = N / NumRowsInChunkB[gemm_count];
    int a_iter = cfg->a_iter;
    if (scenario == 2) {
        a_iter = b_iter * M / NumRowsInChunkA[gemm_count];
    }

    int new_N = cfg->rows_N;
    int new_M = cfg->rows_M;

    long communication_usec = 0;
    long compute_usec = 0;
    long start;

    uint32_t b1_sp = (uint32_t)(new_M * K);
    uint32_t c1_sp = b1_sp + (uint32_t)(new_N * K);
    uint32_t b1_sp_addr = BRAM_CDMA + b1_sp * SHORTS_PER_WORD;
    uint32_t c1_sp_addr = BRAM_CDMA + c1_sp * SHORTS_PER_WORD;

    size_t a_bytes = K * new_M * sizeof(short);
    size_t b_bytes = K * new_N * sizeof(short);
    size_t c_bytes = M * new_N * sizeof(short);

    accelerator_setup_buffer1(M, new_N, K, new_M, b1_sp, c1_sp);

    if (scenario == 1) {
        start = get_time_usec();
        accelerator_send_array(BRAM_CDMA, A, a_bytes);
        communication_usec += get_time_usec() - start;
        for (int b = 0, b_off = 0, c_off = 0; b < b_iter; b++, b_off += b_bytes, c_off += c_bytes) {
            start = get_time_usec();
            accelerator_send_array(b1_sp_addr, B + b_off, b_bytes);
            communication_usec += get_time_usec() - start;

            start = get_time_usec();
            accelerator_start_buffer1();
            accelerator_yield_for_interrupt();
            compute_usec += get_time_usec() - start;

            start = get_time_usec();
            accelerator_get_result(C + c_off, c1_sp_addr, c_bytes);
            communication_usec += get_time_usec() - start;
        }
    } else if (scenario == 2) {
        // A stationary, we index into different elements of B
        for (int b = 0, b_off = 0, c_off = 0; b < b_iter; b++, b_off += b_bytes, c_off += c_bytes) {
            start = get_time_usec();
            accelerator_send_array(b1_sp_addr, B + b_off, b_bytes);
            communication_usec += get_time_usec() - start;

            // Compute the chunk of C in pieces.
            for (int a = 0, a_off = 0, new_c1_sp = c1_sp; a < a_iter; a++, a_off += a_bytes, new_c1_sp += new_M) {
                start = get_time_usec();
                accelerator_send_array(BRAM_CDMA, A + a_off, a_bytes);
                accelerator_update_c1_sp(new_c1_sp);
                communication_usec += get_time_usec() - start;

                start = get_time_usec();
                accelerator_start_buffer1();
                accelerator_yield_for_interrupt();
                compute_usec += get_time_usec() - start;
            }

            // Copy C back
            start = get_time_usec();
            accelerator_get_result(C + c_off, c1_sp_addr, c_bytes);
            communication_usec += get_time_usec() - start;
        }
    }

    double comm_ms = communication_usec / 1000.f;
    double comp_ms = compute_usec / 1000.f;
    double total_ms = (communication_usec + compute_usec) / 1000.f;
    // printd(" [comm %g ms; compute %g ms] ", comm_ms, comp_ms);
    printd(" [\033[38;5;16;1m%.0f ms\033[0m] ", total_ms);

    return NULL;
}

void
gemm_hw(int M, int N, int K, void *A, void *B, void *C, bool partitioned, int layer_count)
{
    struct layer_config *cfg;

    if (partitioned) {
        cfg = &LayerConfigs[layer_count];
        printd(" [b_iter = %d, a_iter = %d, N = %d -> ", cfg->b_iter, cfg->a_iter, N);
        cfg->b_iter = N / NumRowsInChunkB[layer_count];
        if (cfg->scenario == 2) {
            cfg->a_iter = cfg->b_iter * M / NumRowsInChunkA[layer_count];
        }
        printd(" %d, %d] ", cfg->b_iter, cfg->a_iter);
    } else {
        cfg = &LayerConfigs[layer_count];
    }

    layer_count += 1;

    int new_N = cfg->rows_N;
    int new_M = cfg->rows_M;
    int scenario = cfg->scenario;
    int b_iter = cfg->b_iter;
    int a_iter = cfg->a_iter;

    long communication_usec = 0;
    long compute_usec = 0;
    long start;

    uint32_t b1_sp = (uint32_t)(new_M * K);
    uint32_t c1_sp = b1_sp + (uint32_t)(new_N * K);
    uint32_t b1_sp_addr = BRAM_CDMA + b1_sp * SHORTS_PER_WORD;
    uint32_t c1_sp_addr = BRAM_CDMA + c1_sp * SHORTS_PER_WORD;

    size_t a_bytes = K * new_M * sizeof(short);
    size_t b_bytes = K * new_N * sizeof(short);
    size_t c_bytes = M * new_N * sizeof(short);

    accelerator_setup_buffer1(M, new_N, K, new_M, b1_sp, c1_sp);

    if (scenario == 1) {
        start = get_time_usec();
        accelerator_send_array(BRAM_CDMA, A, a_bytes);
        communication_usec += get_time_usec() - start;
        for (int b = 0, b_off = 0, c_off = 0; b < b_iter; b++, b_off += b_bytes, c_off += c_bytes) {
            start = get_time_usec();
            accelerator_send_array(b1_sp_addr, B + b_off, b_bytes);
            communication_usec += get_time_usec() - start;

            start = get_time_usec();
            accelerator_start_buffer1();
            accelerator_wait_for_interrupt();
            compute_usec += get_time_usec() - start;

            start = get_time_usec();
            accelerator_get_result(C + c_off, c1_sp_addr, c_bytes);
            communication_usec += get_time_usec() - start;
        }
    } else if (scenario == 2) {
        // A stationary, we index into different elements of B
        for (int b = 0, b_off = 0, c_off = 0; b < b_iter; b++, b_off += b_bytes, c_off += c_bytes) {
            start = get_time_usec();
            accelerator_send_array(b1_sp_addr, B + b_off, b_bytes);
            communication_usec += get_time_usec() - start;

            // Compute the chunk of C in pieces.
            for (int a = 0, a_off = 0, new_c1_sp = c1_sp; a < a_iter; a++, a_off += a_bytes, new_c1_sp += new_M) {
                start = get_time_usec();
                accelerator_send_array(BRAM_CDMA, A + a_off, a_bytes);
                accelerator_update_c1_sp(new_c1_sp);
                communication_usec += get_time_usec() - start;

                start = get_time_usec();
                accelerator_start_buffer1();
                accelerator_wait_for_interrupt();
                compute_usec += get_time_usec() - start;
            }

            // Copy C back
            start = get_time_usec();
            accelerator_get_result(C + c_off, c1_sp_addr, c_bytes);
            communication_usec += get_time_usec() - start;
        }
    }

    double comm_ms = communication_usec / 1000.f;
    double comp_ms = compute_usec / 1000.f;
    printd(" [comm %g ms; compute %g ms] ", comm_ms, comp_ms);
}
