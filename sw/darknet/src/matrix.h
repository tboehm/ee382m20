#ifndef MATRIX_H
#define MATRIX_H
#include "darknet.h"

typedef struct {
    int *assignments;
    matrix centers;
} model;

#ifdef __cplusplus
extern "C" {
#endif

matrix make_matrix(int rows, int cols);
void free_matrix(matrix m);
matrix resize_matrix(matrix m, int size);

#ifdef __cplusplus
}
#endif
#endif
