#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "accelerator_interface.h"

#include <assert.h>   // assert()
#include <errno.h>    // int errno, EINTR
#include <fcntl.h>    // fcntl() and flags
#include <math.h>     // sqrt()
#include <signal.h>   // sighandler_t
#include <stdint.h>   // uint32_t and friends
#include <stdio.h>    // [f]printf
#include <stdlib.h>   // malloc()
#include <string.h>   // memset(), memcpy()
#include <sys/mman.h> // mmap()
#include <sys/time.h> // gettimeofday(), struct timeval
#include <unistd.h>   // General POSIX API

#include "fixed_configuration.h"
#include "logging.h"

// mmap'd devices
static volatile uint32_t *CdmaBase;
static volatile uint32_t *FpgaBase;
static          void     *OcmBase;

// Interrupt counting
static volatile int          FpgaSignalCount = 0;
static volatile sig_atomic_t FpgaSignalProcessed = 0;
static volatile int          CdmaSignalCount = 0;
static volatile sig_atomic_t CdmaSignalProcessed = 0;

// Interrupt timing
static struct timeval StartTimestamp;
static struct timeval SignalTimestamp;

// Signal handlers
static void fpga_signal_handler(int signo);
static void cdma_signal_handler(int signo);

// File descriptor for the FPGA device.
static int FpgaFd;

const struct kernel_handler DevHandlers[] = {
    { .dev_path = FpgaDevPath,
      .signo = FPGA_SIGNAL,
      .handler = fpga_signal_handler },
    { .dev_path = CdmaDevPath,
      .signo = CDMA_SIGNAL,
      .handler = cdma_signal_handler },
};

static long
get_time_usec(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}


static uint32_t
dma_set(int offset, uint32_t value)
{
    CdmaBase[offset >> 2] = value;
    return 0;
}

static uint32_t
dma_get(int offset)
{
    return CdmaBase[offset >> 2];
}

static int
cdma_sync(void)
{
    uint32_t status = dma_get(CDMASR);

    if ((status & 0x40) != 0) {
        uint32_t desc = dma_get(CURDESC_PNTR);
        log_error("Error address: 0x%x", desc);
        exit(EXIT_FAILURE);
        return 1;
    }

    while (!(status & 0x2)) {
        status = dma_get(CDMASR);
    }

    return 0;
}

static void
cdma_reset(void)
{
    log_debug("Resetting CDMA");
    dma_set(CDMACR, 0x4);
    uint32_t value;
    do {
        value = dma_get(CDMACR);
    } while (value & 0x4);

    // Wait for idle.
    log_debug("cdma_sync()");
    cdma_sync();
}

static inline void
format_register_no_newline(const char *name, uint32_t value)
{
    if (value) {
        printf("%32s: 0x%.8x", name, value);
    } else {
        printf("%32s: 0", name);
    }
}

static inline void
format_register(const char *name, uint32_t value)
{
    if (value) {
        format_register_no_newline(name, value);
        putchar('\n');
    }
}

void
print_register(const char *name, uint32_t value, struct register_field *fields)
{
    format_register_no_newline(name, value);

    int printed_any = 0;
    for (struct register_field *f = &fields[0]; f->size; f++) {
        if (value & (1 << f->bit)) {
            if (!printed_any) {
                printf(" (");
            }
            printf("%s, ", f->description);
            printed_any = 1;
        }
    }
    if (printed_any) {
        printf("\b\b)\n");
    } else {
        printf("\n");
    }
}

void
dump_cdma_registers(void)
{
    uint32_t value;

    value = dma_get(CDMACR);
    print_register("CDMACR", value, CdmaCrFields);

    value = dma_get(CDMASR);
    print_register("CDMASR", value, CdmaSrFields);

    value = dma_get(CURDESC_PNTR);
    format_register("CURDESC_PNTR", value);

    value = dma_get(CURDESC_PNTR_MSB);
    format_register("CURDESC_PNTR_MSB", value);

    value = dma_get(TAILDESC_PNTR);
    format_register("TAILDESC_PNTR", value);

    value = dma_get(TAILDESC_PNTR_MSB);
    format_register("TAILDESC_PNTR_MSB", value);

    value = dma_get(SA);
    format_register("SA", value);

    value = dma_get(SA_MSB);
    format_register("SA_MSB", value);

    value = dma_get(DA);
    format_register("DA", value);

    value = dma_get(DA_MSB);
    format_register("DA_MSB", value);

    value = dma_get(BTT);
    format_register("BTT", value);
}

void
accelerator_dump_registers(void)
{
    uint32_t value;

    for (size_t i = 0; i < sizeof(FpgaRegisters) / sizeof(FpgaRegisters[0]); i++) {
        struct register_description reg_descr = FpgaRegisters[i];
        value = FpgaBase[reg_descr.offset];
        if (reg_descr.offset == FPGA_OFFS_AP_CTRL) {
            print_register("Control signals", value, FpgaCtrlFields);
        } else {
            format_register(reg_descr.name, value);
        }
    }
    dump_cdma_registers();
}

static void
dma_transfer(uint32_t src, uint32_t dst, uint32_t bytes)
{
    static const char BramStr[] = "BRAM";
    static const char OcmStr[] = "OCM";

    const char *from __attribute__((unused)) = ((src & OCM) == OCM) ? OcmStr : BramStr;
    const char *to __attribute__((unused)) = ((dst & OCM) == OCM) ? OcmStr : BramStr;

    if (from == BramStr) {
        assert(src + bytes <= BRAM_CDMA + BRAM_SIZE);
        assert(dst + bytes <= OCM + 0x10000);
    } else {
        assert(dst + bytes <= BRAM_CDMA + BRAM_SIZE);
        assert(src + bytes <= OCM + 0x10000);
    }

    log_debug("Transfer %u bytes from %s to %s (0x%.8x -> 0x%.8x)", bytes, from, to, src, dst);

    long start = get_time_usec();
    cdma_sync();
    dma_set(SA, src);
    dma_set(DA, dst);
    uint32_t cdmacr = dma_get(CDMACR);
    cdmacr &= (~(1 << 12));
    dma_set(CDMACR, cdmacr);
    dma_set(BTT, bytes);
    cdma_sync();
    long elapsed __attribute__((unused)) = get_time_usec() - start;
    log_debug("Complete in %g ms", elapsed / 1000.f);
}

// Signal handler for receiving events from the FPGA driver.
static void
fpga_signal_handler(int signo)
{
    // Confirm correct signal #
    assert(signo == FPGA_SIGNAL);

    // Timestamp for latency measurements.
    gettimeofday(&SignalTimestamp, NULL);

    FpgaSignalCount++;
    FpgaSignalProcessed = 1;

    log_debug("FPGA interrupt done");
}

// Signal handler I would use for the CDMA interrupt... IF I HAD ONE.
static void
cdma_signal_handler(int signo)
{
    // Confirm correct signal #
    assert(signo == CDMA_SIGNAL);

    // Timestamp for latency measurements.
    gettimeofday(&SignalTimestamp, NULL);

    CdmaSignalCount++;
    CdmaSignalProcessed = 1;

    log_debug("CDMA interrupt done");
}

static int
map_addresses()
{
    log_info("Mapping addresses");

    // Open /dev/mem which represents the whole physical memory
    int dh = open("/dev/mem", O_RDWR | O_SYNC);

    if (dh < 0) {
        log_perror("open(/dev/mem)");
        return 1;
    }

    CdmaBase = mmap(NULL, CDMA_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, CDMA);
    if (!CdmaBase) {
        log_perror("mmap(CDMA)");
        return 1;
    }

    OcmBase = mmap(NULL, OCM_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, OCM);
    if (!OcmBase) {
        log_perror("mmap(OCM)");
        return 1;
    }

    FpgaBase = mmap(NULL, FPGA_SIZE, MMAP_PERMISSIONS, MMAP_FLAGS, dh, FPGA_BASE);
    if (!FpgaBase) {
        log_perror("mmap(FPGA)");
        return 1;
    }

    if (close(dh)) {
        log_perror("close(/dev/mem)");
        return 1;
    }

    return 0;
}

static int
setup_signals(void)
{
    log_info("Setting up signals");

    // Set this process to receive signals from the kernel modules.
    pid_t pid = getpid();

    if (pid < 0) {
        log_perror("getpid");
        return 1;
    }

    // We need interrupts for both CDMA and the FPGA.
    for (size_t i = 0; i < sizeof(DevHandlers) / sizeof(DevHandlers[0]); i++) {
        const struct kernel_handler *kh = &DevHandlers[i];

        if (kh->signo == CDMA_SIGNAL) {
            log_info("Skipping CDMA signal");
            continue;
        }

        log_info("Setting up signal handler for device %s", kh->dev_path);

        struct sigaction dev_sigaction;

        memset(&dev_sigaction, 0, sizeof(dev_sigaction));
        dev_sigaction.sa_handler = kh->handler;

        // Block signals while our signal handler is executing.
        (void)sigfillset(&dev_sigaction.sa_mask);

        // Register the handler.
        if (sigaction(kh->signo, &dev_sigaction, NULL) < 0) {
            log_perror("sigaction");
            return 1;
        }

        // Open the FPGA/CDMA device file.
        int dev_fd = open(kh->dev_path, O_RDWR);

        if (dev_fd < 0) {
            log_perror("open(dev_path)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETOWN, pid)) {
            log_perror("fcntl(F_SETOWN)");
            return 1;
        }

        // Enable reception of signals for the kernel module.
        int fd_flags = fcntl(dev_fd, F_GETFL);

        if (fd_flags < 0) {
            log_perror("fcntl(GETFL)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETFL, fd_flags | O_ASYNC) < 0) {
            log_perror("fcntl(SETFL)");
            return 1;
        }

        if (fcntl(dev_fd, F_SETSIG, kh->signo)) {
            log_perror("fcntl(SETSIG)");
            return 1;
        }

        FpgaFd = dev_fd;
    }

    return 0;
}

static void
enable_interrupts(void)
{
    log_spew("Enabling FPGA interrupts");
    FpgaBase[FPGA_OFFS_GIE] = 1; // Enable
    FpgaBase[FPGA_OFFS_IER] = 1; // Enable for ap_done but not ap_ready
}

void
accelerator_wait_for_interrupt(void)
{
    while (!FpgaSignalProcessed);

    FpgaSignalProcessed = 0;

    struct timeval elapsed;
    timersub(&SignalTimestamp, &StartTimestamp, &elapsed);

    log_debug("Finished in %ld us\n", 1000000 * elapsed.tv_sec + elapsed.tv_usec);
}

void
accelerator_yield_for_interrupt(void)
{
    log_debug("Waiting for interrupt");

    while (!FpgaSignalProcessed) {
        usleep(10);
    }

    FpgaSignalProcessed = 0;

    struct timeval elapsed;
    timersub(&SignalTimestamp, &StartTimestamp, &elapsed);

    log_debug("Finished in %ld us\n", 1000000 * elapsed.tv_sec + elapsed.tv_usec);
}

void accelerator_setup_buffer1(unsigned int M, unsigned int N, unsigned int K, unsigned int newM, unsigned int B1, unsigned int C1)
{
    FpgaBase[FPGA_OFFS_M1_DATA] = M;
    FpgaBase[FPGA_OFFS_K1_DATA] = K;
    FpgaBase[FPGA_OFFS_N1_DATA] = N;
    FpgaBase[FPGA_OFFS_NEW_M1_DATA] = newM;
    FpgaBase[FPGA_OFFS_B1_SP_START_DATA] = B1;
    FpgaBase[FPGA_OFFS_C1_SP_START_DATA] = C1;
}

void accelerator_start_buffer1()
{
    gettimeofday(&StartTimestamp, NULL);
    FpgaSignalProcessed = 0;
    FpgaBase[FPGA_OFFS_START1_DATA] = 1;
    FpgaBase[FPGA_OFFS_AP_CTRL] |= (1 << FpgaCtrlFieldStart);
}

void accelerator_update_c1_sp(unsigned int c1_sp)
{
    FpgaBase[FPGA_OFFS_C1_SP_START_DATA] = c1_sp;
}

// One bank of OCM.
const int BytesPerDma = 65536;

void
accelerator_send_array(uint32_t dst, void *src, size_t bytes)
{
    assert(bytes <= BRAM_SIZE);

    uint32_t chunk_count = (bytes + BytesPerDma - 1) / BytesPerDma;
    uint32_t chunk, start, end, chunk_bytes;
    for (chunk = 0, start = 0; chunk < chunk_count; chunk++, start += BytesPerDma) {
        end = start + BytesPerDma;
        if (end > bytes) {
            end = bytes;
        }
        chunk_bytes = end - start;

        // Copy to OCM.
        long start_time = get_time_usec();
        void *src_start = (void *)(((uintptr_t)(void *)src) + start);
        memcpy(OcmBase, src_start, chunk_bytes);
        long elapsed __attribute__((unused)) = get_time_usec() - start_time;
        log_debug("memcpy to OCM in %g ms", elapsed / 1000.f);

        // DMA to accelerator.
        dma_transfer(OCM, dst + start, chunk_bytes);
    }
}

void
accelerator_get_result(int *dst, uint32_t src, size_t bytes)
{
    assert(bytes <= BRAM_SIZE);

    uint32_t chunk_count = (bytes + BytesPerDma - 1) / BytesPerDma;
    uint32_t chunk, start, end, chunk_bytes;
    for (chunk = 0, start = 0; chunk < chunk_count; chunk++, start += BytesPerDma) {
        end = start + BytesPerDma;
        if (end > bytes) {
            end = bytes;
        }
        chunk_bytes = end - start;

        // DMA from accelerator.
        dma_transfer(src + start, OCM, chunk_bytes);

        // Copy from OCM.
        long start_time = get_time_usec();
        void *dst_start = (void *)(((uintptr_t)(void *)dst) + start);
        memcpy(dst_start, OcmBase, chunk_bytes);
        long elapsed __attribute__((unused)) = get_time_usec() - start_time;
        log_debug("memcpy from OCM in %g ms", elapsed / 1000.f);
    }
}

void
accelerator_init(void)
{
    if (map_addresses()) {
        exit(EXIT_FAILURE);
    }

    FpgaSignalCount = 0;
    FpgaSignalProcessed = 0;
    CdmaSignalCount = 0;
    CdmaSignalProcessed = 0;

    FpgaBase[FPGA_OFFS_AP_CTRL] = 0;
    FpgaBase[FPGA_OFFS_GIE] = 0;
    FpgaBase[FPGA_OFFS_IER] = 0;
    FpgaBase[FPGA_OFFS_ISR] = 0;
    FpgaBase[FPGA_OFFS_START1_DATA] = 0;
    FpgaBase[FPGA_OFFS_M1_DATA] = 0;
    FpgaBase[FPGA_OFFS_K1_DATA] = 0;
    FpgaBase[FPGA_OFFS_N1_DATA] = 0;
    FpgaBase[FPGA_OFFS_NEW_M1_DATA] = 0;
    FpgaBase[FPGA_OFFS_B1_SP_START_DATA] = 0;
    FpgaBase[FPGA_OFFS_C1_SP_START_DATA] = 0;

    if (setup_signals()) {
        exit(EXIT_FAILURE);
    }

    cdma_reset();

    enable_interrupts();
}

void
accelerator_finish(void)
{
    FpgaBase[FPGA_OFFS_AP_CTRL] &= 0;
    FpgaBase[FPGA_OFFS_GIE] &= 0;
    FpgaBase[FPGA_OFFS_IER] &= 0;
    FpgaBase[FPGA_OFFS_ISR] &= 0;
    FpgaBase[FPGA_OFFS_START1_DATA] &= 0;

    if (munmap((void *)FpgaBase, FPGA_SIZE)) {
        log_perror("munmap(FPGA)");
        exit(EXIT_FAILURE);
    }

    if (munmap((void *)CdmaBase, CDMA_SIZE)) {
        log_perror("munmap(CDMA)");
        exit(EXIT_FAILURE);
    }

    if (munmap(OcmBase, OCM_SIZE)) {
        log_perror("munmap(OCM)");
        exit(EXIT_FAILURE);
    }

    if (close(FpgaFd)) {
        log_perror("close(FPGA)");
        exit(EXIT_FAILURE);
    }
}
