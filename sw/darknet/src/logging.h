#ifndef __LOGGING_H__
#define __LOGGING_H__

#include <stdbool.h>
#include <stdio.h>

#define LEVEL_NONE    0
#define LEVEL_ERROR   1
#define LEVEL_WARNING 2
#define LEVEL_TIME    3
#define LEVEL_INFO    4
#define LEVEL_DEBUG   5
#define LEVEL_SPEW    6


#ifndef LOGGING_LEVEL
#define LOGGING_LEVEL LEVEL_ERROR
#endif // LOGGING_LEVEL

static const char *RedBoldface = "\033[31;1m";
static const char *YellowBoldface = "\033[33;1m";
static const char *CyanBoldface = "\033[36;1m";
static const char *BlueBoldface = "\033[34;1m";
static const char *BrightBoldface = "\033[97;1m";
static const char *GreyItalics = "\033[37;2;3m";
static const char *DefaultFormat = "\033[0m";

/**
 * Will the logger log a message at this level?
 *
 * Useful for guarding calls that have bare printf()
 */
bool
will_log(
    int level
);

static inline const char __attribute__((used)) *
color_for_level(int level)
{
    switch (level) {
    case LEVEL_ERROR:   return RedBoldface;
    case LEVEL_WARNING: return YellowBoldface;
    case LEVEL_TIME:    return CyanBoldface;
    case LEVEL_INFO:    return BlueBoldface;
    case LEVEL_DEBUG:   return BrightBoldface;
    case LEVEL_SPEW:    return GreyItalics;
    default:            return DefaultFormat;
    }
}

static inline char __attribute__((used))
letter_for_level(int level)
{
    switch (level) {
    case LEVEL_ERROR:   return 'E';
    case LEVEL_WARNING: return 'W';
    case LEVEL_TIME:    return 'T';
    case LEVEL_INFO:    return 'I';
    case LEVEL_DEBUG:   return 'D';
    case LEVEL_SPEW:    return 'S';
    default:            return '?';
    }
}

#define log_msg(level, ...)                     \
    do {                                        \
        fprintf(                                \
    stderr,                                     \
    "%s-%c- [%s] ",                             \
    color_for_level(level),                     \
    letter_for_level(level),                    \
    __func__                                    \
        );                                      \
        fprintf(stderr, ## __VA_ARGS__);        \
        fprintf(stderr, "%s\n", DefaultFormat); \
    } while (0);

#define log_perror(msg)                       \
    do {                                      \
        fprintf(                              \
    stderr,                                   \
    "%s-E- [%s] ",                            \
    color_for_level(LEVEL_ERROR),             \
    __func__                                  \
        );                                    \
        perror(msg);                          \
        fprintf(stderr, "%s", DefaultFormat); \
    } while (0);

#if LOGGING_LEVEL >= LEVEL_ERROR
#define log_error(...) log_msg(LEVEL_ERROR, ## __VA_ARGS__)
#else
#define log_error(...)
#endif

#if LOGGING_LEVEL >= LEVEL_WARNING
#define log_warning(...) log_msg(LEVEL_WARNING, ## __VA_ARGS__)
#else
#define log_warning(...)
#endif

#if LOGGING_LEVEL >= LEVEL_TIME
#define log_time(...) log_msg(LEVEL_TIME, ## __VA_ARGS__)
#define log_timestamp(event, usec) log_msg(LEVEL_TIME, "%s: %.3f ms", event, (double)usec / 1e3)
#else
#define log_time(...)
#define log_timestamp(event, usec)
#endif

#if LOGGING_LEVEL >= LEVEL_INFO
#define log_info(...) log_msg(LEVEL_INFO, ## __VA_ARGS__)
#else
#define log_info(...)
#endif

#if LOGGING_LEVEL >= LEVEL_DEBUG
#define log_debug(...) log_msg(LEVEL_DEBUG, ## __VA_ARGS__)
#else
#define log_debug(...)
#endif

#if LOGGING_LEVEL >= LEVEL_SPEW
#define log_spew(...) log_msg(LEVEL_SPEW, ## __VA_ARGS__)
#else
#define log_spew(...)
#endif
#endif // __LOGGING_H__
