#ifndef __TIME_H__
#define __TIME_H__

#include <stdlib.h>
#include <sys/time.h>

#ifdef TERSE
#define get_time_ms() 0.f
#else // not def TERSE
static inline double get_time_ms()
{
    struct timeval tv;
    double msecs;

    gettimeofday(&tv, NULL);

    msecs = tv.tv_sec * 1000. + tv.tv_usec / 1000.;

    return msecs;
}
#endif // TERSE

#endif // __TIME_H__
