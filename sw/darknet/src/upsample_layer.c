#include "fixed_configuration.h"
#include "printd.h"
#include "time.h"
#include "upsample_layer.h"
#include "utils.h"

#include <stdio.h>

void
upsample_cpu_fx(short *in, int w, int h, int c, short *out)
{
    // I've hard-coded some values for the singular upsample layer we have. This helps reduce
    // instruction count, eliminate some branches, and expose opportunities for constant-folding.
    //
    // This layer takes ~1 ms, so the overhead of OpenMP would probably exceed any benefits we might
    // reap.
    for (int k = 0; k < c; k++) {
        for (int j = 0; j < h * 2; j++) {
            for (int i = 0; i < w * 2; i++) {
                int in_index = k * w * h + (j / 2) * w + i / 2;
                int out_index = k * w * h * 2 * 2 + j * w * 2 + i;
                out[out_index] = in[in_index];
            }
        }
    }
}

void
forward_upsample_layer_fx(const layer l, network_state net)
{
    static int upsample_count = 0;

    upsample_count += 1;

    double ms_start = get_time_ms();

    printd("[UPSA %2d]                                                        ", upsample_count);

    memset(l.output, 0, l.outputs * l.batch * sizeof(short));
    upsample_cpu_fx((short *)net.input, l.w, l.h, l.c, (short *)l.output);

    double ms_end = get_time_ms();
    double elapsed = ms_end - ms_start;

    printd("%3.0f ms\n", elapsed);
}

layer
make_upsample_layer(int batch, int w, int h, int c, int stride)
{
    layer l = { (LAYER_TYPE)0 };

    l.type = UPSAMPLE;
    l.batch = batch;
    l.w = w;
    l.h = h;
    l.c = c;
    l.out_w = w * stride;
    l.out_h = h * stride;
    l.out_c = c;
    if (stride < 0) {
        stride = -stride;
        l.reverse = 1;
        l.out_w = w / stride;
        l.out_h = h / stride;
    }
    l.stride = stride;
    l.outputs = l.out_w * l.out_h * l.out_c;
    l.inputs = l.w * l.h * l.c;
    l.delta = (float *)xcalloc(l.outputs * batch, sizeof(float));
    l.output = (float *)xcalloc(l.outputs * batch, sizeof(float));

    l.forward = forward_upsample_layer_fx;
    l.backward = NULL;
    if (l.reverse) {
        fprintf(stderr, "downsample              %2dx  %4d x%4d x%4d -> %4d x%4d x%4d\n",
                stride, w, h, c, l.out_w, l.out_h, l.out_c);
    } else {
        fprintf(stderr, "upsample                %2dx  %4d x%4d x%4d -> %4d x%4d x%4d\n",
                stride, w, h, c, l.out_w, l.out_h, l.out_c);
    }
    return l;
}

void
resize_upsample_layer(layer *l, int w, int h)
{
    l->w = w;
    l->h = h;
    l->out_w = w * l->stride;
    l->out_h = h * l->stride;
    if (l->reverse) {
        l->out_w = w / l->stride;
        l->out_h = h / l->stride;
    }
    l->outputs = l->out_w * l->out_h * l->out_c;
    l->inputs = l->h * l->w * l->c;
    l->delta = (float *)xrealloc(l->delta, l->outputs * l->batch * sizeof(float));
    l->output = (float *)xrealloc(l->output, l->outputs * l->batch * sizeof(float));
}
