#include "blas.h"
#include "utils.h"

#include <math.h>
#include <assert.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void flatten(float *x, int size, int layers, int batch, int forward)
{
    float* swap = (float*)xcalloc(size * layers * batch, sizeof(float));
    int i,c,b;
    for(b = 0; b < batch; ++b){
        for(c = 0; c < layers; ++c){
            for(i = 0; i < size; ++i){
                int i1 = b*layers*size + c*size + i;
                int i2 = b*layers*size + i*layers + c;
                if (forward) swap[i2] = x[i1];
                else swap[i1] = x[i2];
            }
        }
    }
    memcpy(x, swap, size*layers*batch*sizeof(float));
    free(swap);
}

void axpy_cpu(int N, float ALPHA, float *X, int INCX, float *Y, int INCY)
{
    int i;
    for(i = 0; i < N; ++i) Y[i*INCY] += ALPHA*X[i*INCX];
}

void fill_cpu(int N, float ALPHA, float *X, int INCX)
{
    int i;
    if (INCX == 1 && ALPHA == 0) {
        memset(X, 0, N * sizeof(float));
    }
    else {
        for (i = 0; i < N; ++i) X[i*INCX] = ALPHA;
    }
}

void copy_cpu(int N, float *X, int INCX, float *Y, int INCY)
{
    int i;
    for(i = 0; i < N; ++i) Y[i*INCY] = X[i*INCX];
}

void softmax(float *input, int n, float temp, float *output, int stride)
{
    int i;
    float sum = 0;
    float largest = -FLT_MAX;
    for(i = 0; i < n; ++i){
        if(input[i*stride] > largest) largest = input[i*stride];
    }
    for(i = 0; i < n; ++i){
        float e = exp(input[i*stride]/temp - largest/temp);
        sum += e;
        output[i*stride] = e;
    }
    for(i = 0; i < n; ++i){
        output[i*stride] /= sum;
    }
}

void get_embedding(float *src, int src_w, int src_h, int src_c, int embedding_size, int cur_w, int cur_h, int cur_n, int cur_b, float *dst)
{
    int i;
    for (i = 0; i < embedding_size; ++i) {
        const int src_index = cur_b*(src_c*src_h*src_w) + cur_n*(embedding_size*src_h*src_w) + i*src_h*src_w + cur_h*(src_w) + cur_w;

        const float val = src[src_index];
        dst[i] = val;
    }
}

float cosine_similarity(float *A, float *B, unsigned int feature_size)
{
    float mul = 0.0, d_a = 0.0, d_b = 0.0;

    int i;
    for(i = 0; i < feature_size; ++i)
    {
        mul += A[i] * B[i];
        d_a += A[i] * A[i];
        d_b += B[i] * B[i];
    }
    float similarity;
    float divider = sqrtf(d_a) * sqrtf(d_b);
    if (divider > 0) similarity = mul / divider;
    else similarity = 0;

    return similarity;
}

float find_sim(size_t i, size_t j, contrastive_params *contrast_p, int contrast_p_size)
{
    size_t z;
    for (z = 0; z < contrast_p_size; ++z) {
        if (contrast_p[z].i == i && contrast_p[z].j == j) break;
    }
    if (z == contrast_p_size) {
        printf(" Error: find_sim(): sim isn't found: i = %lu, j = %lu, z = %lu \n", i, j, z);
        getchar();
    }

    return contrast_p[z].sim;
}
