#ifndef __PRINTD_H__
#define __PRINTD_H__

// #define TERSE

#ifdef TERSE
#define printd(...)
#else
#define printd(...) fprintf(stderr, __VA_ARGS__)
#endif // TERSE

#endif // __PRINTD_H__
