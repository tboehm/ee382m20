#ifndef ACTIVATIONS_H
#define ACTIVATIONS_H
#include "darknet.h"
#include "math.h"
#include "utils.h"

#ifdef __cplusplus
extern "C" {
#endif
ACTIVATION get_activation(char *s);

char *get_activation_string(ACTIVATION a);


// static inline float linear_activate(float x){return x;}
static inline float logistic_activate(float x){return 1.f/(1.f + expf(-x));}
// static inline float leaky_activate(float x){return (x>0) ? x : .1f*x;}

// static inline float linear_gradient(float x){return 1;}
static inline float logistic_gradient(float x){return (1-x)*x;}
// static inline float leaky_gradient(float x){return (x>0) ? 1 : .1f;}

#ifdef __cplusplus
}
#endif

#endif
