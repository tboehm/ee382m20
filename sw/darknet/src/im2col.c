#include "im2col.h"
#include "fixed_configuration.h"
#include "gemm.h"

#include <stdio.h>

// Function uses casting from int to unsigned to compare if value of
// parameter a is greater or equal to zero and lower than value of
// parameter b. The b parameter is of type signed and is always positive,
// therefore its value is always lower than 0x800... where casting
// negative value of a parameter converts it to value higher than 0x800...
// The casting allows to use one condition instead of two.
inline static int is_a_ge_zero_and_a_lt_b(int a, int b) {
    return (unsigned)(a) < (unsigned)(b);
}

static inline int
get_out_row(int chan, int ker_r, int ker_c, int channels)
{ 
    return (channels - 1 - chan) * 9 + ker_r * 3 + ker_c;
}

static inline int
get_out_col(int out_r, int out_c, int width, int height)
{
    return width * (height - out_r) + (width - out_c);
}

void
im2col_cpu_ext_fx(const short *data_im, const int channels,
                  const int height, const int width,
                  short *data_col)
{
    const int channel_size = height * width;
    const short *data_im_base = data_im;

    #pragma omp parallel for
    for (int i = 1; i <= channels; i++) {
        int channel = channels - i;
        data_im = data_im_base + (channels - channel - 1) * channel_size;
        for (int kernel_row = 0; kernel_row < 3; kernel_row++) {
            for (int kernel_col = 0; kernel_col < 3; kernel_col++) {
                int input_row = kernel_row - 1;
                int row = get_out_row(channel, kernel_row, kernel_col, channels);
                for (int output_rows = height; output_rows; output_rows--, input_row++) {
                    if (!is_a_ge_zero_and_a_lt_b(input_row, height)) {
                        for (int output_col = width; output_col; output_col--) {
                            int col = get_out_col(output_rows, output_col, width, height);
                            int out_index = width * height * row + col;
                            data_col[out_index] = 0;
                        }
                    } else {
                        int input_col = kernel_col - 1;
                        for (int output_col = width; output_col; output_col--, input_col++) {
                            int col = get_out_col(output_rows, output_col, width, height);
                            int out_index = width * height * row + col;
                            if (is_a_ge_zero_and_a_lt_b(input_col, width)) {
                                data_col[out_index] = data_im[input_row * width + input_col];
                            }
                            else {
                                data_col[out_index] = 0;
                            }
                        }
                    }
                }
            }
        }
    }
}

// Fortunately for us, all kernels are 3x3.
#define KER_SIZE 3

// Return the transpose of im2col(data_im)
//
// The first branch of the conditional doesn't depend on any col variable, and none of them depend
// on channel. However, breaking them out seems to interfere with the compiler's ability to make
// good optimizations.
void
im2col_of_transpose(const short *data_im, const int channels,
                    const int height, const int width,
                    short *data_col)
{
#pragma omp parallel for
    for (int kernel_row = 0; kernel_row < KER_SIZE; kernel_row++) {
        for (int input_row = kernel_row - 1, output_row = height; output_row; output_row--, input_row++) {
            for (int kernel_col = 0; kernel_col < KER_SIZE; kernel_col++) {
                for (int output_col = width, input_col = kernel_col - 1; output_col; output_col--, input_col++) {
                    int col = get_out_col(output_row, output_col, width, height);
                    for (int channel = channels - 1; channel >= 0; channel--) {
                        int row = get_out_row(channel, kernel_row, kernel_col, channels);
                        int out_index = 9 * channels * col + row;
                        if (!is_a_ge_zero_and_a_lt_b(input_row, height)) {
                            data_col[out_index] = 0;
                        } else if (is_a_ge_zero_and_a_lt_b(input_col, width)) {
                            int in_index = input_row * channels * width + input_col * channels + (channels - channel - 1);
                            data_col[out_index] = data_im[in_index];
                        } else {
                            data_col[out_index] = 0;
                        }
                    }
                }
            }
        }
    }
}

void __attribute__((hot))
im2col_layer_1(const short *data_im, const int channels,
               const int height, const int width,
               short *data_col)
{
    #pragma omp parallel for
    for (int kernel_row = 0; kernel_row < KER_SIZE; kernel_row++) {
        for (int input_row = kernel_row - 1, output_row = height; output_row; output_row--, input_row++) {
            for (int kernel_col = 0; kernel_col < KER_SIZE; kernel_col++) {
                for (int output_col = width, input_col = kernel_col - 1; output_col; output_col--, input_col++) {
                    int col = get_out_col(output_row, output_col, width, height);
                    for (int channel = channels - 1; channel >= 0; channel--) {
                        int row = get_out_row(channel, kernel_row, kernel_col, channels);
                        int out_index = LAYER_1_K * col + row;
                        if (!is_a_ge_zero_and_a_lt_b(input_row, height)) {
                            data_col[out_index] = 0;
                        } else if (is_a_ge_zero_and_a_lt_b(input_col, width)) {
                            int in_index = input_row * channels * width + input_col * channels + (channels - channel - 1);
                            data_col[out_index] = data_im[in_index];
                        } else {
                            data_col[out_index] = 0;
                        }
                    }
                }
            }
        }
    }
}
