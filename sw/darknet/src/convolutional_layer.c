#include "convolutional_layer.h"
#include "utils.h"
#include "im2col.h"
#include "blas.h"
#include "gemm.h"
#include "box.h"
#include "fixed_configuration.h"
#include "time.h"
#include "printd.h"

#include <stdio.h>

int convolutional_out_height(convolutional_layer l)
{
    return (l.h + 2*l.pad - l.size) / l.stride_y + 1;
}

int convolutional_out_width(convolutional_layer l)
{
    return (l.w + 2*l.pad - l.size) / l.stride_x + 1;
}

size_t get_workspace_size32(layer l){
    if (l.xnor) {
        size_t re_packed_input_size = l.c * l.w * l.h * sizeof(float);
        size_t workspace_size = (size_t)l.bit_align*l.size*l.size*l.c * sizeof(float);
        if (workspace_size < re_packed_input_size) workspace_size = re_packed_input_size;
        return workspace_size;
    }
    return (size_t)l.out_h*l.out_w*l.size*l.size*(l.c / l.groups)*sizeof(float);
}

size_t get_workspace_size16(layer l) {
    return 0;
}

size_t get_convolutional_workspace_size(layer l) {
    size_t workspace_size = get_workspace_size32(l);
    size_t workspace_size16 = get_workspace_size16(l);
    if (workspace_size16 > workspace_size) workspace_size = workspace_size16;
    return workspace_size;
}


void free_convolutional_batchnorm(convolutional_layer *l)
{
    if (!l->share_layer) {
        if (l->scales)          free(l->scales),            l->scales = NULL;
        if (l->scale_updates)   free(l->scale_updates),     l->scale_updates = NULL;
        if (l->mean)            free(l->mean),              l->mean = NULL;
        if (l->variance)        free(l->variance),          l->variance = NULL;
        if (l->mean_delta)      free(l->mean_delta),        l->mean_delta = NULL;
        if (l->variance_delta)  free(l->variance_delta),    l->variance_delta = NULL;
        if (l->rolling_mean)    free(l->rolling_mean),      l->rolling_mean = NULL;
        if (l->rolling_variance) free(l->rolling_variance),  l->rolling_variance = NULL;
        if (l->x)               free(l->x),                 l->x = NULL;
        if (l->x_norm)          free(l->x_norm),            l->x_norm = NULL;
    }
}

convolutional_layer make_convolutional_layer(int batch, int steps, int h, int w, int c, int n, int groups, int size, int stride_x, int stride_y, int dilation, int padding, ACTIVATION activation, int batch_normalize, int binary, int xnor, int adam, int use_bin_output, int index, int antialiasing, convolutional_layer *share_layer, int assisted_excitation, int deform, int train)
{
    int total_batch = batch*steps;
    int i;
    convolutional_layer l = { (LAYER_TYPE)0 };
    l.type = CONVOLUTIONAL;
    l.train = train;

    if (xnor) groups = 1;   // disable groups for XNOR-net
    if (groups < 1) groups = 1;

    const int blur_stride_x = stride_x;
    const int blur_stride_y = stride_y;
    l.antialiasing = antialiasing;
    if (antialiasing) {
        stride_x = stride_y = l.stride = l.stride_x = l.stride_y = 1; // use stride=1 in host-layer
    }

    l.deform = deform;
    l.assisted_excitation = assisted_excitation;
    l.share_layer = share_layer;
    l.index = index;
    l.h = h;
    l.w = w;
    l.c = c;
    l.groups = groups;
    l.n = n;
    l.binary = binary;
    l.xnor = xnor;
    l.use_bin_output = use_bin_output;
    l.batch = batch;
    l.steps = steps;
    l.stride = stride_x;
    l.stride_x = stride_x;
    l.stride_y = stride_y;
    l.dilation = dilation;
    l.size = size;
    l.pad = padding;
    l.batch_normalize = batch_normalize;
    l.learning_rate_scale = 1;
    l.nweights = (c / groups) * n * size * size;

    if (l.share_layer) {
        if (l.size != l.share_layer->size || l.nweights != l.share_layer->nweights || l.c != l.share_layer->c || l.n != l.share_layer->n) {
            printf(" Layer size, nweights, channels or filters don't match for the share_layer");
            getchar();
        }

        l.weights = l.share_layer->weights;
        l.weight_updates = l.share_layer->weight_updates;

        l.biases = l.share_layer->biases;
        l.bias_updates = l.share_layer->bias_updates;
    }
    else {
        l.weights = (float*)xcalloc(l.nweights, sizeof(float));
        l.biases = (float*)xcalloc(n, sizeof(float));

        int nweights_alloc = l.nweights;
        if (n & 1) {
            // Pad layers with odd M.
            nweights_alloc += (c / groups) * size * size;
        };
        l.fixed_weights = (short *)xcalloc(nweights_alloc, sizeof(short));
        l.fixed_biases = (short *)xcalloc(n, sizeof(short));

        if (train) {
            l.weight_updates = (float*)xcalloc(l.nweights, sizeof(float));
            l.bias_updates = (float*)xcalloc(n, sizeof(float));

            l.weights_ema = (float*)xcalloc(l.nweights, sizeof(float));
            l.biases_ema = (float*)xcalloc(n, sizeof(float));
        }
    }

    int out_h = convolutional_out_height(l);
    int out_w = convolutional_out_width(l);
    l.out_h = out_h;
    l.out_w = out_w;
    l.out_c = n;
    l.outputs = l.out_h * l.out_w * l.out_c;
    l.inputs = l.w * l.h * l.c;
    l.activation = activation;

    int output_alloc = total_batch * l.outputs;
    if (n & 1) {
        // Pad layers with odd M.
        output_alloc += l.out_h * l.out_w;
    }

    l.output = (float*)xcalloc(output_alloc, sizeof(float));
#ifndef GPU
    if (train) l.delta = (float*)xcalloc(total_batch*l.outputs, sizeof(float));
#endif  // not GPU

    l.forward = forward_convolutional_layer;
    l.backward = NULL;
    l.update = NULL;
    if(binary){
        l.binary_weights = (float*)xcalloc(l.nweights, sizeof(float));
        l.cweights = (char*)xcalloc(l.nweights, sizeof(char));
        l.scales = (float*)xcalloc(n, sizeof(float));
    }
    if(xnor){
        l.binary_weights = (float*)xcalloc(l.nweights, sizeof(float));
        l.binary_input = (float*)xcalloc(l.inputs * l.batch, sizeof(float));

        int align = 32;// 8;
        int src_align = l.out_h*l.out_w;
        l.bit_align = src_align + (align - src_align % align);

        l.mean_arr = (float *)xcalloc(l.n, sizeof(float));

        const size_t new_c = l.c / 32;
        size_t in_re_packed_input_size = new_c * l.w * l.h + 1;
        l.bin_re_packed_input = (uint32_t*)xcalloc(in_re_packed_input_size, sizeof(uint32_t));

        l.lda_align = 256;  // AVX2
        int k = l.size*l.size*l.c;
        size_t k_aligned = k + (l.lda_align - k%l.lda_align);
        size_t t_bit_input_size = k_aligned * l.bit_align / 8;
        l.t_bit_input = (char*)xcalloc(t_bit_input_size, sizeof(char));
    }

    if(batch_normalize){
        if (l.share_layer) {
            l.scales = l.share_layer->scales;
            l.scale_updates = l.share_layer->scale_updates;
            l.mean = l.share_layer->mean;
            l.variance = l.share_layer->variance;
            l.mean_delta = l.share_layer->mean_delta;
            l.variance_delta = l.share_layer->variance_delta;
            l.rolling_mean = l.share_layer->rolling_mean;
            l.rolling_variance = l.share_layer->rolling_variance;
        }
        else {
            l.scales = (float*)xcalloc(n, sizeof(float));
            for (i = 0; i < n; ++i) {
                l.scales[i] = 1;
            }
            if (train) {
                l.scales_ema = (float*)xcalloc(n, sizeof(float));
                l.scale_updates = (float*)xcalloc(n, sizeof(float));

                l.mean = (float*)xcalloc(n, sizeof(float));
                l.variance = (float*)xcalloc(n, sizeof(float));

                l.mean_delta = (float*)xcalloc(n, sizeof(float));
                l.variance_delta = (float*)xcalloc(n, sizeof(float));
            }
            l.rolling_mean = (float*)xcalloc(n, sizeof(float));
            l.rolling_variance = (float*)xcalloc(n, sizeof(float));
        }

#ifndef GPU
        if (train) {
            l.x = (float*)xcalloc(total_batch * l.outputs, sizeof(float));
            l.x_norm = (float*)xcalloc(total_batch * l.outputs, sizeof(float));
        }
#endif  // not GPU
    }

#ifndef GPU
    if (l.activation == SWISH || l.activation == MISH || l.activation == HARD_MISH) l.activation_input = (float*)calloc(total_batch*l.outputs, sizeof(float));
#endif  // not GPU

    if(adam){
        l.adam = 1;
        l.m = (float*)xcalloc(l.nweights, sizeof(float));
        l.v = (float*)xcalloc(l.nweights, sizeof(float));
        l.bias_m = (float*)xcalloc(n, sizeof(float));
        l.scale_m = (float*)xcalloc(n, sizeof(float));
        l.bias_v = (float*)xcalloc(n, sizeof(float));
        l.scale_v = (float*)xcalloc(n, sizeof(float));
    }

    l.workspace_size = get_convolutional_workspace_size(l);

    //fprintf(stderr, "conv  %5d %2d x%2d /%2d  %4d x%4d x%4d   ->  %4d x%4d x%4d\n", n, size, size, stride, w, h, c, l.out_w, l.out_h, l.out_c);
    l.bflops = (2.0 * l.nweights * l.out_h*l.out_w) / 1000000000.;
    if (l.xnor) l.bflops = l.bflops / 32;
    if (l.xnor && l.use_bin_output) fprintf(stderr, "convXB");
    else if (l.xnor) fprintf(stderr, "convX ");
    else if (l.share_layer) fprintf(stderr, "convS ");
    else if (l.assisted_excitation) fprintf(stderr, "convAE");
    else fprintf(stderr, "conv  ");

    if (groups > 1) fprintf(stderr, "%5d/%4d ", n, groups);
    else           fprintf(stderr, "%5d      ", n);

    if (stride_x != stride_y) fprintf(stderr, "%2dx%2d/%2dx%2d ", size, size, stride_x, stride_y);
    else {
        if (dilation > 1) fprintf(stderr, "%2d x%2d/%2d(%1d)", size, size, stride_x, dilation);
        else             fprintf(stderr, "%2d x%2d/%2d   ", size, size, stride_x);
    }

    fprintf(stderr, "%4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n", w, h, c, l.out_w, l.out_h, l.out_c, l.bflops);

    //fprintf(stderr, "%5d/%2d %2d x%2d /%2d(%d)%4d x%4d x%4d  -> %4d x%4d x%4d %5.3f BF\n", n, groups, size, size, stride, dilation, w, h, c, l.out_w, l.out_h, l.out_c, l.bflops);

    if (l.antialiasing) {
        printf("AA:  ");
        l.input_layer = (layer*)calloc(1, sizeof(layer));
        int blur_size = 3;
        int blur_pad = blur_size / 2;
        if (l.antialiasing == 2) {
            blur_size = 2;
            blur_pad = 0;
        }
        *(l.input_layer) = make_convolutional_layer(batch, steps, out_h, out_w, n, n, n, blur_size, blur_stride_x, blur_stride_y, 1, blur_pad, LINEAR, 0, 0, 0, 0, 0, index, 0, NULL, 0, 0, train);
        const int blur_nweights = n * blur_size * blur_size;  // (n / n) * n * blur_size * blur_size;
        int i;
        if (blur_size == 2) {
            for (i = 0; i < blur_nweights; i += (blur_size*blur_size)) {
                l.input_layer->weights[i + 0] = 1 / 4.f;
                l.input_layer->weights[i + 1] = 1 / 4.f;
                l.input_layer->weights[i + 2] = 1 / 4.f;
                l.input_layer->weights[i + 3] = 1 / 4.f;
            }
        }
        else {
            for (i = 0; i < blur_nweights; i += (blur_size*blur_size)) {
                l.input_layer->weights[i + 0] = 1 / 16.f;
                l.input_layer->weights[i + 1] = 2 / 16.f;
                l.input_layer->weights[i + 2] = 1 / 16.f;

                l.input_layer->weights[i + 3] = 2 / 16.f;
                l.input_layer->weights[i + 4] = 4 / 16.f;
                l.input_layer->weights[i + 5] = 2 / 16.f;

                l.input_layer->weights[i + 6] = 1 / 16.f;
                l.input_layer->weights[i + 7] = 2 / 16.f;
                l.input_layer->weights[i + 8] = 1 / 16.f;
            }
        }
        for (i = 0; i < n; ++i) l.input_layer->biases[i] = 0;
    }

    return l;
}

#define LEAKY_ALPHA ((short)(0.1 * (FX_SCALE)))

void
add_bias_and_activate_fx(short *output, short *input, short *biases, int n, int size)
{
    #pragma omp parallel for
    for (int j = 0; j < size; j++) {
        for (int i = 0; i < n; i++) {
            short fx_bias = biases[i];
            int idx = j * n + i;
            short out_fx = input[idx] + fx_bias;
            if (out_fx < 0) {
                out_fx = FX_MUL(out_fx, LEAKY_ALPHA);
            }
            output[idx] = out_fx;
        }
    }
}

void
add_bias_fx(short *output, short *input, short *biases, int n, int size)
{
    #pragma omp parallel for
    for (int j = 0; j < size; j++) {
        for (int i = 0; i < n; i++) {
            short fx_bias = biases[i];
            int idx = i * size + j;
            short out_fx = input[idx] + fx_bias;
            output[idx] = out_fx;
        }
    }
}

static void
transpose_matrix_short(short *src, short *dst, int rows, int cols)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            dst[j * rows + i] = src[i * cols + j];
        }
    }
}

void forward_convolutional_layer(convolutional_layer l, network_state state)
{
    static int conv_count = 0;

    conv_count += 1;

    // Profiling. If "TERSE" is defined, get_time_ms() evaluates to 0.f.
    double ms_start = get_time_ms();
    double im2col_start;
    double im2col_ms;
    double activate_start;
    double activate_ms;
    double elapsed_ms;

    int out_h = convolutional_out_height(l);
    int out_w = convolutional_out_width(l);

    int m = l.n / l.groups;
    int k = l.size*l.size*l.c / l.groups;
    int n = out_h*out_w;
    printd("[CONV %2d]\tA: %dx%d.  \tB: %dx%d:\t", conv_count, m, k, k, n);

    short *a = l.fixed_weights;
    short *b = (short *)state.workspace;
    short *c = (short *)l.output;

    float *im = state.input;
    short *im_short = NULL;

    bool skip_im2col = (l.size == 1 && l.stride == 1 && l.dilation == 1);
    bool input_is_fp = (conv_count == 1);

    // There's something wrong with the CONV 10 -> YOLO 1 -> ROUT 1 -> CONV 11 sequence when we
    // transpose C after CONV 10. When I inspect some random values, they look the same, but the
    // prediction comes up empty. There's already some weird magic going on with these layers and
    // the whole fixed/float thing. Plus, the overhead of transposing a 255x169 output and then a
    // 256x169 input is tiny -- maybe like 2 ms.

    bool b_is_transposed = (conv_count != 11);
    bool leave_c_transposed = (conv_count != 10 && conv_count != 13);

    im2col_start = get_time_ms();

    // 1. The first input is in floating point (and non-transposed).
    if (input_is_fp) {
        double t_start = get_time_ms();
        im_short = (short *)malloc(sizeof(short) * n * k);
        #pragma omp parallel for
        for (int j = 0; j < l.c; j++) {
            for (int i = 0; i < l.h * l.w; i++) {
                im_short[i * l.c + j] = FP_TO_FX(im[j * l.h * l.w + i]);
            }
        }
        double t_elapsed = get_time_ms() - t_start;
        printd(" [\033[32;1m%.1f ms\033[0m] ", t_elapsed);
    } else {
        im_short = (short *)im;
    }

    // 2. Many layers require im2col.
    if (skip_im2col) {
        printd(" [b = im] ");
        b = im_short;
    } else if (input_is_fp) {
        // Annoying odd case
        int orig_k = k;
        k += PAD_K_CONV_1;
        printd(" [im2col layer 1, pad k %d -> %d] ", orig_k, k);

        // Pad B with zeros while doing im2col.
        memset(b, 0, sizeof(short) * k * n);
        im2col_layer_1(im_short, l.c, l.h, l.w, b);

        // Pad A with zeros.
        short *a_padded = (short *)calloc(m * k, sizeof(short));
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < orig_k; j++) {
                a_padded[i * k + j] = a[i * orig_k + j];
            }
        }

        a = a_padded;
    } else {
        printd(" [im2col] ");
        im2col_of_transpose(im_short, l.c, l.h, l.w, b);
    }

    // 3. Not all B matrices come in transposed.
    short *B_T = NULL;
    if (!b_is_transposed) {
        B_T = (short *)malloc(sizeof(short) * n * k);
        transpose_matrix_short(b, B_T, k, n);
    } else {
        B_T = b;
    }

    im2col_ms = get_time_ms() - im2col_start;
    printd(" [\033[34;1m%.0f ms\033[0m] ", im2col_ms);

    // 4. Do the GEMM. C doesn't always stay transposed for the next layer.
    if (!leave_c_transposed) {
        // Instead of allocating a temporary buffer for the transposed result, just use the output
        // array since we'll overwrite it anyway.
        //
        // Additionally, two times we want to un-transpose C after computation are exactly the times
        // when the M dimension is odd. This is no coincidence: we want to give un-transposed
        // matrices to the YOLO layers, and the number of filters we have in the previous
        // convolutional layer is determined by the number of classes and anchors in the YOLO layer.
        // We handle the padding for A in make_convolutional_layer, but we can un-pad C here by
        // simply ignoring the last row after undoing the transposition.
        int padded_m = m + 1;
        printd(" [M -> %d] ", padded_m);
        short *C_T = (short *)xcalloc(padded_m * n, sizeof(short));
        gemm(padded_m, n, k, a, B_T, C_T);
        transpose_matrix_short(C_T, c, n, padded_m);
        free(C_T);
    } else {
        gemm(m, n, k, a, B_T, c);
    }

    if (!b_is_transposed) {
        free(B_T);
    }

    // 5. Apply the activation / add biases.
    activate_start = get_time_ms();
    if (l.activation == LEAKY) {
        printd(" [ACTIVATE] ");
        add_bias_and_activate_fx(c, c, l.fixed_biases, l.n, out_h * out_w);
    } else {
        printd(" [ADD BIAS] ");
        add_bias_fx(c, c, l.fixed_biases, l.n, out_h * out_w);
    }
    activate_ms = get_time_ms() - activate_start;
    printd(" [\033[35;1m%.0f ms\033[0m] ", activate_ms);

    if (input_is_fp) {
        free(im_short);
        free(a); // This is actually a_padded
    }

    elapsed_ms = get_time_ms() - ms_start;
    printd(" %3.0f ms\n", elapsed_ms);
}
