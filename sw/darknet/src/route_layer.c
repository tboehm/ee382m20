#include "blas.h"
#include "printd.h"
#include "route_layer.h"
#include "time.h"
#include "utils.h"

#include <stdio.h>

route_layer
make_route_layer(int batch, int n, int *input_layers, int *input_sizes, int groups, int group_id)
{
    fprintf(stderr, "route ");
    route_layer l = { (LAYER_TYPE)0 };

    l.type = ROUTE;
    l.batch = batch;
    l.n = n;
    l.input_layers = input_layers;
    l.input_sizes = input_sizes;
    l.groups = groups;
    l.group_id = group_id;
    int i;
    int outputs = 0;

    for (i = 0; i < n; ++i) {
        fprintf(stderr, " %d", input_layers[i]);
        outputs += input_sizes[i];
    }
    outputs = outputs / groups;
    l.outputs = outputs;
    l.inputs = outputs;
    l.delta = (float *)xcalloc(outputs * batch, sizeof(float));
    l.output = (float *)xcalloc(outputs * batch, sizeof(float));

    l.forward = forward_route_layer;
    l.backward = NULL;
    return l;
}

void
resize_route_layer(route_layer *l, network *net)
{
    int i;
    layer first = net->layers[l->input_layers[0]];

    l->out_w = first.out_w;
    l->out_h = first.out_h;
    l->out_c = first.out_c;
    l->outputs = first.outputs;
    l->input_sizes[0] = first.outputs;
    for (i = 1; i < l->n; ++i) {
        int index = l->input_layers[i];
        layer next = net->layers[index];
        l->outputs += next.outputs;
        l->input_sizes[i] = next.outputs;
        if (next.out_w == first.out_w && next.out_h == first.out_h) {
            l->out_c += next.out_c;
        } else {
            printf(
                "Error: Different size of input layers: %d x %d, %d x %d\n",
                next.out_w,
                next.out_h,
                first.out_w,
                first.out_h
            );
            l->out_h = l->out_w = l->out_c = 0;
            exit(EXIT_FAILURE);
        }
    }
    l->out_c = l->out_c / l->groups;
    l->outputs = l->outputs / l->groups;
    l->inputs = l->outputs;
    l->delta = (float *)xrealloc(l->delta, l->outputs * l->batch * sizeof(float));
    l->output = (float *)xrealloc(l->output, l->outputs * l->batch * sizeof(float));
}

void
forward_route_layer(const route_layer l, network_state state)
{
    static int route_count = 0;

    route_count += 1;

    double ms_start = get_time_ms();

    printd("[ROUT %2d]                                                        ", route_count);

    if (route_count == 2) {
        int offset = 0;
        for (int i = 0; i < l.n; i++) {
            int index = l.input_layers[i];
            int input_size = l.input_sizes[i];
            int part_input_size = input_size / l.groups;
            short *input = ((short *)state.net.layers[index].output) + part_input_size * l.group_id;
            short *output = ((short *)l.output) + offset;
            memcpy(output, input, sizeof(short) * part_input_size);
            offset += part_input_size;
        }
    } else {
        int offset = 0;
        for (int i = 0; i < l.n; i++) {
            int index = l.input_layers[i];
            int input_size = l.input_sizes[i];
            int part_input_size = input_size / l.groups;
            float *input = state.net.layers[index].output + part_input_size * l.group_id;
            // Instead of memcpy, convert to short right now as well.
            short *output = ((short *)l.output) + offset;
            for (int j = 0; j < part_input_size; j++) {
                output[j] = input[j];
            }
            offset += part_input_size;
        }
    }

    double ms_end = get_time_ms();
    double elapsed = ms_end - ms_start;

    printd("%3.0f ms\n", elapsed);
}
