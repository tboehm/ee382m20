import os
import time
savedir = time.strftime('%m-%d_%H-%M-%S')
os.mkdir(savedir)
os.chdir("..")
images = [f for f in os.listdir("data/") if f.endswith(".jpg")]
for img in images:
    print("Running darknet on {}".format(img))
    os.system("./darknet detector test cfg/coco.data cfg/yolov3-tiny.cfg yolov3-tiny.weights data/{}".format(img))
    os.system("mv predictions.jpg predictions/{}/{}".format(savedir, img))