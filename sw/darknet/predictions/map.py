#!/usr/bin/env python3

import glob
import pathlib
import subprocess as sp

map_cmd = [
    './darknet',
    'detector',
    'map',
    'cfg/coco.data',
    'cfg/yolov3-tiny.cfg',
    'convert/converted.weights',
]

map_needle = 'mean average precision (mAP) = '


def main():
    # cd one directory up from this script's location
    pathlib.Path(__file__).parent.resolve()

    images = glob.glob("data/*.jpg")

    for img in images:
        print(f"{img}:")
        with open('coco_testdev', 'w') as fp:
            fp.write(img)
        rc = sp.run(map_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        output_lines = rc.stdout.decode().splitlines()
        map_lines = filter(lambda line: map_needle in line, output_lines)
        print(list(map_lines)[0])


if __name__ == '__main__':
    main()
