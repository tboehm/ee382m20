#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "snr.h"
#include <stdint.h>

// typedef short fxp;
typedef int fxp;
// typedef int64_t fxp;

fxp roundup(float fp, int scale)
{
	fp = fp * (1 << scale);
	fxp fx = (fxp)fp;
	if(fp - fx >= 0.5) fx++;
	return fx;
}

void fxp_mm(fxp res[M][N], int scale){
	int i, j, k;
    for (i = 0; i < M; ++i) {
        for (k = 0; k < K; ++k) {
            for (j = 0; j < N; ++j) {
				res[i][j] += roundup(A[i][k], scale) * roundup(B[k][j], scale);
            }
        }
    }
}

void calc_snr(float fxp[M][N]){
	float ddiff = 0;
	float cout_flp = 0;
	int i, j;
	for (i = 0; i < M; ++i){
		for (j = 0; j < N; ++j){
			ddiff += (fxp[i][j] - Cout[i][j]) * (fxp[i][j] - Cout[i][j]);
			cout_flp += (Cout[i][j]) * (Cout[i][j]);
		}
	}
	float snr = 10*log10(cout_flp / ddiff);
	printf("%.10f ],\n",snr);
}

int main(int argc, char* argv[])
{
	int i,j;
	fxp fxpRes[M][N];
	float fxp_fltRes[M][N];
	for (int scale = 0; scale < sizeof(fxp) * 4; scale++){
		memset(fxpRes, 0, sizeof fxpRes);
		printf("[ %d, ", scale);
		fxp_mm(fxpRes, scale);
		for (i = 0; i < M; ++i){
			for (j = 0; j < N; ++j){
				fxp_fltRes[i][j] = (float)(fxpRes[i][j])/(1<<(2*scale));
			}
		}
		calc_snr(fxp_fltRes);
	}
}

