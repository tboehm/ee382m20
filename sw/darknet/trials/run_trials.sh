#!/usr/bin/env bash

trial_file=$1

if ! [ "$trial_file" ]; then
    echo "Usage: $0 <results file>"
    exit 1
fi

make -j4 || exit 1

for i in $(seq 1 34); do
    sleep 0.5
    ./darknet detect cfg/yolov3-tiny.cfg convert/converted.weights data/person.jpg 2> /dev/null > temp-out
    grep 'Predicted in' temp-out | grep -Eo '[0-9]+'
    rm temp-out
done | tee "trials/$trial_file.txt"

~/bin/stats "trials/$trial_file.txt"
