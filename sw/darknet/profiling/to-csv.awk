#!/usr/bin/env awk -f

BEGIN {
    print "percent_time,cumulative_seconds,self_seconds,calls,self_seconds_per_call,total_seconds_per_call,function"
    in_table = 0
}

in_table && /^$/ {
    exit 0;
}

in_table {
    print $1 "," $2 "," $3 "," $4 "," $5 "," $6 "," $7;
}

/^ time/ {
    in_table = 1
}
