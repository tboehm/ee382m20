#!/usr/bin/env python3

import argparse
from io import StringIO
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import subprocess as sp


parser = argparse.ArgumentParser()
parser.add_argument('--top-n', '-n', type=int, default=10, help='Select the n longest functions')
parser.add_argument('gprof_out', type=str, help='Output of gprof')

def main():
    args = parser.parse_args()
    rc = sp.run(['./to-csv.awk', args.gprof_out], capture_output=True)
    out = rc.stdout.decode().strip()
    df = pd.read_csv(StringIO(out))
    data = df.sort_values(by='percent_time', ascending=False).iloc[
        :args.top_n,
    ]
    print(data)
    g = sns.barplot(data=data, x='function', y='percent_time')
    plt.xticks(rotation=90)
    plt.title(f'{args.top_n} longest functions in Darknet')
    g.set_xlabel('')
    g.set_ylabel('Percent of execution time')
    g.set_ylim([0, 100])
    g.set_yticks(g.get_yticks().tolist())  # Annoying matplotlib bug
    g.set_yticklabels([f'{int(pct)}%' for pct in g.get_yticks()])
    plt.tight_layout()
    plt.savefig('function-breakdown.png', dpi=500)


if __name__ == '__main__':
    main()
