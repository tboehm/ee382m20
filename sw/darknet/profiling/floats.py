#!/usr/bin/env python3

import argparse
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

parser = argparse.ArgumentParser()
parser.add_argument('csv', help='CSV of float data')


def main():
    args = parser.parse_args()
    data = pd.read_csv(args.csv)

    # Alternative plot: show the whole range, but label just a couple points.
    # g = sns.barplot(data=data, x='Exponent', y='Count')
    # nonzero_labels = [0, 85, 105, 133]
    # xticklabels = [str(n) if n in nonzero_labels else '' for n in range(256)]
    # g.set_xticklabels(xticklabels)

    data.Exponent -= 127
    nonzero = data[(data['Count'] > 0) & (data['Exponent'] != -127)]
    plt.title('Distribution of float exponents in gemm_nn')
    g = sns.barplot(data=nonzero, x='Exponent', y='Count', color='black')
    g.set_xticklabels(g.get_xticklabels(), rotation=80)
    plt.tight_layout()
    plt.savefig('exponents.png', dpi=500)


if __name__ == '__main__':
    main()
