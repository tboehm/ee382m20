#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

Plots = [
    {'name': 'sw-optimizations.png',
     'csv': 'sw-optimizations.csv',
     'baseline': 'No OpenMP, no outer loop, no blocking',
     'key': 'Optimizations',
     'fontsize': 6,
     'figsize': (8, 6),
     },
    {'name': 'fx-arithmetic.png',
     'csv': 'fx-arithmetic.csv',
     'baseline': 'Floating point',
     'key': 'Arithmetic type',
     'fontsize': 8,
     'figsize': None,
     },
]

def plot_csv(params):
    data = pd.read_csv(params['csv'])
    print(data)
    has_hit_rate = 'L1 hit rate' in data.columns
    stats = list()
    baseline_exe_time = 0
    for key, sub in data.groupby(params['key']):
        key = key.replace(';', ', ')
        exe_time = sub['Execution time']
        mean_exe = np.mean(exe_time)
        # 95% symmetric CI
        ci_width = 1.96 * np.std(exe_time) / np.sqrt(len(exe_time))
        if key == params['baseline']:
            baseline_exe_time = mean_exe
        record = [key, mean_exe, f'${mean_exe:.0f} pm {ci_width:.1f}$']
        if has_hit_rate:
            hit_rate = sub['L1 hit rate']
            hit_mean = hit_rate.mean()
            ci_width = 1.96 * np.std(hit_rate) / np.sqrt(len(hit_rate))
            record.append(f'${hit_mean:.3f} pm {ci_width:.4f}$')
        stats.append(record)
    columns = [params['key'], 'exe time', 'Execution time (milliseconds)']
    if has_hit_rate:
        columns.append('L1 hit rate')
    stats = pd.DataFrame.from_records(stats, columns=columns)
    stats['Speedup'] = baseline_exe_time / stats['exe time']
    del stats['exe time']
    print(stats.to_latex(index=False))

    plt.cla()
    if params['figsize']:
        plt.figure(figsize=params['figsize'])
    else:
        plt.figure()
    g = sns.barplot(data=data, x=params['key'], y='Execution time')
    plt.xticks(rotation=90)
    xticklabels = [label._text.replace(';', ',\n') for label in g.get_xticklabels()]
    g.set_xticklabels(xticklabels, rotation=0, fontsize=params['fontsize'])
    g.bar_label(g.containers[0])
    plt.ylabel('Execution time (milliseconds)')
    plt.tight_layout()
    plt.savefig(params['name'], dpi=500)

def main():
    for plot_params in Plots:
        plot_csv(plot_params)


if __name__ == '__main__':
    main()
