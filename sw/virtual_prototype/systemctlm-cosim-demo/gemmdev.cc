#define SC_INCLUDE_DYNAMIC_PROCESSES

#include <inttypes.h>

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

using namespace sc_core;
using namespace std;

#include <sys/types.h>
#include <time.h>
#include "gemmdev.h"

gemmdev::gemmdev(sc_module_name name)
	: sc_module(name), socket("socket")
{
	socket.register_b_transport(this, &gemmdev::b_transport);
}

void gemmdev::compute_1()
{

    int b_start = B1_SP_START;
    int c_start = C1_SP_START;

    for (int i = 0; i < new_M1; i++) {
        int a_idx = i * K1;
        for(int j = 0; j < N1; j++) {
            int b_idx = j * K1;
            int c_idx = j * M1 + i;
            int c_part = 0;
            for (int k = 0; k < K1; k++) {
                int a_part = SP1[(a_idx + k)/2].read();
                short a_data;
                if((a_idx+k) & 1) {
                    a_data = (a_part & UPPER_HALF) >> 16;
                } else {
                    a_data = (a_part & LOWER_HALF);
                }

                
                int b_part = SP1[(b_idx + k)/2 + b_start].read();
                short b_data;
                if((b_idx + k) & 1) {
                    b_data = (b_part & UPPER_HALF) >> 16;
                } else {
                    b_data = (b_part & LOWER_HALF);
                }

                c_part = c_part + a_data * b_data;

                wait(COMP_DELAY, SC_NS);
            }

            // TODO Downshift C here
            int val = SP1[(c_idx)/2 + c_start].read();
            if ((c_idx) & 1) {
                val &= LOWER_HALF;
                val |= (c_part << 16) & UPPER_HALF;
            } else {
                val &= UPPER_HALF;
                val |= (c_part & LOWER_HALF);
            }
            SP1[(c_idx)/2 + c_start].write(val);

        }
    }
    irq.write(true);
}

void gemmdev::compute_2()
{ 
    int a_start = 0;
    int b_start = B2_SP_START;
    int c_start = C2_SP_START;

    std::cout << "Compute Matmul\n";
    for (int i = 0; i < M2; i++) {
        for (int k = 0; k < K2; k++) {
            int a_part = SP2[i * K1 + k + a_start].read();
            int b_idx = k * N2 + b_start;
            int c_idx = i * N2 + c_start;
            for (int j = 0; j < N2; j++, b_idx++, c_idx++) {
                int b_part = SP2[b_idx].read();
                int c_part = SP2[c_idx].read();
                int res = c_part + a_part * b_part;
                SP2[c_idx].write(res);
                wait(COMP_DELAY, SC_NS);
            }
        }
    }

    irq.write(true);
    std::cout << "Finished compute 2" << std::endl;
}

void gemmdev::b_transport(tlm::tlm_generic_payload& trans, sc_time& delay)
{
	tlm::tlm_command cmd = trans.get_command();
	sc_dt::uint64 addr = trans.get_address();
	unsigned char *data = trans.get_data_ptr();
	unsigned int len = trans.get_data_length();
	unsigned char *byt = trans.get_byte_enable_ptr();
	unsigned int wid = trans.get_streaming_width();
    if (byt != 0) {
		trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
		return;
	}

	if (len > 4 || wid < len) {
		trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
		return;
	}
	trans.set_response_status(tlm::TLM_OK_RESPONSE);

    // Pretend this is slow!
    delay += sc_time(TRAN_DELAY, SC_US);
    sc_time now;
    if (trans.get_command() == tlm::TLM_READ_COMMAND) {
        now = sc_time_stamp() + delay;
        int v = 0;
        bool read_reg = true;
        switch (addr) {
        case REG_START_1:
            v = START1.read();
            std::cout << "Reading start1 " << v << std::endl;
            break;
        case REG_M_1:
            v = M1.read();
            std::cout << "Reading m1 " << v << std::endl;
            break;
        case REG_N_1:
            v = N1.read();
            std::cout << "Reading n1 " << v << std::endl;
            break;
        case REG_K_1:
            v = K1.read();
            std::cout << "Reading k1 " << v << std::endl;
            break;
        case REG_newM_1:
            v = new_M1.read();
            std::cout << "Reading newM1 " << v << std::endl;
            break;
        case REG_B1_SP_START:
            v = B1_SP_START.read();
            std::cout << "Reading b1_sp_start " << v << std::endl;
            break;
        case REG_C1_SP_START:
            v = C1_SP_START.read();
            std::cout << "Reading c1_sp_start " << v << std::endl;
            break;
        case REG_START_2:
            v = START2.read();
            std::cout << "Reading start2 " << v << std::endl;
            break;
        case REG_M_2:
            v = M2.read();
            std::cout << "Reading m2 " << v << std::endl;
            break;
        case REG_N_2:
            v = N2.read();
            std::cout << "Reading n2 " << v << std::endl;
            break;
        case REG_K_2:
            v = K2.read();
            std::cout << "Reading k2 " << v << std::endl;
            break;
        case REG_newM_2:
            v = new_M2.read();
            std::cout << "Reading newM2 " << v << std::endl;
            break;
        case REG_B2_SP_START:
            v = B2_SP_START.read();
            std::cout << "Reading b2_sp_start " << v << std::endl;
            break;
        case REG_C2_SP_START:
            v = C2_SP_START.read();
            std::cout << "Reading c2_sp_start " << v << std::endl;
            break;
        case REG_IRQ:
            v = irq.read();
            std::cout << "Reading irq " << v << std::endl;
            break;
        default:
            read_reg = false;
            break;
        }

        // Check to see if we are reading SP
        if (!read_reg) {
            if( addr >= SP1_START && addr < SP1_END) {
                addr = (addr - SP1_START) / 4;
                v = SP1[addr].read();
            } else if( addr >= SP2_START && addr < SP2_END ) {
                addr = (addr - SP2_START) / 4;
                v = SP2[addr].read();
            } else {
                // bad addr
                std::cerr << "Bad Read: addr = " << addr << std::endl;
            }
        }

            memcpy(data, &v, len);
    } else if (cmd == tlm::TLM_WRITE_COMMAND) {
        now = sc_time_stamp() + delay;
        bool write_reg = true;
        bool b_data = *(bool *)data;
        short s_data = *(short *)data;
        int i_data = *(int *)data;

        switch (addr) {
        case REG_START_1:
            START1.write(b_data);
            if (b_data) {
                compute_1();
            }
            break;
        case REG_M_1:
            std::cout << "Writing m1 " << i_data << std::endl;
            M1.write(i_data);
            break;
        case REG_N_1:
            std::cout << "Writing n1 " << i_data << std::endl;
            N1.write(i_data);
            break;
        case REG_K_1:
            std::cout << "Writing k1 " << i_data << std::endl;
            K1.write(i_data);
            break;
        case REG_newM_1:
            std::cout << "Writing newM1 " << i_data << std::endl;
            new_M1.write(i_data);
            break;
        case REG_B1_SP_START:
            B1_SP_START.write(i_data);
            std::cout << "Writing B1 " << i_data << std::endl;
            break;
        case REG_C1_SP_START:
            C1_SP_START.write(i_data);
            std::cout << "Writing C1 " << i_data << std::endl;
            break;
        case REG_START_2:
            START2.write(b_data);
            if (b_data) {
                compute_2();
            }
            break;  
        case REG_M_2:
            std::cout << "Writing m2 " << i_data << std::endl;
            M2.write(i_data);
            break;
        case REG_N_2:
            std::cout << "Writing n2 " << i_data << std::endl;
            N2.write(i_data);
            break;
        case REG_K_2:
            std::cout << "Writing k2 " << i_data << std::endl;
            K2.write(i_data);
            break;
        case REG_newM_2:
            std::cout << "Writing newM2 " << i_data << std::endl;
            new_M2.write(i_data);
            break;
        case REG_B2_SP_START:
            B2_SP_START.write(b_data);
            break;
        case REG_C2_SP_START:
            C2_SP_START.write(b_data);
            break;
        case REG_IRQ:
            irq.write(b_data);
            break;
        default:
            write_reg = false;
            break;
        }

        if (!write_reg) {
            int write_value = *(int *)data;
            if (addr >= SP1_START && addr < SP1_END) {
                addr = (addr - SP1_START)/4;
                SP1[addr].write(write_value);
            } else if (addr >= SP2_START && addr < SP2_END) {
                addr = (addr - SP2_START)/4;
                SP2[addr].write(write_value);
            } else {
                std::cerr << "Bad Write: addr = " << addr << std::endl;
            } 
        }
    } else {
        std::cerr << "Bad Address: addr = " << addr << std::endl;
    }
}
