// Distance between scratchpad addresses.
#define SP_MEM_SIZE  0x1800000ul
#define FPGA_BASE   0xa1000000ul
#define FPGA_SIZE (SP_MEM_SIZE * 4)

#define SP_SIZE 106496
#define SP1_START SP_MEM_SIZE //0xA2800000
#define SP2_START SP1_START + SP_MEM_SIZE //0xA285F1E0

#define SP1_END   SP1_START + (SP_SIZE * 4)
#define SP2_END   SP2_START + (SP_SIZE * 4)

#define UPPER_HALF 0xFFFF0000
#define LOWER_HALF 0x0000FFFF

#define COMP_DELAY 10
#define TRAN_DELAY 1

enum RegOffsets {
    REG_START_1     = 0,
    REG_M_1         = 4,
    REG_N_1         = 8,
    REG_K_1         = 12,
    REG_newM_1      = 16,
    REG_B1_SP_START = 20,
    REG_C1_SP_START = 24,
    REG_START_2     = 28,
    REG_M_2         = 32,
    REG_N_2         = 36,
    REG_K_2         = 40,
    REG_newM_2      = 44,
    REG_B2_SP_START = 48,
    REG_C2_SP_START = 52,
    REG_IRQ         = 56,
};

class gemmdev
: public sc_core::sc_module
{
public:
	gemmdev(sc_core::sc_module_name name);
	tlm_utils::simple_target_socket<gemmdev> socket;
    sc_signal<bool> START1;
    sc_signal<int> M1;
    sc_signal<int> N1;
    sc_signal<int> K1;
    sc_signal<int> new_M1;
    sc_signal<int> B1_SP_START;
    sc_signal<int> C1_SP_START;    

    sc_signal<bool> START2;
    sc_signal<int> M2;
    sc_signal<int> N2;
    sc_signal<int> K2;
    sc_signal<int> new_M2;
    sc_signal<int> B2_SP_START;
    sc_signal<int> C2_SP_START;    

	sc_out<bool> irq;

    sc_signal<int> SP1[SP_SIZE];
    sc_signal<int> SP2[SP_SIZE];

	virtual void b_transport(tlm::tlm_generic_payload& trans,
					sc_time& delay);
	void compute_1(void);
    void compute_2(void);
	void clear_c1(void);
    void clear_c2(void);
};
