#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "accelerator_interface.h"

#define LOWER_HALF 0x0000FFFF
#define UPPER_HALF 0xFFFF0000
#define PACKING_FACTOR 2
#define ENTRY_SIZE 4

extern int det_int;
unsigned long val, result;
void matmul_test();


int main(int argc, char * argv[])
{
    accelerator_init();
    matmul_test();
    accelerator_finish();
    return 0;
}

int rows_N[] = {4096,1024,400,160,80,20,20,50,20,50,100,25,200};
int rows_M[] = {16,32,64,128,64,64,16,128,64,255,128,32,255};
int b_iterations[] = {25,25,16,10,5,5,5,2,5,2,1,16,2};
int a_iterations[] = {1,1,1,1,4,8,64,2,8,1,1,8,1};
int layer_case[] = {1,1,1,1,2,2,2,2,2,1,1,2,1};

void gemm_acc_pack(int M, int K, int N, int *A, int *B, int *C)
{
    static int darknet_layer = 4;
    int new_N = rows_N[darknet_layer];
    int new_M = rows_M[darknet_layer];
    int scenario = layer_case[darknet_layer];
    int b_iter = b_iterations[darknet_layer];
    int a_iter = a_iterations[darknet_layer];
    int b_off = 0;
    int c_off = 0;

    if(scenario == 1) {
        // Setup M, N, K, and SP
        unsigned long B1_SP = (unsigned long) M*K / PACKING_FACTOR;
        unsigned long C1_SP = B1_SP + ((new_N * K)/PACKING_FACTOR);
        accelerator_setup_buffer1(M, new_N, K, M, B1_SP, C1_SP);
        accelerator_send_int_array(A, (M*K)/2, SP1_START);
        for(int b = 0; b < b_iter; b++) {
            // Send B
            accelerator_send_int_array(B + b_off, (new_N * K)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * B1_SP));
            b_off += K * new_N/PACKING_FACTOR;

            // Start
            accelerator_start_buffer1();

            // Wait for interrupt
            while(!det_int) {
                continue;
            }
            det_int -=1;

            // Copy C back
            accelerator_get_int_result(C + c_off, M*new_N/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * C1_SP));
            c_off += new_N * M/PACKING_FACTOR;
        }
    } else if (scenario == 2) {
         unsigned long B1_SP = (unsigned long) (new_M * K)/PACKING_FACTOR;
         unsigned long C1_SP = (unsigned long) B1_SP + (new_N *K)/PACKING_FACTOR;
         accelerator_setup_buffer1(M, new_N, K, new_M, B1_SP, C1_SP);

         // A stationary, we index into different elements of B
         for(int b = 0; b < b_iter; b++){
            accelerator_send_int_array(B + b_off, (new_N * K)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * B1_SP));
            b_off += (new_N * K)/PACKING_FACTOR;

            int a_off = 0;
            for(int a = 0; a < a_iter; a++){
                accelerator_c1sp_update(C1_SP + (new_M*a)/PACKING_FACTOR);
                accelerator_send_int_array(A + a_off, (new_M * K)/PACKING_FACTOR, SP1_START);
                a_off += (new_M * K)/PACKING_FACTOR;

                accelerator_start_buffer1();

                while(!det_int){
                    continue;
                }
                det_int -=1; 
            }

            // Copy C back
            accelerator_get_int_result(C + c_off,(M * new_N)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * C1_SP));
            c_off += (new_N * M)/PACKING_FACTOR; 
        }
   
    } 
    darknet_layer++;
}


void gemm_check_packed(int M, int N, int K, short *A, short *B, short *C_prime, int *A_int, int *B_int, int *C_int)
{

    int *C_prime_int = (int*)malloc(M * N * 2);

    // Generate Packed Reference
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            int c_part = 0;
            for (int k = 0; k < K; k++) {
                // A value
                short a_data;
                int a_part = A_int[(i*K + k)/2];
                if((i*K + k) & 1) {
                    a_data = (a_part & UPPER_HALF) >> 16;
                } else {
                    a_data = (a_part & LOWER_HALF);
                }

                // B Value
                short b_data;
                int b_part = B_int[(j * K +k)/2];
                if ((j * K +k) & 1) {
                    b_data = (b_part & UPPER_HALF) >> 16;
                } else {
                    b_data = (b_part & LOWER_HALF);
                }

                c_part = c_part + a_data * b_data;
            }
            int val = C_prime_int[(j*M+i)/2];
            if((j*M+i) & 1) {
                val &= LOWER_HALF;
                val |= (c_part << 16) & UPPER_HALF;
            } else {
                val &= UPPER_HALF;
                val |= (c_part & LOWER_HALF);
            }
            C_prime_int[(j*M+i)/2] = val;
        }
    }   

    // Compare C_int with reference packed
    int mismatch_count = 0;
    for (int i = 0; i < (M * N)/2; i++) {
        if (C_int[i] != C_prime_int[i]) {
            mismatch_count += 1;
            if (mismatch_count < 20) {
                fprintf(stderr, "First mismatch at index %d: C = %d, C_prime = %d\n",
                        i, C_int[i], C_prime_int[i]);
            }
        }
    }

    if (mismatch_count != 0) {
        fprintf(stderr, "\033[31;1mFound %d mismatches total\033[0m\n", mismatch_count);
        exit(EXIT_FAILURE);
    } else {
        printf("\033[32;1mPerfect Match!\033[0m\n");
    }
}



void accelerator_pack(short *A, short *B, int *A_int, int *B_int, int M, int N, int K)
{
    for(int i = 0; i < M*K; i++) {
        int val = A_int[i/2];
        if (i & 1) {
            val &= LOWER_HALF;
            val |= (A[i] << 16) & UPPER_HALF;
        } else {
            val &= UPPER_HALF;
            val |= (A[i] & LOWER_HALF) & 0x00FF;
        }
        A_int[i/2] = val;
    }

    for(int i = 0; i < N*K; i++) {
        int val = B_int[i/2];
        if (i & 1) {
            val &= LOWER_HALF;
            val |= (B[i] << 16) & UPPER_HALF;
        } else {
            val &= UPPER_HALF;
            val |= (B[i] & LOWER_HALF) & 0x00FF;
        }
        B_int[i/2] = val;
    }

}


void matmul_test()
{
    // These are passed to gemm
    int layer_M[] = {16, 32, 64, 128, 256, 512, 1024, 256, 512, 255, 128, 256, 255};
    int layer_K[] = {27, 144, 288, 576, 1152, 2304, 4608, 1024, 2304, 512, 256, 3456, 256};
    int layer_N[] = {102400,25600,6400,1600,400,100,100,100,100,100,100,400,400};
    
    // Approximation
    for(int darknet_layer = 0; darknet_layer < 13; darknet_layer++){
        printf("running layer %d\n", darknet_layer + 1);

        // Normally Given as input simulated for now
        unsigned int M = layer_M[darknet_layer];
        unsigned int K = layer_K[darknet_layer];
        unsigned int N = layer_N[darknet_layer];

        // Generate Matrices
        short *A = (short *)malloc(M * K * 2);
        short *B = (short *)malloc(N * K * 2);
        int *A_int = (int *)malloc(M * K * 2);        
        int *B_int = (int *)malloc(N * K * 2);        
        int *C_int = (int *)malloc(M * N * 2);        
        short *C_prime =   (short *)malloc(M * N * 2);

        for (int i = 0; i < M * K; i++) {
            A[i] = rand() % 10;
        }

        for (int i = 0; i < N * K; i++) {
            B[i] = rand() % 10;
        }

        accelerator_pack(A, B, A_int, B_int, M, N, K);
        gemm_acc_pack(M, K, N, A_int, B_int, C_int);
        gemm_check_packed(M, N, K, A, B, C_prime, A_int, B_int, C_int);
    }
    printf("run complete\n");
}


