#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "example_helper.h"

//#define ACC
#define ELEM_SIZE 4

extern int det_int;

unsigned long val, result;

void matmul_test();

int main(int argc, char * argv[])
{
#ifdef ACC
    accelerator_init();
#endif
    matmul_test();
#ifdef ACC
    accelerator_finish();
#endif
    return 0;
}

//TODO Uneven case setup

// Original Optimized values
//    int rows_N[] = {2704, 1014, 520, 192, 97, 26, 26, 65, 26, 85, 169, 14, 169};
//    int iterations[] = {64, 43, 21, 15, 7, 7, 7, 3, 7, 2, 1, 49, 4};

// Short values
/*
int rows_N[] = {2704, 676, 338, 169, 13, 13, 13, 13, 13, 13, 169, 13, 169};
int rows_M[] = {16, 32, 64, 128, 128, 64, 32, 128, 64, 255, 128, 32, 255};
int b_iterations[] = {64, 64, 32, 16, 52, 13, 13, 13, 13, 13, 1, 52, 4};
int a_iterations[] = {1, 1, 1, 1, 2, 8, 32, 2, 8, 1, 1, 8, 1};
int layer_case[] = {1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 2, 1};
*/

// Int values
int rows_N[] = {2704, 338, 169, 52, 13, 13, 13, 13, 13, 13, 169, 13, 13};
int rows_M[] = {16, 32, 64, 128, 64, 32, 8, 64, 32, 128, 128, 16, 255};
int b_iterations[] = {64, 128, 64, 52, 52, 13, 13, 13, 13, 13, 1, 52, 52};
int a_iterations[] = {1, 1, 1, 1, 4, 16, 128, 4, 16, 1, 1, 16, 1};
int layer_case[] = {1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 2, 1};

#ifdef ACC
// Virtual Prototype calls
// Need to change routines to regular accelerator interface
void gemm_acc(int M, int K, int N, int *A, int *B, int *C)
{
    static int darknet_layer = 0;
    // SP Memory setup
    int new_N = rows_N[darknet_layer];
    int new_M = rows_M[darknet_layer];
    int scenario = layer_case[darknet_layer];
    int *temp_C = (int *)malloc(new_N * M * sizeof(int));

    int b_iter = b_iterations[darknet_layer];
    int a_iter = a_iterations[darknet_layer];

    if(scenario == 1) {
        // A stationary, we index into different elements of B
        // Setup SP and M, N, K and send A
        unsigned long B1_SP_START = (unsigned long) M*K;
        unsigned long C1_SP_START = B1_SP_START + new_N * K;
        accelerator_setup_buffer1_case1(M, new_N, K, B1_SP_START, C1_SP_START);
        accelerator_send_int_array(A, M * K, SP1_START);

        int b_off = 0;
        int c_off = 0;

        for(int l = 0; l < b_iter; l++){
            // Clear C
            accelerator_clear_buffer1();

            // Send B
            accelerator_send_int_array(B + b_off, new_N * K, SP1_START + (ELEM_SIZE *B1_SP_START));
            b_off += K * new_N;

            // Start
            accelerator_start_buffer1();

            // Wait for interrupt
            while(!det_int){
                continue;
            }
            det_int = 0;

            // Copy C back
            accelerator_get_int_result(C + c_off, M*new_N, SP1_START + (ELEM_SIZE * C1_SP_START));
            c_off += new_N * M;
        }
    }
    else if(scenario == 2){
        // Parts of B stationary while we index into different elements of A
        // Setup SP, M, N, K
        unsigned long B1_SP_START = (unsigned long) new_M * K;
        unsigned long C1_SP_START = (unsigned long) B1_SP_START + new_N *K;
        accelerator_setup_buffer1_case1(new_M, new_N, K, B1_SP_START, C1_SP_START);

        int a_off = 0;
        int b_off = 0;
        int c_off = 0;

        // A stationary, we index into different elements of B
        for(int l = 0; l < b_iter; l++){
            accelerator_send_int_array(B + b_off, new_N * K, SP1_START + (ELEM_SIZE * B1_SP_START));
            b_off += new_N * K;

            accelerator_clear_buffer1();

            a_off = 0;
            for(int a = 0; a < a_iter; a++){

                accelerator_send_int_array(A + a_off, new_M * K, SP1_START);
                a_off += new_M * K;

                accelerator_start_buffer1();

                while(!det_int){
                    continue;
                }
                det_int = 0;

            }

            // Copy C back
            accelerator_get_int_result(C + c_off, M * new_N, SP1_START + (ELEM_SIZE*C1_SP_START));
            c_off += new_N * M; 
        } 
    }

    darknet_layer++;

}
#endif

void gemm_app_int(int M, int K, int N, int *A, int *B, int *C)
{
    static int darknet_layer = 0;
    // SP Memory setup
    int new_N = rows_N[darknet_layer];
    int new_M = rows_M[darknet_layer];
    int scenario = layer_case[darknet_layer];
    int *temp_C = (int *)malloc(new_N * M * sizeof(int));

    int b_iter = b_iterations[darknet_layer];
    int a_iter = a_iterations[darknet_layer];

    printf("New N: %d\n", new_N);
    printf("New M: %d\n", new_M);
    printf("A_Iterations: %d\n", a_iter);
    printf("B_Iterations: %d\n", b_iter);
    printf("Size of temp_C: %d\n", new_N * M * sizeof(int));

    if(scenario == 1) {
        // A stationary, we index into different elements of B
        for(int l = 0; l < b_iter; l++){
            memset(temp_C, 0, new_N * M * sizeof(int));  
            for (int i = 0; i < M; i++){
                for(int j = 0; j < new_N; j++){
                    for(int k = 0; k < K; k++){
                        // Write to temporary C
                        temp_C[j*M+i] += A[i*K + k] * B[(K * new_N * l) + j*K + k];
                    }
                }
            }

            // Approximate the SP setup by smaller tempC and larger permanent array
            for(int m = 0; m < new_N * M; m++){
                C[l*new_N*M + m] = temp_C[m];
            }
        }
    }
    else if(scenario == 2){
        // A stationary, we index into different elements of B
        for(int l = 0; l < b_iter; l++){
            memset(temp_C, 0, new_N * M * sizeof(int));
            for(int a = 0; a < a_iter; a++){
                for (int i = 0; i < new_M; i++){
                    for(int j = 0; j < new_N; j++){
                        for(int k = 0; k < K; k++){
                            temp_C[j*M+i + a * new_M] += A[(K * new_M * a) + i*K + k] * B[(K * new_N * l) + j*K + k];
                        }
                    }
                }
            }
 
            // Approximate the SP setup by smaller tempC and larger permanent array
            for(int m = 0; m < new_N * M; m++){
                C[l*new_N*M + m] = temp_C[m];
            }

        } 
    }

    darknet_layer++;
} 

void gemm_check_int(int M, int K, int N, int *A, int *B, int *C, int *C_prime)
{
    // Check Layer
    memset(C_prime, 0, N * M * 4);
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            for (int k = 0; k < K; k++) {
                C_prime[j*M+i] += A[i*K + k] * B[j*K + k];
            }
        }
    }
   
    // For now one matmul
    int mismatch_count = 0;
    for (int i = 0; i < M * N; i++) {
        if (C[i] != C_prime[i]) {
            mismatch_count += 1;
            if (mismatch_count == 1) {
                fprintf(stderr, "First mismatch at index %d: C = %d, C_prime = %d",
                        i, C[i], C_prime[i]);
            }
        }
    }

    if (mismatch_count != 0) {
        fprintf(stderr, "\033[31;1mFound %d mismatches total\033[0m\n", mismatch_count);
        exit(EXIT_FAILURE);
    } else {
        printf("\033[32;1mPerfect Match!\033[0m\n");
    }
}

void matmul_test()
{
    // These are passed to gemm
    int layer_M[] = {16, 32, 64, 128, 256, 512, 1024, 256, 512, 255, 128, 256, 255};
    int layer_N[] = {173056, 43264, 10816, 2704, 676, 169, 169, 169, 169, 169, 169, 676, 676};
    int layer_K[] = {27, 144, 288, 576, 1152, 2304, 4608, 1024, 2304, 512, 256, 3456, 256};
    
    // Approximation
    for(int darknet_layer = 0; darknet_layer < 13; darknet_layer++){
        printf("running layer %d\n", darknet_layer + 1);

        // Normally Given as input simulated for now
        unsigned int M = layer_M[darknet_layer];
        unsigned int K = layer_K[darknet_layer];
        unsigned int N = layer_N[darknet_layer];

        // Generate Matrices
        int *A = (int *)malloc(M * K * sizeof(int));
        int *B = (int *)malloc(N * K * sizeof(int));
        int *C = (int *)malloc(M * N * sizeof(int));
        int *C_prime =   (int *)malloc(M * N * sizeof(int));

        for (int i = 0; i < M * K; i++) {
            A[i] = rand() % 10;
        }

        for (int i = 0; i < N * K; i++) {
            B[i] = rand() % 10;
        }
#ifndef ACC
        gemm_app_int(M, K, N, A, B, C);
#else
        gemm_acc(M, K, N, A, B, C);
#endif
        gemm_check_int(M, K, N, A, B, C, C_prime);

    }
    printf("run complete\n");

}


