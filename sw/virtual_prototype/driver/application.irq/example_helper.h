#define SP_MEM_SIZE  0x1800000ul
#define FPGA_BASE   0xa1000000ul
#define FPGA_SIZE  (SP_MEM_SIZE * 4)

#define SP1_START SP_MEM_SIZE
#define SP1_SIZE 425984
#define SP1_END SP1_START + SP1_SIZE
 
enum NewRegOffsets {
    REG_START_1     = 0,
    REG_M_1         = 4,
    REG_N_1         = 8,
    REG_K_1         = 12,
    REG_newM_1      = 16,
    REG_B1_SP_START = 20,
    REG_C1_SP_START = 24,
    REG_START_2     = 28,
    REG_M_2         = 32,
    REG_N_2         = 36,
    REG_K_2         = 40,
    REG_newM_2      = 44,
    REG_B2_SP_START = 52,
    REG_C2_SP_START = 56,
    REG_IRQ         = 60,
};


#define MAKE_ADDR(base, offs) ((unsigned int *)(((void *)(base)) + (offs)))

// Gemm interface functions
void accelerator_setup_buffer1(unsigned int M, unsigned int N, unsigned int K, unsigned int newM, unsigned int B1, unsigned int C1);
void accelerator_start_buffer1(void);
void accelerator_c1sp_update(unsigned int C1);

// Helper functions
void accelerator_send_array(short *data, int size, int dest);
void accelerator_get_result(short *data, int size, int source);
void accelerator_send_int_array(int *data, int size, int dest);
void accelerator_get_int_result(int *data, int size, int source);

void accelerator_init();
void accelerator_finish();
