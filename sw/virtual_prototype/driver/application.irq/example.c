#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "example_helper.h"

#define ACC
#define SIZE_OF_A 4
#define SIZE_OF_RESULT 4

#define LOWER_HALF 0x0000FFFF
#define UPPER_HALF 0xFFFF0000
#define PACKING_FACTOR 2
#define ENTRY_SIZE 4

extern int det_int;

unsigned long val, result;

void matmul_test();

int main(int argc, char * argv[])
{
#ifdef ACC
    accelerator_init();
#endif
    matmul_test();
#ifdef ACC
    accelerator_finish();
#endif
    return 0;
}

int rows_N[] = {4096,1024,400,160,80,20,20,50,20,50,100,25,200};
int rows_M[] = {16,32,64,128,64,64,16,128,64,255,128,32,255};
int b_iterations[] = {25,25,16,10,5,5,5,2,5,2,1,16,2};
int a_iterations[] = {1,1,1,1,4,8,64,2,8,1,1,8,1};
int layer_case[] = {1,1,1,1,2,2,2,2,2,1,1,2,1};


#ifdef ACC
// Virtual Prototype calls
void gemm_acc_pack(int M, int K, int N, int *A, int *B, int *C)
{
    static int darknet_layer = 4;
    int new_N = rows_N[darknet_layer];
    int new_M = rows_M[darknet_layer];
    int scenario = layer_case[darknet_layer];
    int b_iter = b_iterations[darknet_layer];
    int a_iter = a_iterations[darknet_layer];
    int b_off = 0;
    int c_off = 0;

    if(scenario == 1) {
        // Setup M, N, K, and SP
        unsigned long B1_SP = (unsigned long) M*K / PACKING_FACTOR;
        unsigned long C1_SP = B1_SP + ((new_N * K)/PACKING_FACTOR);
        accelerator_setup_buffer1(M, new_N, K, M, B1_SP, C1_SP);
        accelerator_send_int_array(A, (M*K)/2, SP1_START);
        for(int b = 0; b < b_iter; b++) {
            // Send B
            accelerator_send_int_array(B + b_off, (new_N * K)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * B1_SP));
            b_off += K * new_N/PACKING_FACTOR;

            // Start
            accelerator_start_buffer1();

            // Wait for interrupt
            while(!det_int) {
                continue;
            }
            det_int -=1;

            // Copy C back
            accelerator_get_int_result(C + c_off, M*new_N/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * C1_SP));
            c_off += new_N * M/PACKING_FACTOR;
        }

    } else if (scenario == 2) {
         unsigned long B1_SP = (unsigned long) (new_M * K)/PACKING_FACTOR;
         unsigned long C1_SP = (unsigned long) B1_SP + (new_N *K)/PACKING_FACTOR;
         accelerator_setup_buffer1(M, new_N, K, new_M, B1_SP, C1_SP);

         // A stationary, we index into different elements of B
         for(int b = 0; b < b_iter; b++){
            accelerator_send_int_array(B + b_off, (new_N * K)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * B1_SP));
            b_off += (new_N * K)/PACKING_FACTOR;

            int a_off = 0;
            for(int a = 0; a < a_iter; a++){
                accelerator_c1sp_update(C1_SP + (new_M*a)/PACKING_FACTOR);
                accelerator_send_int_array(A + a_off, (new_M * K)/PACKING_FACTOR, SP1_START);
                a_off += (new_M * K)/PACKING_FACTOR;

                accelerator_start_buffer1();

                while(!det_int){
                    continue;
                }
                det_int -=1; 
            }

            // Copy C back
            accelerator_get_int_result(C + c_off,(M * new_N)/PACKING_FACTOR, SP1_START + (ENTRY_SIZE * C1_SP));
            c_off += (new_N * M)/PACKING_FACTOR; 
        }
   
    } 
    darknet_layer++;
}
#endif

void gemm_app_pack(int M, int K, int N, int* A, int *B, int *C)
{
    static int darknet_layer = 0;

    // SP Memory setup
    int new_N = rows_N[darknet_layer];
    int new_M = rows_M[darknet_layer];
    int scenario = layer_case[darknet_layer];
    int b_iter = b_iterations[darknet_layer];
    int a_iter = a_iterations[darknet_layer];

    short *temp_C = (short *)malloc(new_N * M * SIZE_OF_RESULT);

    if(scenario == 1) {
        // A stationary, we index into different elements of B
        for(int b = 0; b < b_iter; b++){
            for (int i = 0; i < M; i++){
                for(int j = 0; j < new_N; j++){
                    int c_part = 0;
                    for(int k = 0; k < K; k++){
                        // A value
                        short a_data;
                        int a_part = A[(i*K + k)/2];
                        if((i*K + k) & 1) {
                            a_data = (a_part & UPPER_HALF) >> 16;
                        } else {
                            a_data = (a_part & LOWER_HALF);
                        }

                        // B Value
                        short b_data;
                        int b_part = B[((K * new_N *b) + j * K +k)/2];
                        if (((K * new_N *b) + j * K +k) & 1) {
                            b_data = (b_part & UPPER_HALF) >> 16;
                        } else {
                            b_data = (b_part & LOWER_HALF);
                        }

                        c_part = c_part + a_data * b_data;

                    } 
                    // Write to temporary C
                    temp_C[j*M+i] = c_part;                
                }
            }
            // Approximate the SP setup by smaller tempC and larger permanent array
            for(int m = 0; m < new_N * M; m++){
                int val = C[(b*new_N*M +m)/2];
                if (m & 1) {
                    val &= LOWER_HALF;
                    val |= (temp_C[m] << 16) & UPPER_HALF;
                } else {
                    val &= UPPER_HALF;
                    val |= (temp_C[m] & LOWER_HALF);
                }
                C[(b*new_N*M +m)/2] = val;
            }
        }
    }
    else if(scenario == 2){
        // A stationary, we index into different elements of B
        for(int b = 0; b < b_iter; b++){
            for(int a = 0; a < a_iter; a++){
                for (int i = 0; i < new_M; i++){
                    for(int j = 0; j < new_N; j++){
                        int c_part = 0;
                        for(int k = 0; k < K; k++){
                            short a_data;
                            int a_idx = ((K*new_M*a)+i*K+k); 
                            int a_part = A[a_idx/2];
                            if(a_idx & 1) {
                                a_data = (a_part & UPPER_HALF) >> 16;
                            } else {
                                a_data = (a_part & LOWER_HALF);
                            }

                            short b_data;
                            int b_idx = K*new_N*b + j*K +k;
                            int b_part = B[b_idx/2];
                            if(b_idx & 1) {
                                b_data = (b_part & UPPER_HALF) >> 16;
                            } else {
                                b_data = (b_part & LOWER_HALF);
                            }

                            c_part = c_part + a_data * b_data;

                        }
                        temp_C[j*M+i + a*new_M] = c_part;
                    }
                }
            }
            // Approximate the SP setup by smaller tempC and larger permanent array
            for(int m = 0; m < new_N * M; m++){
                int val = C[(b*new_N*M +m)/2];
                if (m & 1) {
                    val &= LOWER_HALF;
                    val |= (temp_C[m] << 16) & UPPER_HALF;
                } else {
                    val &= UPPER_HALF;
                    val |= (temp_C[m] & LOWER_HALF);
                }
                C[(b*new_N*M +m)/2] = val;
            }
        } 
    }

    darknet_layer++;

}

void gemm_check(int M, int K, int N, short *A, short *B, short *C, short *C_prime)
{
    // Check Layer
    memset(C_prime, 0, N * M * SIZE_OF_RESULT);
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            for (int k = 0; k < K; k++) {
               C_prime[j*M+i] += A[i*K + k] * B[j*K + k];
            }
        }
    }
 
    // For now one matmul
    int mismatch_count = 0;
    for (int i = 0; i < M * N; i++) {
        if (C[i] != C_prime[i]) {
            mismatch_count += 1;
            if (mismatch_count == 1) {
                fprintf(stderr, "First mismatch at index %d: C = %d, C_prime = %d",
                        i, C[i], C_prime[i]);
            }
        }
    }

    if (mismatch_count != 0) {
        fprintf(stderr, "\033[31;1mFound %d mismatches total\033[0m\n", mismatch_count);
        exit(EXIT_FAILURE);
    } else {
        printf("\033[32;1mPerfect Match!\033[0m\n");
    }
}

void gemm_check_packed(int M, int N, int K, short *A, short *B, short *C_prime, int *A_int, int *B_int, int *C_int)
{

    int *C_prime_int = (int*)malloc(M * N * 2);

    // Generate Packed Reference
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            int c_part = 0;
            for (int k = 0; k < K; k++) {
                // A value
                short a_data;
                int a_part = A_int[(i*K + k)/2];
                if((i*K + k) & 1) {
                    a_data = (a_part & UPPER_HALF) >> 16;
                } else {
                    a_data = (a_part & LOWER_HALF);
                }

                // B Value
                short b_data;
                int b_part = B_int[(j * K +k)/2];
                if ((j * K +k) & 1) {
                    b_data = (b_part & UPPER_HALF) >> 16;
                } else {
                    b_data = (b_part & LOWER_HALF);
                }

                c_part = c_part + a_data * b_data;
            }
            int val = C_prime_int[(j*M+i)/2];
            if((j*M+i) & 1) {
                val &= LOWER_HALF;
                val |= (c_part << 16) & UPPER_HALF;
            } else {
                val &= UPPER_HALF;
                val |= (c_part & LOWER_HALF);
            }
            C_prime_int[(j*M+i)/2] = val;
        }
    }   

    // Compare C_int with reference packed
    int mismatch_count = 0;
    for (int i = 0; i < (M * N)/2; i++) {
        if (C_int[i] != C_prime_int[i]) {
            mismatch_count += 1;
            if (mismatch_count < 20) {
                fprintf(stderr, "First mismatch at index %d: C = %d, C_prime = %d\n",
                        i, C_int[i], C_prime_int[i]);
            }
        }
    }

    if (mismatch_count != 0) {
        fprintf(stderr, "\033[31;1mFound %d mismatches total\033[0m\n", mismatch_count);
        exit(EXIT_FAILURE);
    } else {
        printf("\033[32;1mPerfect Match!\033[0m\n");
    }

    

}

void accelerator_pack(short *A, short *B, int *A_int, int *B_int, int M, int N, int K)
{
    for(int i = 0; i < M*K; i++) {
        int val = A_int[i/2];
        if (i & 1) {
            val &= LOWER_HALF;
            val |= (A[i] << 16) & UPPER_HALF;
        } else {
            val &= UPPER_HALF;
            val |= (A[i] & LOWER_HALF) & 0x00FF;
        }
        A_int[i/2] = val;
    }

    for(int i = 0; i < N*K; i++) {
        int val = B_int[i/2];
        if (i & 1) {
            val &= LOWER_HALF;
            val |= (B[i] << 16) & UPPER_HALF;
        } else {
            val &= UPPER_HALF;
            val |= (B[i] & LOWER_HALF) & 0x00FF;
        }
        B_int[i/2] = val;
    }

}

void packing_check(short *A, short *B, int *A_int, int *B_int, int M, int N, int K)
{
    for (int i = 0; i < M * K; i++) {
        int val = A_int[i/2];
        short a_data;
        if (i & 1) {
            a_data = (val & UPPER_HALF) >> 16;
        } else {
            a_data = (val & LOWER_HALF);
        }

        if (a_data != A[i]) {
            printf("mistmatch at %d: A: %d a_data: %d A_int: %x\n", i, A[i], a_data, A_int[i/2]);
            exit(1);
        }
    }

    for (int i = 0; i < N * K; i++) {
        int val = B_int[i/2];
        short b_data;
        if (i & 1) {
            b_data = (val & UPPER_HALF) >> 16;
        } else {
            b_data = (val & LOWER_HALF);
        }

        if (b_data != B[i]) {
            printf("mistmatch at %d: B: %d b_data: %d B_int: %x\n", i, B[i], b_data, B_int[i/2]);
            exit(1);
        }
    }
    printf("Passed packing test\n");
}

void matmul_test()
{
    // These are passed to gemm
    int layer_M[] = {16, 32, 64, 128, 256, 512, 1024, 256, 512, 255, 128, 256, 255};
    int layer_K[] = {27, 144, 288, 576, 1152, 2304, 4608, 1024, 2304, 512, 256, 3456, 256};
    int layer_N[] = {102400,25600,6400,1600,400,100,100,100,100,100,100,400,400};
    
    // Approximation
    for(int darknet_layer = 4; darknet_layer < 13; darknet_layer++){
        printf("running layer %d\n", darknet_layer + 1);

        // Normally Given as input simulated for now
        unsigned int M = layer_M[darknet_layer];
        unsigned int K = layer_K[darknet_layer];
        unsigned int N = layer_N[darknet_layer];

        // Generate Matrices
        short *A = (short *)malloc(M * K * SIZE_OF_A);
        short *B = (short *)malloc(N * K * SIZE_OF_A);
        int *A_int = (int *)malloc(M * K * SIZE_OF_A);        
        int *B_int = (int *)malloc(N * K * SIZE_OF_A);        
        int *C_int = (int *)malloc(M * N * SIZE_OF_A);        
        short *C_prime =   (short *)malloc(M * N * SIZE_OF_RESULT);

        for (int i = 0; i < M * K; i++) {
            A[i] = rand() % 10;
        }

        for (int i = 0; i < N * K; i++) {
            B[i] = rand() % 10;
        }


#ifndef ACC
        accelerator_pack(A, B, A_int, B_int, M, N, K);
//        packing_check(A, B, A_int, B_int, M, N, K);
        gemm_app_pack(M, K, N, A_int, B_int, C_int);
        gemm_check_packed(M, N,K,A, B,C_prime,A_int,B_int, C_int);
#else
        accelerator_pack(A, B, A_int, B_int, M, N, K);
        gemm_acc_pack(M, K, N, A_int, B_int, C_int);
        gemm_check_packed(M, N, K, A, B, C_prime, A_int, B_int, C_int);
#endif

    }
    printf("run complete\n");

}


