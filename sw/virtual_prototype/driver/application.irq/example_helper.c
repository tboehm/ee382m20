#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "example_helper.h"

unsigned int *addr;
void *base;


int det_int = 0;

int dev_fd;
int fd;

// When we try to memcpy odd-sized arrays, we see the writes show up multiple times on the SystemC
// side. The initial values are correct, but then we see several additional writes with zeros. Not
// sure why this happens, but using our own basic memcpy implementation fixes it.

void short_memcpy(short *restrict dst, const short *restrict src, int count)
{
    for (int i = 0; i < count; i++) {
       dst[i] = src[i];
    }
}

void int_memcpy(int *restrict dst, const int *restrict src, int count)
{
    for (int i = 0; i < count; i++) {
       dst[i] = src[i];
    }
}


// signal handler for receiving events from hardware driver
void sighandler(int signo)
{
    if (signo==SIGIO) {
        det_int++;
        printf(">>> Interrupt detected <<<\n");
        printf("det_int: %d\n", det_int);
        fflush(stdout);
    }
    return;
}

void accelerator_setup_buffer1(unsigned int M, unsigned int N, unsigned int K, unsigned int newM, unsigned int B1, unsigned int C1)
{
    // Write M
    addr = MAKE_ADDR(base, REG_M_1);
    *addr = M;

    // Write K
    addr = MAKE_ADDR(base, REG_K_1);
    *addr = K;

    // Write N
    addr = MAKE_ADDR(base, REG_N_1);
    *addr = N;

    // Write newM
    addr = MAKE_ADDR(base, REG_newM_1);
    *addr = newM;

    // Write SP Regions 
    addr = MAKE_ADDR(base, REG_B1_SP_START);
    *addr = B1;

    addr = MAKE_ADDR(base, REG_C1_SP_START);
    *addr = C1;
}

void accelerator_c1sp_update(unsigned int C1)
{
     addr = MAKE_ADDR(base, REG_C1_SP_START);
    *addr = C1;  
}

void accelerator_start_buffer1()
{
    addr = MAKE_ADDR(base, REG_START_1);
    *addr = 1;
}

void accelerator_send_array(short *data, int size, int dest)
{
    addr = MAKE_ADDR(base, dest);
    short_memcpy((short *)addr, (short*) data, size);
}

void accelerator_send_int_array(int *data, int size, int dest)
{
    addr = MAKE_ADDR(base, dest);
    int_memcpy((int *)addr, (int*) data, size);
}

void accelerator_get_result(short *data, int size, int source)
{
    addr = MAKE_ADDR(base, source);
    short_memcpy((short*) data, (short*) addr, size);
}

void accelerator_get_int_result(int *data, int size, int source)
{
    addr = MAKE_ADDR(base, source);
    int_memcpy((int*) data, (int*) addr, size);
}

// One time setup for driver and such
void accelerator_init(void)
{
    struct sigaction action;

    // install signal handler
    sigemptyset(&action.sa_mask);
    sigaddset(&action.sa_mask, SIGIO);

    action.sa_handler = sighandler;
    action.sa_flags=0;

    sigaction(SIGIO, &action, NULL);

    pid_t pid = getpid();
    if (pid < 0) {
        perror("getpid");
        exit(EXIT_FAILURE);
    }

    // Open the FPGA device file.
    dev_fd = open("/dev/fpga", O_RDWR);

    if (dev_fd < 0) {
        perror("open(/dev/fpga)");
        exit(EXIT_FAILURE);
    }

    if (fcntl(dev_fd, F_SETOWN, pid)) {
        perror("fcntl(F_SETOWN)");
        exit(EXIT_FAILURE);
    }

    // Enable reception of signals for the kernel module.
    int fd_flags = fcntl(dev_fd, F_GETFL);

    if (fd_flags < 0) {
        perror("fcntl(GETFL)");
        exit(EXIT_FAILURE);
    }

    if (fcntl(dev_fd, F_SETFL, fd_flags | O_ASYNC) < 0) {
        perror("fcntl(SETFL)");
        exit(EXIT_FAILURE);
    }

    if (fcntl(dev_fd, F_SETSIG, SIGIO)) {
        perror("fcntl(SETSIG)");
        exit(EXIT_FAILURE);
    }

    // open hardware device (driver)
    fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (!fd) {
        fprintf(stderr, "Unable to open /dev/mem.\n");
        perror("open");
        exit(EXIT_FAILURE);
    }

    base = (void *)mmap(NULL, FPGA_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, FPGA_BASE);
    if (base == MAP_FAILED) {
        perror("mmap");
        fflush(stdout);
        fflush(stderr);
        exit(EXIT_FAILURE);
    }
    printf("base: %u\n", base);

    printf("Accelerator setup\n");
}

void accelerator_finish()
{
    close(fd);
    close(dev_fd);
}
