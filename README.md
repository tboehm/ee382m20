To Run:
1. cd to sw/darknet and make
2. cat the bitstream located at hw/bitstream
3. Run darknet using following command: sudo ./darknet detect cfg/yolov3-tiny.cfg  convert/converted.weights data/person.jpg hw-only
4. Change hw-only to sw-only or hw-sw to see different options
