# ==============================================================
# File generated on Wed Dec 01 11:21:11 -0600 2021
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
# SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
# IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
# ==============================================================
add_files -tb ../../gemm_sw/gemm_tb.c -cflags { -Wno-unknown-pragmas}
add_files gemm_sw/gemm.c
set_part xczu3eg-sbva484-1-e
create_clock -name default -period 100MHz
config_export -format=ip_catalog
config_export -rtl=verilog
