// ==============================================================
// File generated on Wed Dec 08 08:52:26 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XGEMM_SYNTH_H
#define XGEMM_SYNTH_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xgemm_synth_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Hls_gemm_periph_bus_BaseAddress;
} XGemm_synth_Config;
#endif

typedef struct {
    u32 Hls_gemm_periph_bus_BaseAddress;
    u32 IsReady;
} XGemm_synth;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XGemm_synth_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XGemm_synth_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XGemm_synth_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XGemm_synth_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XGemm_synth_Initialize(XGemm_synth *InstancePtr, u16 DeviceId);
XGemm_synth_Config* XGemm_synth_LookupConfig(u16 DeviceId);
int XGemm_synth_CfgInitialize(XGemm_synth *InstancePtr, XGemm_synth_Config *ConfigPtr);
#else
int XGemm_synth_Initialize(XGemm_synth *InstancePtr, const char* InstanceName);
int XGemm_synth_Release(XGemm_synth *InstancePtr);
#endif

void XGemm_synth_Start(XGemm_synth *InstancePtr);
u32 XGemm_synth_IsDone(XGemm_synth *InstancePtr);
u32 XGemm_synth_IsIdle(XGemm_synth *InstancePtr);
u32 XGemm_synth_IsReady(XGemm_synth *InstancePtr);
void XGemm_synth_EnableAutoRestart(XGemm_synth *InstancePtr);
void XGemm_synth_DisableAutoRestart(XGemm_synth *InstancePtr);

void XGemm_synth_Set_START1(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_START1(XGemm_synth *InstancePtr);
void XGemm_synth_Set_M1(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_M1(XGemm_synth *InstancePtr);
void XGemm_synth_Set_N1(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_N1(XGemm_synth *InstancePtr);
void XGemm_synth_Set_K1(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_K1(XGemm_synth *InstancePtr);
void XGemm_synth_Set_new_M1(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_new_M1(XGemm_synth *InstancePtr);
void XGemm_synth_Set_B1_SP_START(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_B1_SP_START(XGemm_synth *InstancePtr);
void XGemm_synth_Set_C1_SP_START(XGemm_synth *InstancePtr, u32 Data);
u32 XGemm_synth_Get_C1_SP_START(XGemm_synth *InstancePtr);

void XGemm_synth_InterruptGlobalEnable(XGemm_synth *InstancePtr);
void XGemm_synth_InterruptGlobalDisable(XGemm_synth *InstancePtr);
void XGemm_synth_InterruptEnable(XGemm_synth *InstancePtr, u32 Mask);
void XGemm_synth_InterruptDisable(XGemm_synth *InstancePtr, u32 Mask);
void XGemm_synth_InterruptClear(XGemm_synth *InstancePtr, u32 Mask);
u32 XGemm_synth_InterruptGetEnabled(XGemm_synth *InstancePtr);
u32 XGemm_synth_InterruptGetStatus(XGemm_synth *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
