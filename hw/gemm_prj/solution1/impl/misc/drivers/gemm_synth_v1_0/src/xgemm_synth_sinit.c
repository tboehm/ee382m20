// ==============================================================
// File generated on Wed Dec 08 08:52:26 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xgemm_synth.h"

extern XGemm_synth_Config XGemm_synth_ConfigTable[];

XGemm_synth_Config *XGemm_synth_LookupConfig(u16 DeviceId) {
	XGemm_synth_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XGEMM_SYNTH_NUM_INSTANCES; Index++) {
		if (XGemm_synth_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XGemm_synth_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XGemm_synth_Initialize(XGemm_synth *InstancePtr, u16 DeviceId) {
	XGemm_synth_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XGemm_synth_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XGemm_synth_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

