// ==============================================================
// File generated on Wed Dec 08 08:52:26 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xgemm_synth.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XGemm_synth_CfgInitialize(XGemm_synth *InstancePtr, XGemm_synth_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Hls_gemm_periph_bus_BaseAddress = ConfigPtr->Hls_gemm_periph_bus_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XGemm_synth_Start(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL) & 0x80;
    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL, Data | 0x01);
}

u32 XGemm_synth_IsDone(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XGemm_synth_IsIdle(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XGemm_synth_IsReady(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XGemm_synth_EnableAutoRestart(XGemm_synth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL, 0x80);
}

void XGemm_synth_DisableAutoRestart(XGemm_synth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL, 0);
}

void XGemm_synth_Set_START1(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_START1_DATA, Data);
}

u32 XGemm_synth_Get_START1(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_START1_DATA);
    return Data;
}

void XGemm_synth_Set_M1(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_M1_DATA, Data);
}

u32 XGemm_synth_Get_M1(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_M1_DATA);
    return Data;
}

void XGemm_synth_Set_N1(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_N1_DATA, Data);
}

u32 XGemm_synth_Get_N1(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_N1_DATA);
    return Data;
}

void XGemm_synth_Set_K1(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_K1_DATA, Data);
}

u32 XGemm_synth_Get_K1(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_K1_DATA);
    return Data;
}

void XGemm_synth_Set_new_M1(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_NEW_M1_DATA, Data);
}

u32 XGemm_synth_Get_new_M1(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_NEW_M1_DATA);
    return Data;
}

void XGemm_synth_Set_B1_SP_START(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_B1_SP_START_DATA, Data);
}

u32 XGemm_synth_Get_B1_SP_START(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_B1_SP_START_DATA);
    return Data;
}

void XGemm_synth_Set_C1_SP_START(XGemm_synth *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_C1_SP_START_DATA, Data);
}

u32 XGemm_synth_Get_C1_SP_START(XGemm_synth *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_C1_SP_START_DATA);
    return Data;
}

void XGemm_synth_InterruptGlobalEnable(XGemm_synth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_GIE, 1);
}

void XGemm_synth_InterruptGlobalDisable(XGemm_synth *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_GIE, 0);
}

void XGemm_synth_InterruptEnable(XGemm_synth *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER);
    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER, Register | Mask);
}

void XGemm_synth_InterruptDisable(XGemm_synth *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER);
    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER, Register & (~Mask));
}

void XGemm_synth_InterruptClear(XGemm_synth *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XGemm_synth_WriteReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_ISR, Mask);
}

u32 XGemm_synth_InterruptGetEnabled(XGemm_synth *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER);
}

u32 XGemm_synth_InterruptGetStatus(XGemm_synth *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XGemm_synth_ReadReg(InstancePtr->Hls_gemm_periph_bus_BaseAddress, XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_ISR);
}

