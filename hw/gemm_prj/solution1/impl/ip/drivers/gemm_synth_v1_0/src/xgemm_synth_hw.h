// ==============================================================
// File generated on Wed Dec 08 08:52:26 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// HLS_GEMM_PERIPH_BUS
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x10 : Data signal of START1
//        bit 0  - START1[0] (Read/Write)
//        others - reserved
// 0x14 : reserved
// 0x18 : Data signal of M1
//        bit 31~0 - M1[31:0] (Read/Write)
// 0x1c : reserved
// 0x20 : Data signal of N1
//        bit 31~0 - N1[31:0] (Read/Write)
// 0x24 : reserved
// 0x28 : Data signal of K1
//        bit 31~0 - K1[31:0] (Read/Write)
// 0x2c : reserved
// 0x30 : Data signal of new_M1
//        bit 31~0 - new_M1[31:0] (Read/Write)
// 0x34 : reserved
// 0x38 : Data signal of B1_SP_START
//        bit 31~0 - B1_SP_START[31:0] (Read/Write)
// 0x3c : reserved
// 0x40 : Data signal of C1_SP_START
//        bit 31~0 - C1_SP_START[31:0] (Read/Write)
// 0x44 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_AP_CTRL          0x00
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_GIE              0x04
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_IER              0x08
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_ISR              0x0c
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_START1_DATA      0x10
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_START1_DATA      1
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_M1_DATA          0x18
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_M1_DATA          32
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_N1_DATA          0x20
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_N1_DATA          32
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_K1_DATA          0x28
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_K1_DATA          32
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_NEW_M1_DATA      0x30
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_NEW_M1_DATA      32
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_B1_SP_START_DATA 0x38
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_B1_SP_START_DATA 32
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_ADDR_C1_SP_START_DATA 0x40
#define XGEMM_SYNTH_HLS_GEMM_PERIPH_BUS_BITS_C1_SP_START_DATA 32

