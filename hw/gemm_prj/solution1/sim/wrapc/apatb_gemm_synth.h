// ==============================================================
// File generated on Sun Nov 28 11:49:38 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern void AESL_WRAP_gemm_synth (
int M1,
int K1,
int N1,
int B1_SP_START,
int C1_SP_START,
int1 START1,
int1 CLEAR1,
int M2,
int K2,
int N2,
int B2_SP_START,
int C2_SP_START,
int1 START2,
int1 CLEAR2,
short SP1[75497472],
short SP2[75497472]);
