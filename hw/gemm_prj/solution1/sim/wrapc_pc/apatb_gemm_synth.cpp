// ==============================================================
// File generated on Sun Nov 28 11:49:38 -0600 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;

// apint = int1
#define int1 bool

// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->


// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "M1"
#define AUTOTB_TVIN_M1  "../tv/cdatafile/c.gemm_synth.autotvin_M1.dat"
// wrapc file define: "K1"
#define AUTOTB_TVIN_K1  "../tv/cdatafile/c.gemm_synth.autotvin_K1.dat"
// wrapc file define: "N1"
#define AUTOTB_TVIN_N1  "../tv/cdatafile/c.gemm_synth.autotvin_N1.dat"
// wrapc file define: "B1_SP_START"
#define AUTOTB_TVIN_B1_SP_START  "../tv/cdatafile/c.gemm_synth.autotvin_B1_SP_START.dat"
// wrapc file define: "C1_SP_START"
#define AUTOTB_TVIN_C1_SP_START  "../tv/cdatafile/c.gemm_synth.autotvin_C1_SP_START.dat"
// wrapc file define: "START1"
#define AUTOTB_TVIN_START1  "../tv/cdatafile/c.gemm_synth.autotvin_START1.dat"
// wrapc file define: "CLEAR1"
#define AUTOTB_TVIN_CLEAR1  "../tv/cdatafile/c.gemm_synth.autotvin_CLEAR1.dat"
// wrapc file define: "M2"
#define AUTOTB_TVIN_M2  "../tv/cdatafile/c.gemm_synth.autotvin_M2.dat"
// wrapc file define: "K2"
#define AUTOTB_TVIN_K2  "../tv/cdatafile/c.gemm_synth.autotvin_K2.dat"
// wrapc file define: "N2"
#define AUTOTB_TVIN_N2  "../tv/cdatafile/c.gemm_synth.autotvin_N2.dat"
// wrapc file define: "B2_SP_START"
#define AUTOTB_TVIN_B2_SP_START  "../tv/cdatafile/c.gemm_synth.autotvin_B2_SP_START.dat"
// wrapc file define: "C2_SP_START"
#define AUTOTB_TVIN_C2_SP_START  "../tv/cdatafile/c.gemm_synth.autotvin_C2_SP_START.dat"
// wrapc file define: "START2"
#define AUTOTB_TVIN_START2  "../tv/cdatafile/c.gemm_synth.autotvin_START2.dat"
// wrapc file define: "CLEAR2"
#define AUTOTB_TVIN_CLEAR2  "../tv/cdatafile/c.gemm_synth.autotvin_CLEAR2.dat"
// wrapc file define: "SP1"
#define AUTOTB_TVIN_SP1  "../tv/cdatafile/c.gemm_synth.autotvin_SP1.dat"
#define AUTOTB_TVOUT_SP1  "../tv/cdatafile/c.gemm_synth.autotvout_SP1.dat"
// wrapc file define: "SP2"
#define AUTOTB_TVIN_SP2  "../tv/cdatafile/c.gemm_synth.autotvin_SP2.dat"
#define AUTOTB_TVOUT_SP2  "../tv/cdatafile/c.gemm_synth.autotvout_SP2.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "SP1"
#define AUTOTB_TVOUT_PC_SP1  "../tv/rtldatafile/rtl.gemm_synth.autotvout_SP1.dat"
// tvout file define: "SP2"
#define AUTOTB_TVOUT_PC_SP2  "../tv/rtldatafile/rtl.gemm_synth.autotvout_SP2.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			M1_depth = 0;
			K1_depth = 0;
			N1_depth = 0;
			B1_SP_START_depth = 0;
			C1_SP_START_depth = 0;
			START1_depth = 0;
			CLEAR1_depth = 0;
			M2_depth = 0;
			K2_depth = 0;
			N2_depth = 0;
			B2_SP_START_depth = 0;
			C2_SP_START_depth = 0;
			START2_depth = 0;
			CLEAR2_depth = 0;
			SP1_depth = 0;
			SP2_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{M1 " << M1_depth << "}\n";
			total_list << "{K1 " << K1_depth << "}\n";
			total_list << "{N1 " << N1_depth << "}\n";
			total_list << "{B1_SP_START " << B1_SP_START_depth << "}\n";
			total_list << "{C1_SP_START " << C1_SP_START_depth << "}\n";
			total_list << "{START1 " << START1_depth << "}\n";
			total_list << "{CLEAR1 " << CLEAR1_depth << "}\n";
			total_list << "{M2 " << M2_depth << "}\n";
			total_list << "{K2 " << K2_depth << "}\n";
			total_list << "{N2 " << N2_depth << "}\n";
			total_list << "{B2_SP_START " << B2_SP_START_depth << "}\n";
			total_list << "{C2_SP_START " << C2_SP_START_depth << "}\n";
			total_list << "{START2 " << START2_depth << "}\n";
			total_list << "{CLEAR2 " << CLEAR2_depth << "}\n";
			total_list << "{SP1 " << SP1_depth << "}\n";
			total_list << "{SP2 " << SP2_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int M1_depth;
		int K1_depth;
		int N1_depth;
		int B1_SP_START_depth;
		int C1_SP_START_depth;
		int START1_depth;
		int CLEAR1_depth;
		int M2_depth;
		int K2_depth;
		int N2_depth;
		int B2_SP_START_depth;
		int C2_SP_START_depth;
		int START2_depth;
		int CLEAR2_depth;
		int SP1_depth;
		int SP2_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};

extern "C" void gemm_synth (
int M1,
int K1,
int N1,
int B1_SP_START,
int C1_SP_START,
int1 START1,
int1 CLEAR1,
int M2,
int K2,
int N2,
int B2_SP_START,
int C2_SP_START,
int1 START2,
int1 CLEAR2,
short SP1[75497472],
short SP2[75497472]);

extern "C" void AESL_WRAP_gemm_synth (
int M1,
int K1,
int N1,
int B1_SP_START,
int C1_SP_START,
int1 START1,
int1 CLEAR1,
int M2,
int K2,
int N2,
int B2_SP_START,
int C2_SP_START,
int1 START2,
int1 CLEAR2,
short SP1[75497472],
short SP2[75497472])
{
	refine_signal_handler();
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		CodeState = ENTER_WRAPC_PC;
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;


		// output port post check: "SP1"
		aesl_fh.read(AUTOTB_TVOUT_PC_SP1, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_SP1, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_SP1, AESL_token); // data

			sc_bv<16> *SP1_pc_buffer = new sc_bv<16>[75497472];
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'SP1', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'SP1', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					SP1_pc_buffer[i] = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_SP1, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_SP1))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: SP1
				{
					// bitslice(15, 0)
					// {
						// celement: SP1(15, 0)
						// {
							sc_lv<16>* SP1_lv0_0_75497471_1 = new sc_lv<16>[75497472];
						// }
					// }

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: SP1(15, 0)
						{
							// carray: (0) => (75497471) @ (1)
							for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
							{
								if (&(SP1[0]) != NULL) // check the null address if the c port is array or others
								{
									SP1_lv0_0_75497471_1[hls_map_index].range(15, 0) = sc_bv<16>(SP1_pc_buffer[hls_map_index].range(15, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: SP1(15, 0)
						{
							// carray: (0) => (75497471) @ (1)
							for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : SP1[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : SP1[0]
								// output_left_conversion : SP1[i_0]
								// output_type_conversion : (SP1_lv0_0_75497471_1[hls_map_index]).to_uint64()
								if (&(SP1[0]) != NULL) // check the null address if the c port is array or others
								{
									SP1[i_0] = (SP1_lv0_0_75497471_1[hls_map_index]).to_uint64();
									hls_map_index++;
								}
							}
						}
					}
				}
			}

			// release memory allocation
			delete [] SP1_pc_buffer;
		}

		// output port post check: "SP2"
		aesl_fh.read(AUTOTB_TVOUT_PC_SP2, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_SP2, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_SP2, AESL_token); // data

			sc_bv<16> *SP2_pc_buffer = new sc_bv<16>[75497472];
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'SP2', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'SP2', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					SP2_pc_buffer[i] = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_SP2, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_SP2))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: SP2
				{
					// bitslice(15, 0)
					// {
						// celement: SP2(15, 0)
						// {
							sc_lv<16>* SP2_lv0_0_75497471_1 = new sc_lv<16>[75497472];
						// }
					// }

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: SP2(15, 0)
						{
							// carray: (0) => (75497471) @ (1)
							for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
							{
								if (&(SP2[0]) != NULL) // check the null address if the c port is array or others
								{
									SP2_lv0_0_75497471_1[hls_map_index].range(15, 0) = sc_bv<16>(SP2_pc_buffer[hls_map_index].range(15, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: SP2(15, 0)
						{
							// carray: (0) => (75497471) @ (1)
							for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : SP2[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : SP2[0]
								// output_left_conversion : SP2[i_0]
								// output_type_conversion : (SP2_lv0_0_75497471_1[hls_map_index]).to_uint64()
								if (&(SP2[0]) != NULL) // check the null address if the c port is array or others
								{
									SP2[i_0] = (SP2_lv0_0_75497471_1[hls_map_index]).to_uint64();
									hls_map_index++;
								}
							}
						}
					}
				}
			}

			// release memory allocation
			delete [] SP2_pc_buffer;
		}

		AESL_transaction_pc++;
	}
	else
	{
		CodeState = ENTER_WRAPC;
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "M1"
		char* tvin_M1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_M1);

		// "K1"
		char* tvin_K1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_K1);

		// "N1"
		char* tvin_N1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_N1);

		// "B1_SP_START"
		char* tvin_B1_SP_START = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_B1_SP_START);

		// "C1_SP_START"
		char* tvin_C1_SP_START = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_C1_SP_START);

		// "START1"
		char* tvin_START1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_START1);

		// "CLEAR1"
		char* tvin_CLEAR1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_CLEAR1);

		// "M2"
		char* tvin_M2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_M2);

		// "K2"
		char* tvin_K2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_K2);

		// "N2"
		char* tvin_N2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_N2);

		// "B2_SP_START"
		char* tvin_B2_SP_START = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_B2_SP_START);

		// "C2_SP_START"
		char* tvin_C2_SP_START = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_C2_SP_START);

		// "START2"
		char* tvin_START2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_START2);

		// "CLEAR2"
		char* tvin_CLEAR2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_CLEAR2);

		// "SP1"
		char* tvin_SP1 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_SP1);
		char* tvout_SP1 = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_SP1);

		// "SP2"
		char* tvin_SP2 = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_SP2);
		char* tvout_SP2 = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_SP2);

		CodeState = DUMP_INPUTS;
		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// [[transaction]]
		sprintf(tvin_M1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_M1, tvin_M1);

		sc_bv<32> M1_tvin_wrapc_buffer;

		// RTL Name: M1
		{
			// bitslice(31, 0)
			{
				// celement: M1(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : M1
						// sub_1st_elem          : 
						// ori_name_1st_elem     : M1
						// regulate_c_name       : M1
						// input_type_conversion : M1
						if (&(M1) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> M1_tmp_mem;
							M1_tmp_mem = M1;
							M1_tvin_wrapc_buffer.range(31, 0) = M1_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_M1, "%s\n", (M1_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_M1, tvin_M1);
		}

		tcl_file.set_num(1, &tcl_file.M1_depth);
		sprintf(tvin_M1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_M1, tvin_M1);

		// [[transaction]]
		sprintf(tvin_K1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_K1, tvin_K1);

		sc_bv<32> K1_tvin_wrapc_buffer;

		// RTL Name: K1
		{
			// bitslice(31, 0)
			{
				// celement: K1(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : K1
						// sub_1st_elem          : 
						// ori_name_1st_elem     : K1
						// regulate_c_name       : K1
						// input_type_conversion : K1
						if (&(K1) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> K1_tmp_mem;
							K1_tmp_mem = K1;
							K1_tvin_wrapc_buffer.range(31, 0) = K1_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_K1, "%s\n", (K1_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_K1, tvin_K1);
		}

		tcl_file.set_num(1, &tcl_file.K1_depth);
		sprintf(tvin_K1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_K1, tvin_K1);

		// [[transaction]]
		sprintf(tvin_N1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_N1, tvin_N1);

		sc_bv<32> N1_tvin_wrapc_buffer;

		// RTL Name: N1
		{
			// bitslice(31, 0)
			{
				// celement: N1(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : N1
						// sub_1st_elem          : 
						// ori_name_1st_elem     : N1
						// regulate_c_name       : N1
						// input_type_conversion : N1
						if (&(N1) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> N1_tmp_mem;
							N1_tmp_mem = N1;
							N1_tvin_wrapc_buffer.range(31, 0) = N1_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_N1, "%s\n", (N1_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_N1, tvin_N1);
		}

		tcl_file.set_num(1, &tcl_file.N1_depth);
		sprintf(tvin_N1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_N1, tvin_N1);

		// [[transaction]]
		sprintf(tvin_B1_SP_START, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_B1_SP_START, tvin_B1_SP_START);

		sc_bv<32> B1_SP_START_tvin_wrapc_buffer;

		// RTL Name: B1_SP_START
		{
			// bitslice(31, 0)
			{
				// celement: B1_SP_START(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : B1_SP_START
						// sub_1st_elem          : 
						// ori_name_1st_elem     : B1_SP_START
						// regulate_c_name       : B1_SP_START
						// input_type_conversion : B1_SP_START
						if (&(B1_SP_START) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> B1_SP_START_tmp_mem;
							B1_SP_START_tmp_mem = B1_SP_START;
							B1_SP_START_tvin_wrapc_buffer.range(31, 0) = B1_SP_START_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_B1_SP_START, "%s\n", (B1_SP_START_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_B1_SP_START, tvin_B1_SP_START);
		}

		tcl_file.set_num(1, &tcl_file.B1_SP_START_depth);
		sprintf(tvin_B1_SP_START, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_B1_SP_START, tvin_B1_SP_START);

		// [[transaction]]
		sprintf(tvin_C1_SP_START, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_C1_SP_START, tvin_C1_SP_START);

		sc_bv<32> C1_SP_START_tvin_wrapc_buffer;

		// RTL Name: C1_SP_START
		{
			// bitslice(31, 0)
			{
				// celement: C1_SP_START(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : C1_SP_START
						// sub_1st_elem          : 
						// ori_name_1st_elem     : C1_SP_START
						// regulate_c_name       : C1_SP_START
						// input_type_conversion : C1_SP_START
						if (&(C1_SP_START) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> C1_SP_START_tmp_mem;
							C1_SP_START_tmp_mem = C1_SP_START;
							C1_SP_START_tvin_wrapc_buffer.range(31, 0) = C1_SP_START_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_C1_SP_START, "%s\n", (C1_SP_START_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_C1_SP_START, tvin_C1_SP_START);
		}

		tcl_file.set_num(1, &tcl_file.C1_SP_START_depth);
		sprintf(tvin_C1_SP_START, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_C1_SP_START, tvin_C1_SP_START);

		// [[transaction]]
		sprintf(tvin_START1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_START1, tvin_START1);

		sc_bv<1> START1_tvin_wrapc_buffer;

		// RTL Name: START1
		{
			// bitslice(0, 0)
			{
				// celement: START1(0, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : START1
						// sub_1st_elem          : 
						// ori_name_1st_elem     : START1
						// regulate_c_name       : START1
						// input_type_conversion : START1
						if (&(START1) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<1> START1_tmp_mem;
							START1_tmp_mem = START1;
							START1_tvin_wrapc_buffer.range(0, 0) = START1_tmp_mem.range(0, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_START1, "%s\n", (START1_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_START1, tvin_START1);
		}

		tcl_file.set_num(1, &tcl_file.START1_depth);
		sprintf(tvin_START1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_START1, tvin_START1);

		// [[transaction]]
		sprintf(tvin_CLEAR1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_CLEAR1, tvin_CLEAR1);

		sc_bv<1> CLEAR1_tvin_wrapc_buffer;

		// RTL Name: CLEAR1
		{
			// bitslice(0, 0)
			{
				// celement: CLEAR1(0, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : CLEAR1
						// sub_1st_elem          : 
						// ori_name_1st_elem     : CLEAR1
						// regulate_c_name       : CLEAR1
						// input_type_conversion : CLEAR1
						if (&(CLEAR1) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<1> CLEAR1_tmp_mem;
							CLEAR1_tmp_mem = CLEAR1;
							CLEAR1_tvin_wrapc_buffer.range(0, 0) = CLEAR1_tmp_mem.range(0, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_CLEAR1, "%s\n", (CLEAR1_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_CLEAR1, tvin_CLEAR1);
		}

		tcl_file.set_num(1, &tcl_file.CLEAR1_depth);
		sprintf(tvin_CLEAR1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_CLEAR1, tvin_CLEAR1);

		// [[transaction]]
		sprintf(tvin_M2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_M2, tvin_M2);

		sc_bv<32> M2_tvin_wrapc_buffer;

		// RTL Name: M2
		{
			// bitslice(31, 0)
			{
				// celement: M2(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : M2
						// sub_1st_elem          : 
						// ori_name_1st_elem     : M2
						// regulate_c_name       : M2
						// input_type_conversion : M2
						if (&(M2) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> M2_tmp_mem;
							M2_tmp_mem = M2;
							M2_tvin_wrapc_buffer.range(31, 0) = M2_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_M2, "%s\n", (M2_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_M2, tvin_M2);
		}

		tcl_file.set_num(1, &tcl_file.M2_depth);
		sprintf(tvin_M2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_M2, tvin_M2);

		// [[transaction]]
		sprintf(tvin_K2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_K2, tvin_K2);

		sc_bv<32> K2_tvin_wrapc_buffer;

		// RTL Name: K2
		{
			// bitslice(31, 0)
			{
				// celement: K2(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : K2
						// sub_1st_elem          : 
						// ori_name_1st_elem     : K2
						// regulate_c_name       : K2
						// input_type_conversion : K2
						if (&(K2) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> K2_tmp_mem;
							K2_tmp_mem = K2;
							K2_tvin_wrapc_buffer.range(31, 0) = K2_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_K2, "%s\n", (K2_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_K2, tvin_K2);
		}

		tcl_file.set_num(1, &tcl_file.K2_depth);
		sprintf(tvin_K2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_K2, tvin_K2);

		// [[transaction]]
		sprintf(tvin_N2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_N2, tvin_N2);

		sc_bv<32> N2_tvin_wrapc_buffer;

		// RTL Name: N2
		{
			// bitslice(31, 0)
			{
				// celement: N2(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : N2
						// sub_1st_elem          : 
						// ori_name_1st_elem     : N2
						// regulate_c_name       : N2
						// input_type_conversion : N2
						if (&(N2) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> N2_tmp_mem;
							N2_tmp_mem = N2;
							N2_tvin_wrapc_buffer.range(31, 0) = N2_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_N2, "%s\n", (N2_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_N2, tvin_N2);
		}

		tcl_file.set_num(1, &tcl_file.N2_depth);
		sprintf(tvin_N2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_N2, tvin_N2);

		// [[transaction]]
		sprintf(tvin_B2_SP_START, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_B2_SP_START, tvin_B2_SP_START);

		sc_bv<32> B2_SP_START_tvin_wrapc_buffer;

		// RTL Name: B2_SP_START
		{
			// bitslice(31, 0)
			{
				// celement: B2_SP_START(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : B2_SP_START
						// sub_1st_elem          : 
						// ori_name_1st_elem     : B2_SP_START
						// regulate_c_name       : B2_SP_START
						// input_type_conversion : B2_SP_START
						if (&(B2_SP_START) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> B2_SP_START_tmp_mem;
							B2_SP_START_tmp_mem = B2_SP_START;
							B2_SP_START_tvin_wrapc_buffer.range(31, 0) = B2_SP_START_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_B2_SP_START, "%s\n", (B2_SP_START_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_B2_SP_START, tvin_B2_SP_START);
		}

		tcl_file.set_num(1, &tcl_file.B2_SP_START_depth);
		sprintf(tvin_B2_SP_START, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_B2_SP_START, tvin_B2_SP_START);

		// [[transaction]]
		sprintf(tvin_C2_SP_START, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_C2_SP_START, tvin_C2_SP_START);

		sc_bv<32> C2_SP_START_tvin_wrapc_buffer;

		// RTL Name: C2_SP_START
		{
			// bitslice(31, 0)
			{
				// celement: C2_SP_START(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : C2_SP_START
						// sub_1st_elem          : 
						// ori_name_1st_elem     : C2_SP_START
						// regulate_c_name       : C2_SP_START
						// input_type_conversion : C2_SP_START
						if (&(C2_SP_START) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> C2_SP_START_tmp_mem;
							C2_SP_START_tmp_mem = C2_SP_START;
							C2_SP_START_tvin_wrapc_buffer.range(31, 0) = C2_SP_START_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_C2_SP_START, "%s\n", (C2_SP_START_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_C2_SP_START, tvin_C2_SP_START);
		}

		tcl_file.set_num(1, &tcl_file.C2_SP_START_depth);
		sprintf(tvin_C2_SP_START, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_C2_SP_START, tvin_C2_SP_START);

		// [[transaction]]
		sprintf(tvin_START2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_START2, tvin_START2);

		sc_bv<1> START2_tvin_wrapc_buffer;

		// RTL Name: START2
		{
			// bitslice(0, 0)
			{
				// celement: START2(0, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : START2
						// sub_1st_elem          : 
						// ori_name_1st_elem     : START2
						// regulate_c_name       : START2
						// input_type_conversion : START2
						if (&(START2) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<1> START2_tmp_mem;
							START2_tmp_mem = START2;
							START2_tvin_wrapc_buffer.range(0, 0) = START2_tmp_mem.range(0, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_START2, "%s\n", (START2_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_START2, tvin_START2);
		}

		tcl_file.set_num(1, &tcl_file.START2_depth);
		sprintf(tvin_START2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_START2, tvin_START2);

		// [[transaction]]
		sprintf(tvin_CLEAR2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_CLEAR2, tvin_CLEAR2);

		sc_bv<1> CLEAR2_tvin_wrapc_buffer;

		// RTL Name: CLEAR2
		{
			// bitslice(0, 0)
			{
				// celement: CLEAR2(0, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : CLEAR2
						// sub_1st_elem          : 
						// ori_name_1st_elem     : CLEAR2
						// regulate_c_name       : CLEAR2
						// input_type_conversion : CLEAR2
						if (&(CLEAR2) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<1> CLEAR2_tmp_mem;
							CLEAR2_tmp_mem = CLEAR2;
							CLEAR2_tvin_wrapc_buffer.range(0, 0) = CLEAR2_tmp_mem.range(0, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_CLEAR2, "%s\n", (CLEAR2_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_CLEAR2, tvin_CLEAR2);
		}

		tcl_file.set_num(1, &tcl_file.CLEAR2_depth);
		sprintf(tvin_CLEAR2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_CLEAR2, tvin_CLEAR2);

		// [[transaction]]
		sprintf(tvin_SP1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_SP1, tvin_SP1);

		sc_bv<16>* SP1_tvin_wrapc_buffer = new sc_bv<16>[75497472];

		// RTL Name: SP1
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: SP1(15, 0)
				{
					// carray: (0) => (75497471) @ (1)
					for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : SP1[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : SP1[0]
						// regulate_c_name       : SP1
						// input_type_conversion : SP1[i_0]
						if (&(SP1[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> SP1_tmp_mem;
							SP1_tmp_mem = SP1[i_0];
							SP1_tvin_wrapc_buffer[hls_map_index].range(15, 0) = SP1_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 75497472; i++)
		{
			sprintf(tvin_SP1, "%s\n", (SP1_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_SP1, tvin_SP1);
		}

		tcl_file.set_num(75497472, &tcl_file.SP1_depth);
		sprintf(tvin_SP1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_SP1, tvin_SP1);

		// release memory allocation
		delete [] SP1_tvin_wrapc_buffer;

		// [[transaction]]
		sprintf(tvin_SP2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_SP2, tvin_SP2);

		sc_bv<16>* SP2_tvin_wrapc_buffer = new sc_bv<16>[75497472];

		// RTL Name: SP2
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: SP2(15, 0)
				{
					// carray: (0) => (75497471) @ (1)
					for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : SP2[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : SP2[0]
						// regulate_c_name       : SP2
						// input_type_conversion : SP2[i_0]
						if (&(SP2[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> SP2_tmp_mem;
							SP2_tmp_mem = SP2[i_0];
							SP2_tvin_wrapc_buffer[hls_map_index].range(15, 0) = SP2_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 75497472; i++)
		{
			sprintf(tvin_SP2, "%s\n", (SP2_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_SP2, tvin_SP2);
		}

		tcl_file.set_num(75497472, &tcl_file.SP2_depth);
		sprintf(tvin_SP2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_SP2, tvin_SP2);

		// release memory allocation
		delete [] SP2_tvin_wrapc_buffer;

// [call_c_dut] ---------->

		CodeState = CALL_C_DUT;
		gemm_synth(M1, K1, N1, B1_SP_START, C1_SP_START, START1, CLEAR1, M2, K2, N2, B2_SP_START, C2_SP_START, START2, CLEAR2, SP1, SP2);

		CodeState = DUMP_OUTPUTS;

		// [[transaction]]
		sprintf(tvout_SP1, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_SP1, tvout_SP1);

		sc_bv<16>* SP1_tvout_wrapc_buffer = new sc_bv<16>[75497472];

		// RTL Name: SP1
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: SP1(15, 0)
				{
					// carray: (0) => (75497471) @ (1)
					for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : SP1[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : SP1[0]
						// regulate_c_name       : SP1
						// input_type_conversion : SP1[i_0]
						if (&(SP1[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> SP1_tmp_mem;
							SP1_tmp_mem = SP1[i_0];
							SP1_tvout_wrapc_buffer[hls_map_index].range(15, 0) = SP1_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 75497472; i++)
		{
			sprintf(tvout_SP1, "%s\n", (SP1_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_SP1, tvout_SP1);
		}

		tcl_file.set_num(75497472, &tcl_file.SP1_depth);
		sprintf(tvout_SP1, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_SP1, tvout_SP1);

		// release memory allocation
		delete [] SP1_tvout_wrapc_buffer;

		// [[transaction]]
		sprintf(tvout_SP2, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_SP2, tvout_SP2);

		sc_bv<16>* SP2_tvout_wrapc_buffer = new sc_bv<16>[75497472];

		// RTL Name: SP2
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: SP2(15, 0)
				{
					// carray: (0) => (75497471) @ (1)
					for (int i_0 = 0; i_0 <= 75497471; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : SP2[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : SP2[0]
						// regulate_c_name       : SP2
						// input_type_conversion : SP2[i_0]
						if (&(SP2[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> SP2_tmp_mem;
							SP2_tmp_mem = SP2[i_0];
							SP2_tvout_wrapc_buffer[hls_map_index].range(15, 0) = SP2_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 75497472; i++)
		{
			sprintf(tvout_SP2, "%s\n", (SP2_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_SP2, tvout_SP2);
		}

		tcl_file.set_num(75497472, &tcl_file.SP2_depth);
		sprintf(tvout_SP2, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_SP2, tvout_SP2);

		// release memory allocation
		delete [] SP2_tvout_wrapc_buffer;

		CodeState = DELETE_CHAR_BUFFERS;
		// release memory allocation: "M1"
		delete [] tvin_M1;
		// release memory allocation: "K1"
		delete [] tvin_K1;
		// release memory allocation: "N1"
		delete [] tvin_N1;
		// release memory allocation: "B1_SP_START"
		delete [] tvin_B1_SP_START;
		// release memory allocation: "C1_SP_START"
		delete [] tvin_C1_SP_START;
		// release memory allocation: "START1"
		delete [] tvin_START1;
		// release memory allocation: "CLEAR1"
		delete [] tvin_CLEAR1;
		// release memory allocation: "M2"
		delete [] tvin_M2;
		// release memory allocation: "K2"
		delete [] tvin_K2;
		// release memory allocation: "N2"
		delete [] tvin_N2;
		// release memory allocation: "B2_SP_START"
		delete [] tvin_B2_SP_START;
		// release memory allocation: "C2_SP_START"
		delete [] tvin_C2_SP_START;
		// release memory allocation: "START2"
		delete [] tvin_START2;
		// release memory allocation: "CLEAR2"
		delete [] tvin_CLEAR2;
		// release memory allocation: "SP1"
		delete [] tvin_SP1;
		delete [] tvout_SP1;
		// release memory allocation: "SP2"
		delete [] tvin_SP2;
		delete [] tvout_SP2;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
	}
}


// apint = int1
#undef int1
