// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2018.3
// Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _gemm_synth_HH_
#define _gemm_synth_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "compute.h"
#include "clear_c.h"
#include "gemm_synth_HLS_GEMM_PERIPH_BUS_s_axi.h"

namespace ap_rtl {

template<unsigned int C_S_AXI_HLS_GEMM_PERIPH_BUS_ADDR_WIDTH = 7,
         unsigned int C_S_AXI_HLS_GEMM_PERIPH_BUS_DATA_WIDTH = 32>
struct gemm_synth : public sc_module {
    // Port declarations 27
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst_n;
    sc_out< sc_lv<32> > SP1_Addr_A;
    sc_out< sc_logic > SP1_EN_A;
    sc_out< sc_lv<4> > SP1_WEN_A;
    sc_out< sc_lv<32> > SP1_Din_A;
    sc_in< sc_lv<32> > SP1_Dout_A;
    sc_out< sc_logic > SP1_Clk_A;
    sc_out< sc_logic > SP1_Rst_A;
    sc_in< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_AWVALID;
    sc_out< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_AWREADY;
    sc_in< sc_uint<C_S_AXI_HLS_GEMM_PERIPH_BUS_ADDR_WIDTH> > s_axi_HLS_GEMM_PERIPH_BUS_AWADDR;
    sc_in< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_WVALID;
    sc_out< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_WREADY;
    sc_in< sc_uint<C_S_AXI_HLS_GEMM_PERIPH_BUS_DATA_WIDTH> > s_axi_HLS_GEMM_PERIPH_BUS_WDATA;
    sc_in< sc_uint<C_S_AXI_HLS_GEMM_PERIPH_BUS_DATA_WIDTH/8> > s_axi_HLS_GEMM_PERIPH_BUS_WSTRB;
    sc_in< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_ARVALID;
    sc_out< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_ARREADY;
    sc_in< sc_uint<C_S_AXI_HLS_GEMM_PERIPH_BUS_ADDR_WIDTH> > s_axi_HLS_GEMM_PERIPH_BUS_ARADDR;
    sc_out< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_RVALID;
    sc_in< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_RREADY;
    sc_out< sc_uint<C_S_AXI_HLS_GEMM_PERIPH_BUS_DATA_WIDTH> > s_axi_HLS_GEMM_PERIPH_BUS_RDATA;
    sc_out< sc_lv<2> > s_axi_HLS_GEMM_PERIPH_BUS_RRESP;
    sc_out< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_BVALID;
    sc_in< sc_logic > s_axi_HLS_GEMM_PERIPH_BUS_BREADY;
    sc_out< sc_lv<2> > s_axi_HLS_GEMM_PERIPH_BUS_BRESP;
    sc_out< sc_logic > interrupt;
    sc_signal< sc_logic > ap_var_for_const0;
    sc_signal< sc_lv<32> > ap_var_for_const1;


    // Module declarations
    gemm_synth(sc_module_name name);
    SC_HAS_PROCESS(gemm_synth);

    ~gemm_synth();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    gemm_synth_HLS_GEMM_PERIPH_BUS_s_axi<C_S_AXI_HLS_GEMM_PERIPH_BUS_ADDR_WIDTH,C_S_AXI_HLS_GEMM_PERIPH_BUS_DATA_WIDTH>* gemm_synth_HLS_GEMM_PERIPH_BUS_s_axi_U;
    compute* grp_compute_fu_90;
    clear_c* grp_clear_c_fu_106;
    sc_signal< sc_logic > ap_rst_n_inv;
    sc_signal< sc_logic > ap_start;
    sc_signal< sc_logic > ap_done;
    sc_signal< sc_logic > ap_idle;
    sc_signal< sc_lv<2> > ap_CS_fsm;
    sc_signal< sc_logic > ap_CS_fsm_state1;
    sc_signal< sc_logic > ap_ready;
    sc_signal< sc_lv<32> > M1;
    sc_signal< sc_lv<32> > K1;
    sc_signal< sc_lv<32> > N1;
    sc_signal< sc_lv<32> > B1_SP_START;
    sc_signal< sc_lv<32> > C1_SP_START;
    sc_signal< sc_lv<1> > START1;
    sc_signal< sc_lv<1> > CLEAR1;
    sc_signal< sc_lv<1> > CLEAR1_read_read_fu_48_p2;
    sc_signal< sc_lv<1> > CLEAR1_read_reg_114;
    sc_signal< sc_lv<1> > START1_read_read_fu_54_p2;
    sc_signal< sc_lv<1> > START1_read_reg_118;
    sc_signal< sc_lv<32> > C1_SP_START_read_reg_122;
    sc_signal< sc_lv<32> > B1_SP_START_read_reg_128;
    sc_signal< sc_lv<32> > N1_read_reg_133;
    sc_signal< sc_lv<32> > K1_read_reg_138;
    sc_signal< sc_lv<32> > M1_read_reg_143;
    sc_signal< sc_logic > grp_compute_fu_90_ap_start;
    sc_signal< sc_logic > grp_compute_fu_90_ap_done;
    sc_signal< sc_logic > grp_compute_fu_90_ap_idle;
    sc_signal< sc_logic > grp_compute_fu_90_ap_ready;
    sc_signal< sc_lv<32> > grp_compute_fu_90_SP_Addr_A;
    sc_signal< sc_logic > grp_compute_fu_90_SP_EN_A;
    sc_signal< sc_lv<4> > grp_compute_fu_90_SP_WEN_A;
    sc_signal< sc_lv<32> > grp_compute_fu_90_SP_Din_A;
    sc_signal< sc_logic > grp_clear_c_fu_106_ap_start;
    sc_signal< sc_logic > grp_clear_c_fu_106_ap_done;
    sc_signal< sc_logic > grp_clear_c_fu_106_ap_idle;
    sc_signal< sc_logic > grp_clear_c_fu_106_ap_ready;
    sc_signal< sc_lv<32> > grp_clear_c_fu_106_SP_Addr_A;
    sc_signal< sc_logic > grp_clear_c_fu_106_SP_EN_A;
    sc_signal< sc_lv<4> > grp_clear_c_fu_106_SP_WEN_A;
    sc_signal< sc_lv<32> > grp_clear_c_fu_106_SP_Din_A;
    sc_signal< sc_logic > grp_compute_fu_90_ap_start_reg;
    sc_signal< sc_logic > ap_CS_fsm_state2;
    sc_signal< sc_logic > grp_clear_c_fu_106_ap_start_reg;
    sc_signal< bool > ap_predicate_op33_call_state2;
    sc_signal< bool > ap_block_state2_on_subcall_done;
    sc_signal< sc_lv<2> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<2> ap_ST_fsm_state1;
    static const sc_lv<2> ap_ST_fsm_state2;
    static const sc_lv<32> ap_const_lv32_0;
    static const bool ap_const_boolean_1;
    static const int C_S_AXI_DATA_WIDTH;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_1;
    static const bool ap_const_boolean_0;
    // Thread declarations
    void thread_ap_var_for_const0();
    void thread_ap_var_for_const1();
    void thread_ap_clk_no_reset_();
    void thread_CLEAR1_read_read_fu_48_p2();
    void thread_SP1_Addr_A();
    void thread_SP1_Clk_A();
    void thread_SP1_Din_A();
    void thread_SP1_EN_A();
    void thread_SP1_Rst_A();
    void thread_SP1_WEN_A();
    void thread_START1_read_read_fu_54_p2();
    void thread_ap_CS_fsm_state1();
    void thread_ap_CS_fsm_state2();
    void thread_ap_block_state2_on_subcall_done();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_predicate_op33_call_state2();
    void thread_ap_ready();
    void thread_ap_rst_n_inv();
    void thread_grp_clear_c_fu_106_ap_start();
    void thread_grp_compute_fu_90_ap_start();
    void thread_ap_NS_fsm();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
