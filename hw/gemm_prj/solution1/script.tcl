############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project gemm_prj
set_top gemm_synth
add_files gemm_sw/gemm.c
add_files -tb gemm_sw/gemm_tb.c -cflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xczu3eg-sbva484-1-e}
create_clock -period 100MHz -name default
config_export -format ip_catalog -rtl verilog
#source "./gemm_prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
