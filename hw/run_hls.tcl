open_project -reset gemm_prj
# The source file and test bench
add_files gemm_sw/gemm.c
add_files -tb gemm_sw/gemm_tb.c
# Specify the top-level function for synthesis
set_top gemm_synth

###########################
# Solution settings

# Create solution1
open_solution solution1

# Specify a Xilinx device and clock period
set_part {xczu3eg-sbva484-1-e}
create_clock -period "100MHz"

# Simulate the C code 
csim_design

# Do not perform any other steps
# - The basic project will be opened in the GUI 
exit

