0. Setup environment on LRC: module load xilinx/2018
1. Setup HLS project: vivado_hls -f run_hls.tcl
2. Open project: vivado_hls -p gemm_prj
3. For each solution:
    a. Hit "C Synthesis" button to generate Synthesis Report
    b. Hit "Run C/RTL Cosimulation" button to generate Cosimulation Report
4. Compare Results: 
    a. Project -> Compare Reports... -> Add all solutions
    b. Compare Timing, Latency, and Utilization Estimates
    c. Look at invidiual Cosimulation Reports to compare latency of each solution through our C testbench