SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c  # "strict" bash mode
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables

# Echo the value of variable $%
print-%: ; @echo $* = $($*)

#####################
### Configuration ###
#####################

# Comment out to build a regular C testbench.
# USE_HLS := 1

# Name of testbench executable
TESTBENCH := gemm_tb

.DEFAULT_GOAL := test

#########################
### End configuration ###
#########################

CC := gcc

WFLAGS := \
	-Wall \
	-Wextra	\
	-Wpedantic\
	-Werror \

STD_FLAGS := -std=c11
DFLAGS := -g
OFLAGS := -Ofast

ifndef (USE_HLS)
DFLAGS += -DNO_SYNTH
endif

CFLAGS := $(WFLAGS) $(STD_FLAGS) $(DFLAGS) $(OFLAGS)

.phony: testbench
testbench: $(TESTBENCH)
$(TESTBENCH): $(shell ls *.[ch])
	@printf '%b' "  CC  $(filter-out %.h,$^) -> $@\n"
	$(CC) -o $@ $(CFLAGS) $(filter-out %.h,$^)

.phony: test
test: $(TESTBENCH)
	./$<

.phony: clean
clean:
	@printf '%b' "  Clean\n"
	rm -f $(TESTBENCH)
