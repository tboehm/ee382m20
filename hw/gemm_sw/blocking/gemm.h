#ifndef __GEMM_H__
#define __GEMM_H__

#include "ap_cint.h"
typedef int64 fixed_t;
typedef int32 index_t;

#define MASK 0x0FFFF
#define MASK0(x) (x & MASK)
#define MASK1(x) ((x >> 16) & MASK)
#define MASK2(x) ((x >> 32) & MASK)
#define MASK3(x) ((x >> 48) & MASK)

#define FX_INT_BITS 7
#define FX_SCALE_BITS 9
#define FX_WIDTH ((FX_INT_BITS) + (FX_SCALE_BITS))

#define FX_MAX ((1 << ((FX_WIDTH) - 1)) - 1)
#define FX_MIN (-1 - (FX_MAX))

#define FX_CLAMP(fx) ((fx) > (FX_MAX) ? (FX_MAX) : (fx) < (FX_MIN) ? (FX_MIN) : (fx))

#define RESULT(r3,r2,r1,r0) ((r3 << 48) | (r2 << 32) | (r1 << 16) | (r0))

#define SP_SIZE 98304

// #define A_CACHE_SIZE 1024

void
gemm_synth(bool START1, index_t M1, index_t N1, 
           index_t K1, index_t new_M1, index_t B1_SP_START, 
           index_t C1_SP_START, fixed_t SP1[SP_SIZE]
		);

#endif // __GEMM_H__
