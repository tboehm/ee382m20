#include "gemm.h"

void
compute(fixed_t *SP, index_t M, index_t K, index_t N,
        index_t new_M, index_t B_SP_START, index_t C_SP_START)
{
    int64 A_CACHE_0[1152];
    int64 A_CACHE_1[1152];
    int64 A_CACHE_2[1152];
    int64 A_CACHE_3[1152];
#pragma HLS RESOURCE variable=A_CACHE_0 core=RAM_2P_LUTRAM
#pragma HLS RESOURCE variable=A_CACHE_1 core=RAM_2P_LUTRAM
#pragma HLS RESOURCE variable=A_CACHE_2 core=RAM_2P_LUTRAM
#pragma HLS RESOURCE variable=A_CACHE_3 core=RAM_2P_LUTRAM
#pragma HLS ARRAY_PARTITION variable=A_CACHE_0 cyclic factor=64
#pragma HLS ARRAY_PARTITION variable=A_CACHE_1 cyclic factor=64
#pragma HLS ARRAY_PARTITION variable=A_CACHE_2 cyclic factor=64
#pragma HLS ARRAY_PARTITION variable=A_CACHE_3 cyclic factor=64
    int64 B_CACHE[1152];
#pragma HLS RESOURCE variable=B_CACHE core=RAM_2P_LUTRAM
#pragma HLS ARRAY_PARTITION variable=B_CACHE cyclic factor=64
    for (index_t j = 0; j < N; j++) {
        index_t b_idx = j * K + B_SP_START;
        for (index_t i = 0; i < new_M; i += 4) {
            // Compute two C results at a time.
            for(int16 m = 0; m < K; m+=4){
#pragma HLS PIPELINE
                // Read into A_CACHE
                A_CACHE_0[m/4] = SP[(i*K+m)/4];
                A_CACHE_1[m/4] = SP[(i*K+1+m)/4];
                A_CACHE_2[m/4] = SP[(i*K+2+m)/4];
                A_CACHE_3[m/4] = SP[(i*K+3+m)/4];
                // Read into B_CACHE
                B_CACHE[m/4] = SP[(b_idx + m) / 4];
            }
            int32 res0 = 0;
            int32 res1 = 0;
            int32 res2 = 0;
            int32 res3 = 0;
            for (index_t k = 0; k < K; k += 4) {
#pragma HLS UNROLL factor=4
#pragma HLS PIPELINE
                // Use the upper and lower part of each word.
                fixed_t a_word0 = A_CACHE_0[k/4];//SP[(a_idx0 + k) / 4];
                fixed_t a_word1 = A_CACHE_1[k/4];//SP[(a_idx1 + k) / 4];
                fixed_t a_word2 = A_CACHE_2[k/4];//SP[(a_idx2 + k) / 4];
                fixed_t a_word3 = A_CACHE_3[k/4];//SP[(a_idx3 + k) / 4];
                fixed_t b_word = B_CACHE[k/4];
                int16 a_M0_0 = MASK0(a_word0); int16 a_M0_1 = MASK0(a_word1);
                int16 a_M1_0 = MASK1(a_word0); int16 a_M1_1 = MASK1(a_word1);
                int16 a_M2_0 = MASK2(a_word0); int16 a_M2_1 = MASK2(a_word1);
                int16 a_M3_0 = MASK3(a_word0); int16 a_M3_1 = MASK3(a_word1);
                int16 a_M0_2 = MASK0(a_word2); int16 a_M0_3 = MASK0(a_word3);
                int16 a_M1_2 = MASK1(a_word2); int16 a_M1_3 = MASK1(a_word3);
                int16 a_M2_2 = MASK2(a_word2); int16 a_M2_3 = MASK2(a_word3);
                int16 a_M3_2 = MASK3(a_word2); int16 a_M3_3 = MASK3(a_word3);
                int16 b_M0 = MASK0(b_word); 
                int16 b_M1 = MASK1(b_word); 
                int16 b_M2 = MASK2(b_word); 
                int16 b_M3 = MASK3(b_word); 
                
                res0 += a_M0_0 * b_M0 + 
                        a_M1_0 * b_M1 +
                		a_M2_0 * b_M2 + 
                        a_M3_0 * b_M3;
                res1 += a_M0_1 * b_M0 +
                        a_M1_1 * b_M1 +
                        a_M2_1 * b_M2 +
                        a_M3_1 * b_M3;
                res2 += a_M0_2 * b_M0 + 
                        a_M1_2 * b_M1 +
                		a_M2_2 * b_M2 + 
                        a_M3_2 * b_M3;
                res3 += a_M0_3 * b_M0 +
                        a_M1_3 * b_M1 +
                        a_M2_3 * b_M2 +
                        a_M3_3 * b_M3;
            }
            res0 = MASK0(FX_CLAMP(res0 >> FX_SCALE_BITS));
            res1 = MASK0(FX_CLAMP(res1 >> FX_SCALE_BITS));
            res2 = MASK0(FX_CLAMP(res2 >> FX_SCALE_BITS));
            res3 = MASK0(FX_CLAMP(res3 >> FX_SCALE_BITS));
            SP[(j * M + i + C_SP_START) / 4] = RESULT((fixed_t)res3, (uint48)res2, (uint32)res1, res0);
        }
    }
}

void
gemm_synth(bool START1, index_t M1, index_t N1, 
           index_t K1, index_t new_M1, index_t B1_SP_START, 
           index_t C1_SP_START, fixed_t SP1[SP_SIZE])
{
#pragma HLS INTERFACE s_axilite port=START1 bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=M1 bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=N1 bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=K1 bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=new_M1 bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=B1_SP_START bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE s_axilite port=C1_SP_START bundle="HLS_GEMM_PERIPH_BUS"
#pragma HLS INTERFACE bram port=SP1
#pragma HLS RESOURCE variable=SP1 core=RAM_1P_BRAM

#pragma HLS INTERFACE s_axilite port=return bundle="HLS_GEMM_PERIPH_BUS"

    if(START1) compute(SP1, M1, K1, N1, new_M1, B1_SP_START, C1_SP_START);
    return;
}
