#include <stdio.h>
#include <stdlib.h>

#include "gemm.h"
#define M_SIZE 128
#define N_SIZE 160
#define K_SIZE 576

#define A_SIZE M_SIZE * K_SIZE
#define B_SIZE N_SIZE * K_SIZE
#define C_SIZE M_SIZE * N_SIZE
static void
ref_gemm(int16 A[M_SIZE][K_SIZE], int16 B[N_SIZE][K_SIZE], int16 C[N_SIZE][M_SIZE], index_t M, index_t K, index_t N)
{
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            int c_part = 0;
            for (int k = 0; k < K; k++) {
                c_part += A[i][k] * B[j][k];
            }
            C[j][i] = FX_CLAMP(c_part >> FX_SCALE_BITS);
        }
    }
}


void
initialize_matrices(fixed_t *SP1, int16 A[M_SIZE][K_SIZE], int16 B[N_SIZE][K_SIZE], index_t M, index_t N, index_t K)
{
	printf("Initialize Matrices\n");
    srand(1337);
    int k;
    index_t b_start = M*K;
    for(int i = 0; i < M; i++){
        for(k = 0; k < K; k+=4){
            A[i][k  ] =  rand() % (1 << (FX_WIDTH));
            A[i][k+1] =  rand() % (1 << (FX_WIDTH));
            A[i][k+2] =  rand() % (1 << (FX_WIDTH));
            A[i][k+3] =  rand() % (1 << (FX_WIDTH));
            SP1[(i*K+k)/4] = ((fixed_t)A[i][k+3] << 48) | ((uint48)A[i][k+2] << 32) | (uint32)(A[i][k+1] << 16) | A[i][k];
        }
    }
    for(int j = 0; j < N; j++){
        for(k = 0; k < K; k+=4){
            B[j][k  ] =  rand() % (1 << (FX_WIDTH));
            B[j][k+1] =  rand() % (1 << (FX_WIDTH));
            B[j][k+2] =  rand() % (1 << (FX_WIDTH));
            B[j][k+3] =  rand() % (1 << (FX_WIDTH));

            SP1[(j*K+k + b_start)/4] = ((fixed_t)B[j][k+3] << 48) | ((uint48)B[j][k+2] << 32) | (uint32)(B[j][k+1] << 16) | B[j][k];
        }
    }
}


int
compute_test(fixed_t *SP1, index_t C_SP_START, int16 C[N_SIZE][M_SIZE], index_t C_ROWS, index_t C_COLS){
	printf("Test COMPUTE Functionality\n");
    index_t c_idx = C_SP_START;
    int16 sp_val;
    for(int n = 0; n < C_ROWS; n++){
        for(int m = 0; m < C_COLS; m++){
            if     (m%4==3) sp_val = MASK3(SP1[((n*C_COLS+m + C_SP_START)/4)]);
            else if(m%4==2) sp_val = MASK2(SP1[((n*C_COLS+m + C_SP_START)/4)]);
            else if(m%4==1) sp_val = MASK1(SP1[((n*C_COLS+m + C_SP_START)/4)]);
            else            sp_val = MASK0(SP1[((n*C_COLS+m + C_SP_START)/4)]);
            
            if((sp_val & MASK) != (C[n][m] & MASK)) {
                printf("C[%u][%u] = 0x%0.4x, sp_val = 0x%0.4x, SP = 0x%0.8x%0.8x\n", n, m, C[n][m] & MASK, sp_val & MASK, 
                (unsigned int)(SP1[((n*C_COLS+m + C_SP_START)/4)]>>32),
                (unsigned int)(SP1[((n*C_COLS+m + C_SP_START)/4)] & 0xFFFFFFFF) );
                return 1;
            }
        }
    }
    printf("TEST PASS!\n");
    return 0;
}

int
main(void)
{
    fixed_t *SP1;
    static int16 A[M_SIZE][K_SIZE];
    static int16 B[N_SIZE][K_SIZE];
    static int16 C[N_SIZE][M_SIZE];

#ifdef NO_SYNTH
    SP1 = (fixed_t *)malloc(SP_SIZE * sizeof(fixed_t));
    A = (int16 *)malloc(A_SIZE * sizeof(int16));
    B = (int16 *)malloc(B_SIZE * sizeof(int16));
    C = (int16 *)malloc(C_SIZE * sizeof(int16));
#else
    static fixed_t SP1_arr[SP_SIZE];
    // static int16 A_arr[M_SIZE][K_SIZE];
    // static int16 B_arr[N_SIZE][K_SIZE];
    // static int16 C_arr[M_SIZE][N_SIZE];

    SP1 = &SP1_arr[0];
    // A = A_arr;
    // B = B_arr;
    // C = C_arr;
#endif // NO_SYNTH

    index_t M = M_SIZE;
    index_t N = N_SIZE;
    index_t K = K_SIZE;

    index_t B_SP_START = M * K;
    index_t C_SP_START = B_SP_START + (N * K);

    initialize_matrices(SP1, A, B, M, N, K);
    ref_gemm(A, B, C, M, K, N);
    gemm_synth(1, M, N, K, M, B_SP_START, C_SP_START, SP1);
    return compute_test(SP1, C_SP_START, C, N, M);
}
